---
title: 'Scripts for Easier Use of Spice (SEUS): A Perl script package for simulating and creating batches of circuit netlists for Monte Carlo simulations when using *Ngspice* or *Ngspice*-based simulators'
tags:
  - Perl
  - Ngspice
  - Monte Carlo simulations
authors:
  - name: Michael A. Turi
    orcid: 0000-0002-9171-5305
    affiliation: 1
affiliations:
 - name: Computer Engineering Program, California State University, Fullerton
   index: 1
date: 28 July 2020
bibliography: paper.bib

---

# Introduction

Spice, or Spice-based, circuit simulators are typically used to simulate electronic circuits at the transistor level.  There are many Spice simulators available, with *Ngspice* being a popular open-source Spice option [@ngspice].  This Perl script package, *Scripts for Easier Use of Spice*, or ``SEUS``, allows users to more easily run *Ngspice* to simulate and create batches of circuit netlists for Monte Carlo simulations [@seus].  Monte Carlo simulations are used in many fields; these are often used in circuit design to simulate the effects of process, voltage, and/or temperature (PVT) variations on circuits.  Process variations are important because they may change the physical dimensions of transistors or other devices and affect the intended operation and/or performance of a circuit.  Likewise, variations in operational voltages and temperatures may affect the intended operation and/or performance of a circuit.


# Statement of Need

*Ngspice* and many other current Spice simulators support Monte Carlo simulations.  However, some older Spice simulators or Spice-based simulators, such as *University of Florida's Spice3-UFDG (Linux version 3.71)* [@Fossum2004; @Fossum2006], do not provide this support.  Without ``SEUS``, a user cannot use Monte Carlo simulations to analyze the effects of PVT variations when using this *Spice3-UFDG* simulator and its built-in model for FinFET, or tri-gate, transistor technology.  Lack of simulation support can occur with Spice-based simulators for alternative technologies, such as FinFETs.  ``SEUS`` was originally designed to add Monte Carlo simulation support to the *Spice3-UFDG* simulator, but has since been revised to work with current versions of *Ngspice*.

``SEUS`` offers users Monte Carlo support with *Ngspice* or *Ngspice*-based simulators, which is especially useful when the simulator lacks native Monte Carlo simulation support.  The Monte Carlo initialization algorithm used by *init\_batch\_seus.pl* is already published [@Turi2017a], and the ``SEUS`` package enabled the use of Monte Carlo simulations for a few engineering publications [@Turi2020;@Turi2017b].  ``SEUS`` can enable additional research or exploration by analyzing PVT variation effects on circuit designs.


# Availability and Support

The ``SEUS`` Perl script package is found at the git repository at [@seus] and is licensed under version 3 of the GNU Affero General Public License.  Please refer to the README in the repository and the script documentation for information about installing and using this package; documentation for each script is available via HTML files and manual pages after installing the package or by using the ``perldoc`` command with the script's name (e.g., ``perldoc seus.pl``).

To contribute to the ``SEUS`` package or seek support, please contact the author via the repository or via the email address in the documentation.  An issue tracker is available in the repository to report a bug or request a feature or enhancement [@seusissues].


# Acknowledgements

The author wishes to acknowledge Jose G. Delgado-Frias of the School of Electrical Engineering and Computer Science at Washington State University for a doctoral assistantship under his supervision and for his ideas on FinFET circuit design and simulation, which drove the development of this project.  The author also wishes to thank the reviewers and editors for their efforts and for their helpful comments to improve this paper and the software package.


# References
