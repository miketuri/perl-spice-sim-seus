#!/usr/bin/perl -w
use strict;
use warnings;

use File::Copy;
use File::Compare;
use File::Path 'remove_tree';
use IPC::Run 'run';
use Test::More tests => 96;

# SEUS Package's init_batch_seus.t:
#   Test script for init_batch_seus.pl
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $SUT = './lib/init_batch_seus.pl'; # Script Under Test
my $ORIG_NETLIST = './examples/amp-csrc-dc'; # Original netlist to copy for testing
my $NET_NAME = './t/init_batch'; # Copy of netlist to use for tests
my $DIR_NAME = './t/init_batch_seus'; # Directory name to put generated netlists


# Procedure: check_dir
#
#   Check if the named directory contains the particular file types
#   First argument: Scalar ($) of the directory path
#   Second argument: Array reference (\@) containing the range of numbered files
#   Third argument: Scalar ($) of the file type to look for
#   Return value: Number of files that exist
sub check_dir {
  my $dir_path = shift;
  my $range = shift;
  my $filetype = shift;
  my $exist_count = 0;
  
  foreach (@{$range}) {
    if( -e "$dir_path/$_$filetype" ) { $exist_count++; }
  }
  return $exist_count;
}


my @a0_10 = (0..10);

# Run with no arguments to get usage
my @cmd = ($SUT);
my $in;
my $out;
my $err;
run( \@cmd, \$in, \$out, \$err );
like( $err, qr/^Usage: init_batch_seus.pl/, "#1: $SUT should print usage if no arguments provided" ); #1

copy( "$ORIG_NETLIST.i", "$NET_NAME.i" ) or BAIL_OUT( "Error: Cannot copy file $ORIG_NETLIST.i due to: $!" );

# Run init_batch to get the summary of data columns for vary defs and display license
remove_tree( $DIR_NAME, {error => \my $rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

@cmd = ( $SUT, '-u', './lib', $NET_NAME, $DIR_NAME, './examples/init_batch_def.vary-dc.txt' );
run( \@cmd, \$in, \$out, \$err );
like( $out, qr/Copyright \(C\) 2009-2020/, "#2: $SUT should print license since not suppressed" ); #2
is( check_dir( $DIR_NAME, \@a0_10, '.i' ), 1, '#3: only one netlist created when printing summary of data columns' ); #3
ok( -e "$DIR_NAME/0.i", '#4: the only netlist created when printing summary of data columns is the nominal, 0.i netlist' ); #4

my @run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-ls', "$NET_NAME" );
run( \@run_cmd, \$in, \$out, \$err );
@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-ls', "$DIR_NAME/0" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$NET_NAME.sti", "$DIR_NAME/0.sti" ), 0, '#5: simulation results of the nominal, 0.i netlist when printing summary of data columns should be identical to the original netlist' ); #5


# Run init_batch to create a batch of netlists for vary defs using Monte Carlo values
#   and suppress displaying license and mosfet transistor fingers are expanded
remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

@cmd = ( $SUT, '-u', './lib', '-fl', $NET_NAME, $DIR_NAME, './examples/init_batch_def.vary-dc.txt', './examples/init_batch_mcvals_9d4n.vary-dc.txt', 4 );
run( \@cmd, \$in, \$out, \$err );
unlike( $out, qr/Copyright \(C\) 2009-2020/, "#6: $SUT should not print license since suppressed" ); #6
is( check_dir( $DIR_NAME, \@a0_10, '.i' ), 5, '#7: five netlists created for vary definitions' ); #7
ok( -e "$DIR_NAME/0.i", "#8: specifically checked that $DIR_NAME/0.i exists" ); #8
ok( -e "$DIR_NAME/1.i", "#9: specifically checked that $DIR_NAME/1.i exists" ); #9
open my $in_fh, "<$DIR_NAME/1.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/1.i for read due to: $!" );
my @in_net = <$in_fh>;
my $in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=7/i, '#10: parameter tempc is correct in vary def\'s 1.i' ); #10
like( $in_netlist, qr/\.model m\.xinv\.mp.*vth0=-0\.1/i, '#11: parameter vth0 is correct in vary def\'s 1.i' ); #11
like( $in_netlist, qr/\.model m\.xinv\.mp.*nch=240000000000000000/i, '#12: parameter nch is correct in vary def\'s 1.i' ); #12
like( $in_netlist, qr/\.model m\.xinv\.mn\.1.*tox=1\.2e-08/i, '#13: parameter tox is correct in vary def\'s 1.i' ); #13
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*w=1e-6/i, '#14: parameter w is correct in vary def\'s 1.i' ); #14
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*l=3e-06/i, '#15: parameter l is correct in vary def\'s 1.i' ); #15
like( $in_netlist, qr/\.model m\.xinv\.mn\.2.*tox=1\.32e-08/i, '#16: parameter tox is correct in vary def\'s 4.i' ); #16
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*w=4e-6/i, '#17: parameter w is correct in vary def\'s 4.i' ); #17
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*l=3\.45e-06/i, '#18: parameter l is correct in vary def\'s 4.i' ); #18
ok( -e "$DIR_NAME/2.i", "#19: specifically checked that $DIR_NAME/2.i exists" ); #19
open $in_fh, "<$DIR_NAME/2.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/2.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=17/i, '#20: parameter tempc is correct in vary def\'s 2.i' ); #20
like( $in_netlist, qr/\.model m\.xinv\.mp.*vth0=-0\.2/i, '#21: parameter vth0 is correct in vary def\'s 2.i' ); #21
like( $in_netlist, qr/\.model m\.xinv\.mp.*nch=232000000000000000/i, '#22: parameter nch is correct in vary def\'s 2.i' ); #22
like( $in_netlist, qr/\.model m\.xinv\.mn\.1.*tox=1\.24e-08/i, '#23: parameter tox is correct in vary def\'s 2.i' ); #23
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*w=2e-6/i, '#24: parameter w is correct in vary def\'s 2.i' ); #24
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*l=3\.15e-06/i, '#25: parameter l is correct in vary def\'s 2.i' ); #25
like( $in_netlist, qr/\.model m\.xinv\.mn\.2.*tox=1\.28e-08/i, '#26: parameter tox is correct in vary def\'s 3.i' ); #26
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*w=3e-6/i, '#27: parameter w is correct in vary def\'s 3.i' ); #27
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*l=3\.3e-06/i, '#28: parameter l is correct in vary def\'s 3.i' ); #28
ok( -e "$DIR_NAME/3.i", "#29: specifically checked that $DIR_NAME/3.i exists" ); #29
open $in_fh, "<$DIR_NAME/3.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/3.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=37/i, '#30: parameter tempc is correct in vary def\'s 3.i' ); #30
like( $in_netlist, qr/\.model m\.xinv\.mp.*vth0=-0\.3/i, '#31: parameter vth0 is correct in vary def\'s 3.i' ); #31
like( $in_netlist, qr/\.model m\.xinv\.mp.*nch=224000000000000000/i, '#32: parameter nch is correct in vary def\'s 3.i' ); #32
like( $in_netlist, qr/\.model m\.xinv\.mn\.1.*tox=1\.28e-08/i, '#33: parameter tox is correct in vary def\'s 3.i' ); #33
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*w=3e-6/i, '#34: parameter w is correct in vary def\'s 3.i' ); #34
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*l=3\.3e-06/i, '#35: parameter l is correct in vary def\'s 3.i' ); #35
like( $in_netlist, qr/\.model m\.xinv\.mn\.2.*tox=1\.24e-08/i, '#36: parameter tox is correct in vary def\'s 2.i' ); #36
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*w=2e-6/i, '#37: parameter w is correct in vary def\'s 2.i' ); #37
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*l=3\.15e-06/i, '#38: parameter l is correct in vary def\'s 2.i' ); #38
ok( -e "$DIR_NAME/4.i", "#39: specifically checked that $DIR_NAME/4.i exists" ); #39
open $in_fh, "<$DIR_NAME/4.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/4.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=47/i, '#40: parameter tempc is correct in vary def\'s 4.i' ); #40
like( $in_netlist, qr/\.model m\.xinv\.mp.*vth0=-0\.4/i, '#41: parameter vth0 is correct in vary def\'s 4.i' ); #41
like( $in_netlist, qr/\.model m\.xinv\.mp.*nch=216000000000000000/i, '#42: parameter nch is correct in vary def\'s 4.i' ); #42
like( $in_netlist, qr/\.model m\.xinv\.mn\.1.*tox=1\.32e-08/i, '#43: parameter tox is correct in vary def\'s 4.i' ); #43
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*w=4e-6/i, '#44: parameter w is correct in vary def\'s 4.i' ); #44
like( $in_netlist, qr/m\.xinv\.mn\.1 out.*l=3\.45e-06/i, '#45: parameter l is correct in vary def\'s 4.i' ); #45
like( $in_netlist, qr/\.model m\.xinv\.mn\.2.*tox=1\.2e-08/i, '#46: parameter tox is correct in vary def\'s 1.i' ); #46
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*w=1e-6/i, '#47: parameter w is correct in vary def\'s 1.i' ); #47
like( $in_netlist, qr/m\.xinv\.mn\.2 out.*l=3e-06/i, '#48: parameter l is correct in vary def\'s 1.i' ); #48

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-ls', "$DIR_NAME/0" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$NET_NAME.sti", "$DIR_NAME/0.sti" ), 0, '#49: simulation results of the nominal, 0.i netlist for vary def should be identical to the original netlist' ); #49


# Run init_batch to create a batch of netlists for non-hierarchical sweep defs (one level of sweep) and mosfet transistor fingers are not expanded
remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

@cmd = ( $SUT, '-u', './lib', $NET_NAME, $DIR_NAME, './examples/init_batch_def.sweep.txt' );
run( \@cmd, \$in, \$out, \$err );
ok( -e "$DIR_NAME/0.i", "#50: specifically checked that $DIR_NAME/0.i exists" ); #50
ok( -e "$DIR_NAME/1/0.i", "#51: specifically checked that $DIR_NAME/1/0.i exists" ); #51
open $in_fh, "<$DIR_NAME/1/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/1/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=0/i, '#52: parameter tempc is correct in non-hierarchical sweep def\'s 1/0.i' ); #52
like( $in_netlist, qr/\.model nmos.*tox=13e-9/i, '$53: parameter tox is correct in non-hierarchical sweep def\'s 1/0.i' ); #53
like( $in_netlist, qr/m\.xinv\.mp out.*w=2\.4e-06/i, '#54: parameter w is correct in non-hierarchical sweep def\'s 1/0.i' ); #54
ok( -e "$DIR_NAME/2/0.i", "#55: specifically checked that $DIR_NAME/2/0.i exists" ); #55
open $in_fh, "<$DIR_NAME/2/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/2/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=27/i, '#56: parameter tempc is correct in non-hierarchical sweep def\'s 2/0.i' ); #56
like( $in_netlist, qr/\.model nmos.*tox=1\.25e-08/i, '#57: parameter tox is correct in non-hierarchical sweep def\'s 2/0.i' ); #57
like( $in_netlist, qr/m\.xinv\.mp out.*w=2\.7e-06/i, '#58: parameter w is correct in non-hierarchical sweep def\'s 2/0.i' ); #58
ok( -e "$DIR_NAME/3/0.i", "#59: specifically checked that $DIR_NAME/3/0.i exists" ); #59
open $in_fh, "<$DIR_NAME/3/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/3/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=50/i, '#60: parameter tempc is correct in non-hierarchical sweep def\'s 3/0.i' ); #60
like( $in_netlist, qr/\.model nmos.*tox=1\.2e-08/i, '#61: parameter tox is correct in non-hierarchical sweep def\'s 3/0.i' ); #61
like( $in_netlist, qr/m\.xinv\.mp out.*w=3e-06/i, '#62: parameter w is correct in non-hierarchical sweep def\'s 3/0.i' ); #62
ok( -e "$DIR_NAME/4/0.i", "#63: specifically checked that $DIR_NAME/4/0.i exists" ); #63
open $in_fh, "<$DIR_NAME/4/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/4/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=75/i, '#64: parameter tempc is correct in non-hierarchical sweep def\'s 4/0.i' ); #64
like( $in_netlist, qr/\.model nmos.*tox=1\.15e-08/i, '#65: parameter tox is correct in non-hierarchical sweep def\'s 4/0.i' ); #65
like( $in_netlist, qr/m\.xinv\.mp out.*w=3\.3e-06/i, '#66: parameter w is correct in non-hierarchical sweep def\'s 4/0.i' ); #66
ok( -e "$DIR_NAME/5/0.i", "#67: specifically checked that $DIR_NAME/5/0.i exists" ); #67
open $in_fh, "<$DIR_NAME/5/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/5/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=100/i, '#68: parameter tempc is correct in non-hierarchical sweep def\'s 5/0.i' ); #68
like( $in_netlist, qr/\.model nmos.*tox=1\.1e-08/i, '#69: parameter tox is correct in non-hierarchical sweep def\'s 5/0.i' ); #69
like( $in_netlist, qr/m\.xinv\.mp out.*w=3\.6e-06/i, '#70: parameter w is correct in non-hierarchical sweep def\'s 5/0.i' ); #70

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-ls', "$DIR_NAME/0" );
run( \@run_cmd, \$in, \$out, \$err );

# TODO: These files are not identical, parameters are modified for variables on option line
#       But this is not a problem for multiple levels of sweep
#is( compare( "$NET_NAME.sti", "$DIR_NAME/0.sti" ), 0, 'simulation results of the nominal, 0.i netlist for non-hierarchical sweep def should be identical to the original netlist' );


# Run init_batch to create a batch of netlists for hierarchical sweep defs (two levels of sweep)
remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

@cmd = ( $SUT, '-u', './lib', $NET_NAME, $DIR_NAME, './examples/init_batch_def.sweep-hierarchy.txt' );
run( \@cmd, \$in, \$out, \$err );
ok( -e "$DIR_NAME/0.i", "#71: specifically checked that $DIR_NAME/0.i exists" ); #71
ok( -e "$DIR_NAME/27c_a/0.i", "#72: specifically checked that $DIR_NAME/27c_a/0.i exists" ); #72
open $in_fh, "<$DIR_NAME/27c_a/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/27c_a/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=27/i, '#73: parameter tempc is correct in hierarchical sweep def\'s 27c_a/0.i' ); #73
like( $in_netlist, qr/\.model nmos.*tox=12.5e-9/i, '#74: parameter tox is correct in hierarchical sweep def\'s 27c_a/0.i' ); #74
like( $in_netlist, qr/m\.xinv\.mp out.*w=2\.7e-06/i, '#75: parameter w is correct in hierarchical sweep def\'s 27c_a/0.i' ); #75
ok( -e "$DIR_NAME/27c_b/0.i", "#76: specifically checked that $DIR_NAME/27c_b/0.i exists" ); #76
open $in_fh, "<$DIR_NAME/27c_b/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/27c_b/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=27/i, '#77: parameter tempc is correct in hierarchical sweep def\'s 27c_b/0.i' ); #77
like( $in_netlist, qr/\.model nmos.*tox=1\.2e-08/i, '#78: parameter tox is correct in hierarchical sweep def\'s 27c_b/0.i' ); #78
like( $in_netlist, qr/m\.xinv\.mp out.*w=3e-06/i, '#79: parameter w is correct in hierarchical sweep def\'s 27c_b/0.i' ); #79
ok( -e "$DIR_NAME/27c_c/0.i", "#80: specifically checked that $DIR_NAME/27c_c/0.i exists" ); #80
open $in_fh, "<$DIR_NAME/27c_c/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/27c_c/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=27/i, '#81: parameter tempc is correct in hierarchical sweep def\'s 27c_c/0.i' ); #81
like( $in_netlist, qr/\.model nmos.*tox=1\.15e-08/i, '#82: parameter tox is correct in hierarchical sweep def\'s 27c_c/0.i' ); #82
like( $in_netlist, qr/m\.xinv\.mp out.*w=3\.3e-06/i, '#83: parameter w is correct in hierarchical sweep def\'s 27c_c/0.i' ); #83
ok( -e "$DIR_NAME/100c_a/0.i", "#84: specifically checked that $DIR_NAME/100c_a/0.i exists" ); #84
open $in_fh, "<$DIR_NAME/100c_a/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/100c_a/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=100/i, '#85: parameter tempc is correct in hierarchical sweep def\'s 100c_a/0.i' ); #85
like( $in_netlist, qr/\.model nmos.*tox=12.5e-9/i, '#86: parameter tox is correct in hierarchical sweep def\'s 100c_a/0.i' ); #86
like( $in_netlist, qr/m\.xinv\.mp out.*w=2\.7e-06/i, '#87: parameter w is correct in hierarchical sweep def\'s 100c_a/0.i' ); #87
ok( -e "$DIR_NAME/100c_b/0.i", "#88: specifically checked that $DIR_NAME/100c_b/0.i exists" ); #88
open $in_fh, "<$DIR_NAME/100c_b/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/100c_b/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=100/i, '#89: parameter tempc is correct in hierarchical sweep def\'s 100c_b/0.i' ); #89
like( $in_netlist, qr/\.model nmos.*tox=1\.2e-08/i, '#90: parameter tox is correct in hierarchical sweep def\'s 100c_b/0.i' ); #90
like( $in_netlist, qr/m\.xinv\.mp out.*w=3e-06/i, '#91: parameter w is correct in hierarchical sweep def\'s 100c_b/0.i' ); #91
ok( -e "$DIR_NAME/100c_c/0.i", "#92: specifically checked that $DIR_NAME/100c_c/0.i exists" ); #92
open $in_fh, "<$DIR_NAME/100c_c/0.i" or BAIL_OUT( "Error: Cannot open $DIR_NAME/100c_c/0.i for read due to: $!" );
@in_net = <$in_fh>;
$in_netlist = join '', @in_net;
close $in_fh;
like( $in_netlist, qr/\.param tempc=100/i, '#93: parameter tempc is correct in hierarchical sweep def\'s 100c_c/0.i' ); #93
like( $in_netlist, qr/\.model nmos.*tox=1\.15e-08/i, '#94: parameter tox is correct in hierarchical sweep def\'s 100c_c/0.i' ); #94
like( $in_netlist, qr/m\.xinv\.mp out.*w=3\.3e-06/i, '#95: parameter w is correct in hierarchical sweep def\'s 100c_c/0.i' ); #95

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-ls', "$DIR_NAME/0" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$NET_NAME.sti", "$DIR_NAME/0.sti" ), 0, '#96: simulation results of the nominal, 0.i netlist for hierarchical sweep def should be identical to the original netlist' ); #96

remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

if( -e "$NET_NAME.o" ) {
  unlink "$NET_NAME.o" or warn "Warning: Could not unlink '$NET_NAME.o' due to: $!";
}
if( -e "$NET_NAME.sti" ) {
  unlink "$NET_NAME.sti" or warn "Warning: Could not unlink '$NET_NAME.sti' due to: $!";
}
if( -e "$NET_NAME.i" ) {
  unlink "$NET_NAME.i" or warn "Warning: Could not unlink '$NET_NAME.i' due to: $!";
}

done_testing();

