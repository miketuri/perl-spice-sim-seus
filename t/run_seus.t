#!/usr/bin/perl -w
use strict;
use warnings;

use File::Copy;
use IPC::Run 'run';
use Test::More tests => 34;

# SEUS Package's run_seus.t:
#   Test script for run_seus.pl
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $SUT = './lib/run_seus.pl'; # Script Under Test
my $ORIG_NETLIST = './examples/amp-csrc-dc'; # Original netlist to copy for testing
my $NET_NAME = './t/run_seus'; # Copy of netlist to use for tests

# Procedure: remove_test_files
#
#   Delete files (if present) that will/could be generated during this test
sub remove_test_files {
  if( -e "$NET_NAME.o" ) {
    unlink "$NET_NAME.o" or warn "Warning: Could not unlink '$NET_NAME.o' due to: $!";
  }
  if( -e "$NET_NAME.raw" ) {
    unlink "$NET_NAME.raw" or warn "Warning: Could not unlink '$NET_NAME.raw' due to: $!";
  }
  if( -e "$NET_NAME.log" ) {
    unlink "$NET_NAME.log" or warn "Warning: Could not unlink '$NET_NAME.log' due to: $!";
  }
  if( -e "$NET_NAME.csv" ) {
    unlink "$NET_NAME.csv" or warn "Warning: Could not unlink '$NET_NAME.csv' due to: $!";
  }
  if( -e "$NET_NAME.dat" ) {
    unlink "$NET_NAME.dat" or warn "Warning: Could not unlink '$NET_NAME.dat' due to: $!";
  }
  if( -e "$NET_NAME.sti" ) {
    unlink "$NET_NAME.sti" or warn "Warning: Could not unlink '$NET_NAME.sti' due to: $!";
  }
  if( -e "$NET_NAME.txt" ) {
    unlink "$NET_NAME.txt" or warn "Warning: Could not unlink '$NET_NAME.txt' due to: $!";
  }
}


# Run with no arguments to get usage
remove_test_files();
my @cmd = ($SUT);
my $in;
my $out;
my $err;
run( \@cmd, \$in, \$out, \$err );
like( $err, qr/^Usage: run_seus.pl/, "$SUT should print usage if no arguments provided" ); #1
ok( !(-e "$NET_NAME.o"), "After running $SUT with no arguments, no .o output should exist" ); #2
ok( !(-e "$NET_NAME.raw"), "After running $SUT with no arguments, no .raw output should exist" ); #3
ok( !(-e "$NET_NAME.csv"), "After running $SUT with no arguments, no .csv output should exist" ); #4
ok( !(-e "$NET_NAME.dat"), "After running $SUT with no arguments, no .dat output should exist" ); #5
ok( !(-e "$NET_NAME.sti"), "After running $SUT with no arguments, no .sti output should exist" ); #6
ok( !(-e "$NET_NAME.txt"), "After running $SUT with no arguments, no .txt output should exist" ); #7

copy( "$ORIG_NETLIST.i", "$NET_NAME.i" ) or BAIL_OUT( "Error: Cannot copy file $ORIG_NETLIST.i due to: $!" );

# Run with -r to generate .raw output file and display license
remove_test_files();
@cmd = ( $SUT,  '-u', './lib', '-r', $NET_NAME );
run( \@cmd, \$in, \$out, \$err );
like( $out, qr/Copyright \(C\) 2009-2020/, "$SUT -r should print license since not suppressed" ); #8
like( $out, qr/Spice "$NET_NAME" died first \(status=0\)/, "$SUT -r should run simulation of netlist without error" ); #9
ok( -e "$NET_NAME.o", "After running simulation with $SUT -r, output .o should exist" ); #10
ok( -e "$NET_NAME.raw", "After running simulation with $SUT -r, output .raw should exist" ); #11
ok( !(-e "$NET_NAME.csv"), "After running simulation with $SUT -r, output .csv should not exist" ); #12
ok( !(-e "$NET_NAME.dat"), "After running simulation with $SUT -r, output .dat should not exist" ); #13
ok( !(-e "$NET_NAME.sti"), "After running simulation with $SUT -r, output .sti should not exist" ); #14
ok( !(-e "$NET_NAME.txt"), "After running simulation with $SUT -r, output .txt should not exist" ); #15


# Run with -lnrp run_seus.log to generate .raw output without displaying license,
#   without calling netgen_seus, and printing the simulation duration to '$NET_NAME.log'
remove_test_files();
@cmd = ( $SUT, '-u', './lib', '-lnrp', "$NET_NAME.log", $NET_NAME );
run( \@cmd, \$in, \$out, \$err );
unlike( $out, qr/Copyright \(C\) 2009-2020/, "$SUT should not print license since suppressed" ); #16
like( $out, qr/Spice "$NET_NAME" died first \(status=0\)/, "$SUT -lnrp should run simulation of netlist without error" ); #17
ok( -e "$NET_NAME.o", "After running simulation with $SUT -lnrp, output .o should exist" ); #18
ok( -e "$NET_NAME.raw", "After running simulation with $SUT -lnrp, output .raw should exist" ); #19
ok( !(-e "$NET_NAME.csv"), "After running simulation with $SUT -lnrp, output .csv should not exist" ); #20
ok( !(-e "$NET_NAME.dat"), "After running simulation with $SUT -lnrp, output .dat should not exist" ); #21
ok( !(-e "$NET_NAME.sti"), "After running simulation with $SUT -lnrp, output .sti should not exist" ); #22
ok( !(-e "$NET_NAME.txt"), "After running simulation with $SUT -lnrp, output .txt should not exist" ); #23
ok( -e "$NET_NAME.log", "After running simulation with $SUT -lnrp, $NET_NAME.log should exist" ); #24
ok( -r "$NET_NAME.log", "After running simulation with $SUT -lnrp, $NET_NAME.log should be readable" ); #25
open my $in_fh, "<$NET_NAME.log" or BAIL_OUT( "Error: Cannot open $NET_NAME.log for read due to: $!" );
my $log = <$in_fh>;
close $in_fh;
like( $log, qr/^$NET_NAME,/, "$SUT -lnrp should have written duration to $NET_NAME.log" ); #26


# Run with -mt to generate .o output only without calling mkout_seus, and not using a timer
remove_test_files();
@cmd = ( $SUT, '-u', './lib', '-mt', $NET_NAME );
run( \@cmd, \$in, \$out, \$err );
like( $out, qr/Copyright \(C\) 2009-2020/, "$SUT -mt should print license since not suppressed" ); #27
like( $out, qr/Spice "$NET_NAME" died first \(status=0\)/, "$SUT -mt should run simulation of netlist without error" ); #28
ok( -e "$NET_NAME.o", "After running simulation with $SUT -mt, output .o should exist" ); #29
ok( !(-e "$NET_NAME.raw"), "After running simulation with $SUT -mt, output .raw should not exist" ); #30
ok( !(-e "$NET_NAME.csv"), "After running simulation with $SUT -mt, output .csv should not exist" ); #31
ok( !(-e "$NET_NAME.dat"), "After running simulation with $SUT -mt, output .dat should not exist" ); #32
ok( !(-e "$NET_NAME.sti"), "After running simulation with $SUT -mt, output .sti should not exist" ); #33
ok( !(-e "$NET_NAME.txt"), "After running simulation with $SUT -mt, output .txt should not exist" ); #34

remove_test_files();

if( -e "$NET_NAME.i" ) {
  unlink "$NET_NAME.i" or warn "Warning: Could not unlink '$NET_NAME.i' due to: $!";
}


done_testing();

