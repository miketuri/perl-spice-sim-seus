#!/usr/bin/perl -w
use strict;
use warnings;

use IPC::Run 'run';
use Test::More tests => 12;

# SEUS Package's seus.t:
#   Test script for seus.pl
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $SUT = './lib/seus.pl'; # Script Under Test

# Commands/scripts to call
my $BatchExec = 'batchexec_seus.pl';
my $InitBatch = 'init_batch_seus.pl';
my $MakeOut = 'mkout_seus.pl';
my $NetGen = 'netgen_seus.pl';
my $Run = 'run_seus.pl';

# Run with no arguments to get usage
my @cmd = ($SUT);
my $in;
my $out;
my $err;
run( \@cmd, \$in, \$out, \$err );
like( $err, qr/^Usage: seus.pl/, "$SUT should print usage if no arguments provided" ); #1
like( $out, qr/Copyright \(C\) 2009-2020/, "$SUT should always print license" ); #2

SKIP: {
  skip 'Cannot test seus.pl --BatchExec unless SEUS Package is already installed', 2 unless -x $BatchExec;
  
  # Get usage from batchexec_seus (long name)
  @cmd = ( $SUT, '--BatchExec' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $BatchExec/, "$SUT --BatchExec should print $BatchExec\'s usage if no additional arguments provided" ); #3
  
  # Get usage from batchexec_seus (short name)
  @cmd = ( $SUT, '--B' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $BatchExec/, "$SUT --B should print $BatchExec\'s usage if no additional arguments provided" ); #4
}

SKIP: {
  skip 'Cannot test seus.pl --InitBatch unless SEUS Package is already installed', 2 unless -x $InitBatch;
  
  # Get usage from init_batch_seus (long name)
  @cmd = ( $SUT, '--INITBATCH' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $InitBatch/, "$SUT --INITBATCH should print $InitBatch\'s usage if no additional arguments provided" ); #5
  
  # Get usage from init_batch_seus (short name)
  @cmd = ( $SUT, '--i' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $InitBatch/, "$SUT --i should print $InitBatch\'s usage if no additional arguments provided" ); #6
}

SKIP: {
  skip 'Cannot test seus.pl --MakeOut unless SEUS Package is already installed', 2 unless -x $MakeOut;
  
  # Get usage from mkout_seus (long name)
  @cmd = ( $SUT, '-Makeout' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $MakeOut/, "$SUT -Makeout should print $MakeOut\'s usage if no additional arguments provided" ); #7
  
  # Get usage from mkout_seus (short name)
  @cmd = ( $SUT, '-M' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $MakeOut/, "$SUT -M should print $MakeOut\'s usage if no additional arguments provided" ); #8
}

SKIP: {
  skip 'Cannot test seus.pl --NetGen unless SEUS Package is already installed', 2 unless -x $NetGen;
  
  # Get usage from netgen_seus (long name)
  @cmd = ( $SUT, 'NetGen' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $NetGen/, "$SUT NetGen should print $NetGen\'s usage if no additional arguments provided" ); #9
  
  # Get usage from netgen_seus (short name)
  @cmd = ( $SUT, 'N' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $NetGen/, "$SUT N should print $NetGen\'s usage if no additional arguments provided" ); #10
}

SKIP: {
  skip 'Cannot test seus.pl --Run unless SEUS Package is already installed', 2 unless -x $Run;
  
  # Get usage from run_seus (long name)
  @cmd = ( $SUT, 'run' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $Run/, "$SUT run should print $Run\'s usage if no additional arguments provided" ); #11
  
  # Get usage from run_seus (short name)
  @cmd = ( $SUT, 'r' );
  run( \@cmd, \$in, \$out, \$err );
  like( $err, qr/Usage: $Run/, "$SUT r should print $Run\'s usage if no additional arguments provided" ); #12
}

done_testing();

