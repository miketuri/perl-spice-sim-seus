#!/usr/bin/perl -w
use strict;
use warnings;

use File::Path qw(make_path remove_tree);
use File::Copy;
use IPC::Run 'run';
use Test::More tests => 25;

# SEUS Package's batchexec_seus.t:
#   Test script for batchexec_seus.pl
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $SUT = './lib/batchexec_seus.pl'; # Script Under Test
my $NET_NAME = './examples/amp-csrc-dc'; # Netlist to use for tests
my $DIR_NAME = './t/batchexec_seus'; # Directory name to use for tests

# Procedure: make_test_dir
#
#   Make test directory structure with copies of netlists
sub make_test_dir {
  make_path( "$DIR_NAME/SUB" ) or BAIL_OUT( "Error: Cannot make dirs $DIR_NAME and $DIR_NAME/SUB due to: $!" );
  foreach (0..10) {
    copy( "$NET_NAME.i", "$DIR_NAME/$_.i" ) or BAIL_OUT( "Error: Cannot copy file $NET_NAME.i due to: $!" );
  }
  foreach (0..5) {
    copy( "$NET_NAME.i", "$DIR_NAME/SUB/$_.i" ) or BAIL_OUT( "Error: Cannot copy file $NET_NAME.i due to: $!" );
  }
}

# Procedure: check_dir
#
#   Check if the named directory contains the particular file types
#   First argument: Scalar ($) of the directory path
#   Second argument: Array reference (\@) containing the range of numbered files
#   Third argument: Scalar ($) of the file type to look for
#   Return value: Number of files that exist
sub check_dir {
  my $dir_path = shift;
  my $range = shift;
  my $filetype = shift;
  my $exist_count = 0;
  
  foreach (@{$range}) {
    if( -e "$dir_path/$_$filetype" ) { $exist_count++; }
  }
  return $exist_count;
}


my @a0_10 = (0..10);
my @a0_5 = (0..5);

# Run with no arguments to get usage
my @cmd = ($SUT);
my $in;
my $out;
my $err;
run( \@cmd, \$in, \$out, \$err );
like( $err, qr/^Usage: batchexec_seus.pl/, "$SUT should print usage if no arguments provided" ); #1


# Run to batchexec all netlists in first level of directory and display license
remove_tree( $DIR_NAME, {error => \my $rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

make_test_dir();

@cmd = ( $SUT, '-u', './lib', $DIR_NAME );
run( \@cmd, \$in, \$out, \$err );
like( $out, qr/Copyright \(C\) 2009-2020/, "$SUT should print license since not suppressed" ); #2
is( check_dir( $DIR_NAME, \@a0_10, '.o' ), 11, 'all netlists in first level of directory simulated and .o output exists' ); #3
is( check_dir( $DIR_NAME, \@a0_10, '.csv' ), 11, 'all netlists in first level of directory simulated and .csv output exists' ); #4
is( check_dir( $DIR_NAME, \@a0_10, '.dat' ), 11, 'all netlists in first level of directory simulated and .dat output exists' ); #5
is( check_dir( $DIR_NAME, \@a0_10, '.sti' ), 11, 'all netlists in first level of directory simulated and .sti output exists' ); #6
is( check_dir( $DIR_NAME, \@a0_10, '.txt' ), 11, 'all netlists in first level of directory simulated and .txt output exists' ); #7
is( check_dir( "$DIR_NAME/SUB", \@a0_5, '.o' ), 0, 'no netlists in subdirectory simulated and .o output does not exist' ); #8


# Run to batchexec a range of netlists in first level of directory, run run_seus with
#   the -r run option (just generate .raw file), and suppress displaying license
remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

make_test_dir();

@cmd = ( $SUT, '-u', './lib', '-lr', 'r', '-b', 3, '-e', 8, $DIR_NAME );
run( \@cmd, \$in, \$out, \$err );
unlike( $err, qr/Copyright \(C\) 2009-2020/, "$SUT should not print license since suppressed" ); #9
is( check_dir( $DIR_NAME, \@a0_10, '.o' ), 6, 'the specific range of netlists in first level of directory simulated and .o output exists' ); #10
ok( !(-e "$DIR_NAME/0.o"), "specifically checked that $DIR_NAME/0.o does not exist" ); #11
ok( !(-e "$DIR_NAME/1.o"), "specifically checked that $DIR_NAME/1.o does not exist" ); #12
ok( !(-e "$DIR_NAME/8.o"), "specifically checked that $DIR_NAME/9.o does not exist" ); #13
ok( !(-e "$DIR_NAME/9.o"), "specifically checked that $DIR_NAME/9.o does not exist" ); #14
ok( !(-e "$DIR_NAME/10.o"), "specifically checked that $DIR_NAME/10.o does not exist" ); #15
is( check_dir( $DIR_NAME, \@a0_10, '.raw' ), 6, 'the specific range of netlists in first level of directory simulated and .raw output exists' ); #16
is( check_dir( $DIR_NAME, \@a0_10, '.csv' ), 0, 'the specific range of netlists in first level of directory simulated and no .csv output exists' ); #17
is( check_dir( $DIR_NAME, \@a0_10, '.dat' ), 0, 'the specific range of netlists in first level of directory simulated and no .dat output exists' ); #18
is( check_dir( $DIR_NAME, \@a0_10, '.sti' ), 0, 'the specific range of netlists in first level of directory simulated and no .sti output exists' ); #19
is( check_dir( $DIR_NAME, \@a0_10, '.txt' ), 0, 'the specific range of netlists in first level of directory simulated and no .txt output exists' ); #20
is( check_dir( "$DIR_NAME/SUB", \@a0_5, '.o' ), 0, 'no netlists in subdirectory simulated with the specific range and .o output does not exist' ); #21


# Run to batchexec all netlists in a directory and its subdirectories (-s option),
#   except do not run 0.i simulations (-0 option), and only run two simulations in
#   parallel at a time (-p 2 option)
remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

make_test_dir();

@cmd = ( $SUT, '-u', './lib', '-0sp', 2, $DIR_NAME );
run( \@cmd, \$in, \$out, \$err );
is( check_dir( $DIR_NAME, \@a0_10, '.o' ), 10, 'all netlists but 0.i in first level of directory simulated and .o output exists (only ran 2 simulations ran in parallel at a time)' ); #22
ok( !(-e "$DIR_NAME/0.o"), "specifically checked that $DIR_NAME/0.o does not exist" ); #23
is( check_dir( "$DIR_NAME/SUB", \@a0_5, '.o' ), 5, 'all netlists but 0.i in subdirectory simulated and .o output exists (only ran 2 simulations ran in parallel at a time)' ); #24
ok( !(-e "$DIR_NAME/SUB/0.o"), "specifically checked that $DIR_NAME/SUB/0.o does not exist" ); #25


# Skipping testing -n N option (limiting the number of simulations to N)
#   If computer is fast and many simulations finish at once, more than N sims may finish

remove_tree( $DIR_NAME, {error => \$rmerr} );
warn "Could not remove test directory $DIR_NAME" if $rmerr && @$rmerr;

done_testing();

