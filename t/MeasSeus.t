#!/usr/bin/perl -w
use strict;
use warnings;

use File::Copy;
use Test::More tests => 58;

# SEUS Package's MeasSeus.t:
#   Test script for the MeasSeus.pm Perl module
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my @subs = qw(abs_waveform add_waveforms avg diff_waveforms get_signal_sti integ max min neg_waveform put_signal_sti sub_waveforms truncate_waveform xval yval);

use_ok( 'MeasSeus', @subs ); #1
can_ok( 'MeasSeus', @subs ); #2

my %testwave = (
  0 => 2,
  1 => -2,
  2 => -6,
  3 => 2,
  4 => 6,
  5 => 7,
  6 => 8,
  7 => 5.0625
#  8 => 0 # This key-value pair will be added after test #6
);

my $y3 = yval( \%testwave, 5 );
my $a3 = 7;
my $y4 = yval( \%testwave, 5.5 );
my $a4 = 7.5;
my $y5 = yval( \%testwave, 4.75 );
my $a5 = 6.75;

is( $y3, $a3, 'yval should find a y-value for an x-value present in the waveform hash' ); #3
is( $y4, $a4, 'yval should find a y-value using interpolation (x-value above midpoint)' ); #4
is( $y5, $a5, 'yval should find a y-value using interpolation (x-value below midpoint)' ); #5

my @x6 = xval( \%testwave, 2, '', '' );
my %t6;
truncate_waveform( \%testwave, '', '', \%t6 );
my $s6 = join ' ', @x6;
my $a6 = '0 3';

$testwave{8} = 0;

my @x7 = xval( \%testwave, -4, '', '' );
my %t15;
truncate_waveform( \%testwave, '', '', \%t15 );
my $s7 = join ' ', @x7;

my @x8 = xval( \%testwave, -4, '', 2 );
my %t16;
truncate_waveform( \%testwave, '', 2, \%t16 );
my $s8 = join ' ', @x8;

my @x9 = xval( \%testwave, -4, '', 1.8 );
my %t17;
truncate_waveform( \%testwave, '', 1.8, \%t17 );
my $s9 = join ' ', @x9;

my @x10 = xval( \%testwave, -4, 2, '' );
my %t18;
truncate_waveform( \%testwave, 2, '', \%t18 );
my $s10 = join ' ', @x10;

my @x11 = xval( \%testwave, -4, 2.125, '' );
my %t19;
truncate_waveform( \%testwave, 2.125, '', \%t19 );
my $s11 = join ' ', @x11;

my @x12 = xval( \%testwave, -4, 1, 4 );
my %t20;
truncate_waveform( \%testwave, 1, 4, \%t20 );
my $s12 = join ' ', @x12;

my @x13 = xval( \%testwave, -4, 0.25, 3.375 );
my %t21;
truncate_waveform( \%testwave, 0.25, 3.375, \%t21 );
my $s13 = join ' ', @x13;

my @x14 = xval( \%testwave, -4, 4.5, 5.5 );
my %t22;
truncate_waveform( \%testwave, 4.5, 5.5, \%t22 );
my $s14 = scalar @x14;

my $xval_all = '1.5 2.25';
my $xval1 = '1.5';
my $xval2 = '2.25';

is( $s6, $a6, 'xval should find two x-values for a y-value present in entire waveform hash' ); #6
is( $s7, $xval_all, 'xval should find two x-values using interpolation in entire waveform hash' ); #7
is( $s8, $xval1, 'xval should find one x-value using interpolation from beginning to x-value in waveform hash' ); #8
is( $s9, $xval1, 'xval should find one x-value using interpolation from beginning to interpolated x-value in waveform hash' ); #9
is( $s10, $xval2, 'xval should find one x-value using interpolation from x-value to end of waveform hash' ); #10
is( $s11, $xval2, 'xval should find one x-value using interpolation from interpolated x-value to end of waveform hash' ); #11
is( $s12, $xval_all, 'xval should find two x-values using interpolation from defined start and end x-values in waveform hash' ); #12
is( $s13, $xval_all, 'xval should find two x-values using interpolation from defined start and end x-values interpolated in waveform hash' ); #13
is( $s14, 0, 'xval should not find any x-values using interpolation from defined start and end x-values interpolated in waveform hash' ); #14

my %d15;
diff_waveforms( \%t15, '-trunc', \%testwave, '-sol', \%d15 );
cmp_ok( scalar keys %d15, '==', 0, 'truncate_waveform should just copy entire waveform' ); #15
my %s16 = (
  0 => 2,
  1 => -2,
  2 => -6
);
my %d16;
diff_waveforms( \%t16, '-trunc', \%s16, '-sol', \%d16 );
cmp_ok( scalar keys %d16, '==', 0, 'truncate_waveform should just truncate end of waveform' ); #16
my %s17 = (
  0 => 2,
  1 => -2,
  1.8 => -5.2
);
my %d17;
diff_waveforms( \%t17, '-trunc', \%s17, '-sol', \%d17 );
cmp_ok( scalar keys %d17, '==', 0, 'truncate_waveform should just truncate end of waveform' ); #17
my %s18 = (
  2 => -6,
  3 => 2,
  4 => 6,
  5 => 7,
  6 => 8,
  7 => 5.0625,
  8 => 0
);
my %d18;
diff_waveforms( \%t18, '-trunc', \%s18, '-sol', \%d18 );
cmp_ok( scalar keys %d18, '==', 0, 'truncate_waveform should just truncate beginning of waveform' ); #18
my %s19 = (
  2.125 => -5,
  3 => 2,
  4 => 6,
  5 => 7,
  6 => 8,
  7 => 5.0625,
  8 => 0
);
my %d19;
diff_waveforms( \%t19, '-trunc', \%s19, '-sol', \%d19 );
cmp_ok( scalar keys %d19, '==', 0, 'truncate_waveform should just truncate beginning of waveform' ); #19
my %s20 = (
  1 => -2,
  2 => -6,
  3 => 2,
  4 => 6
);
my %d20;
diff_waveforms( \%t20, '-trunc', \%s20, '-sol', \%d20 );
cmp_ok( scalar keys %d20, '==', 0, 'truncate_waveform should truncate beginning and end of waveform' ); #20
my %s21 = (
  0.25 => 1,
  1 => -2,
  2 => -6,
  3 => 2,
  3.375 => 3.5
);
my %d21;
diff_waveforms( \%t21, '-trunc', \%s21, '-sol', \%d21 );
cmp_ok( scalar keys %d21, '==', 0, 'truncate_waveform should truncate beginning and end of waveform' ); #21
my %s22 = (
  4.5 => 6.5,
  5 => 7,
  5.5 => 7.5
);
my %d22;
diff_waveforms( \%t22, '-trunc', \%s22, '-sol', \%d22 );
cmp_ok( scalar keys %d22, '==', 0, 'truncate_waveform should truncate beginning and end of waveform' ); #22

my %w23;
neg_waveform( \%testwave, \%w23 );
my %s23 = (
  0 => -2,
  1 => 2,
  2 => 6,
  3 => -2,
  4 => -6,
  5 => -7,
  6 => -8,
  7 => -5.0625,
  8 => 0
);
my %d23;
diff_waveforms( \%w23, '-neg', \%s23, '-sol', \%d23 );
cmp_ok( scalar keys %d23, '==', 0, 'neg_waveform should multiply each y-value by -1' ); #23

my %w24 = %testwave;
abs_waveform( \%w24 );
my %s24 = (
  0 => 2,
  1 => 2,
  2 => 6,
  3 => 2,
  4 => 6,
  5 => 7,
  6 => 8,
  7 => 5.0625,
  8 => 0
);
my %d24;
diff_waveforms( \%w24, '-abs', \%s24, '-sol', \%d24 );
cmp_ok( scalar keys %d24, '==', 0, 'abs_waveform should take the absoulute value of each y-value' ); #24

my $v25 = min( \%testwave, '', '' );
my $a25 = -6;
my $v26 = min( \%testwave, '', 2 );
my $a26 = -6;
my $v27 = min( \%testwave, '', 1.5 );
my $a27 = -4;
my $v28 = min( \%testwave, 1, '' );
my $a28 = -6;
my $v29 = min( \%testwave, 2.5, '' );
my $a29 = -2;
my $v30 = min( \%testwave, 3.1, '' );
my $a30 = 0;
my $v31 = min( \%testwave, 1, 3 );
my $a31 = -6;
my $v32 = min( \%testwave, 1.8, 2.2 );
my $a32 = -6;

is( $v25, $a25, 'find min y-value of entire waveform hash' ); #25
is( $v26, $a26, 'find min y-value from beginning to x-value of waveform hash' ); #26
is( $v27, $a27, 'find min y-value from beginning to interpolated x-value of waveform hash' ); #27
is( $v28, $a28, 'find min y-value from x-value to end of waveform hash' ); #28
is( $v29, $a29, 'find min y-value from interpolated x-value to end of waveform hash' ); #29
is( $v30, $a30, 'find min y-value from interpolated x-value to end of waveform hash' ); #30
is( $v31, $a31, 'find min y-value from defined start and end x-values in waveform hash' ); #31
is( $v32, $a32, 'find min y-value from interpolated from defined start and end x-values in waveform hash' ); #32

my $v33 = max( \%testwave, '', '' );
my $a33 = 8;
my $v34 = max( \%testwave, '', 2 );
my $a34 = 2;
my $v35 = max( \%testwave, '', 3.5 );
my $a35 = 4;
my $v36 = max( \%testwave, 4, '' );
my $a36 = 8;
my $v37 = max( \%testwave, 4.4, '' );
my $a37 = 8;
my $v38 = max( \%testwave, 6.5, '' );
my $a38 = 6.53125;
my $v39 = max( \%testwave, 5, 7 );
my $a39 = 8;
my $v40 = max( \%testwave, 5.5, 6.1 );
my $a40 = 8;

is( $v33, $a33, 'find max y-value of entire waveform hash' ); #33
is( $v34, $a34, 'find max y-value from beginning to x-value of waveform hash' ); #34
is( $v35, $a35, 'find max y-value from beginning to interpolated x-value of waveform hash' ); #35
is( $v36, $a36, 'find max y-value from x-value to end of waveform hash' ); #36
is( $v37, $a37, 'find max y-value from interpolated x-value to end of waveform hash' ); #37
is( $v38, $a38, 'find max y-value from interpolated x-value to end of waveform hash' ); #38
is( $v39, $a39, 'find max y-value from defined start and end x-values in waveform hash' ); #39
is( $v40, $a40, 'find max y-value from interpolated from defined start and end x-values in waveform hash' ); #40

my $v41 = integ( \%testwave, 4, 6 );
my $a41 = 14;
my $v42 = integ( \%testwave, 4.75, 5.5 );
my $a42 = 5.34375;

is( $v41, $a41, 'integrate waveform from defined start and end x-values in waveform hash' ); #41
is( $v42, $a42, 'integrate waveform from interpolated, defined start and end x-values in waveform hash' ); #42

my $v43 = avg( \%testwave, 4, 6 );
my $a43 = 7;
my $v44 = avg( \%testwave, 4.75, 5.5 );
my $a44 = 7.125;

is( $v43, $a43, 'average waveform from defined start and end x-values in waveform hash' ); #43
is( $v44, $a44, 'average waveform from interpolated, defined start and end x-values in waveform hash' ); #44

my %wave1 = (
  0 => 1,
  1 => 2,
  2 => 3,
  3 => 4,
  4 => 5,
  5 => 6,
  6 => 7,
  7 => 8,
  8 => 9
);

my %wave2 = (
  0 => 9,
  1 => 8,
  2 => 7,
  3 => 6,
  4 => 5,
  4.5 => 4,
  5 => 3,
  5.5 => 2,
  6 => 1
);

my %w45;
add_waveforms( \%testwave, \%wave1, \%w45 );
my %w46;
add_waveforms( \%wave1, \%testwave, \%w46 );
my %s45_46 = (
  0 => 3,
  1 => 0,
  2 => -3,
  3 => 6,
  4 => 11,
  5 => 13,
  6 => 15,
  7 => 13.0625,
  8 => 9
);
my %d45;
my %d46;
diff_waveforms( \%w45, '-sum', \%s45_46, '-sol', \%d45 );
diff_waveforms( \%w46, '-sum', \%s45_46, '-sol', \%d46 );

my %w47;
add_waveforms( \%testwave, \%wave2, \%w47 );
my %w48;
add_waveforms( \%wave2, \%testwave, \%w48 );
my %s47_48 = (
  0 => 11,
  1 => 6,
  2 => 1,
  3 => 8,
  4 => 11,
  4.5 => 10.5,
  5 => 10,
  5.5 => 9.5,
  6 => 9
);
my %d47;
my %d48;
diff_waveforms( \%w47, '-sum', \%s47_48, '-sol', \%d47 );
diff_waveforms( \%w48, '-sum', \%s47_48, '-sol', \%d48 );

cmp_ok( scalar keys %d45, '==', 0, 'add_waveform, without interpolation, should add each y-value to the other corresponding y-value' ); #45
cmp_ok( scalar keys %d46, '==', 0, 'add_waveform, without interpolation, should add each y-value to the other corresponding y-value' ); #46
cmp_ok( scalar keys %d47, '==', 0, 'add_waveform, with interpolation, should add each y-value to the other corresponding y-value and interpolated y-values and truncate values for length mismatch' ); #47
cmp_ok( scalar keys %d48, '==', 0, 'add_waveform, with interpolation, should add each y-value to the other corresponding y-value and interpolated y-values and truncate values for length mismatch' ); #48

my %wave3 = (
  4 => 5,
  4.5 => 4,
  5 => 3,
  5.5 => 2,
  6 => 1,
  7 => 0,
  8 => -1
);

my %w49;
sub_waveforms( \%testwave, \%wave1, \%w49 );
my %s49 = (
  0 => 1,
  1 => -4,
  2 => -9,
  3 => -2,
  4 => 1,
  5 => 1,
  6 => 1,
  7 => -2.9375,
  8 => -9
);
my %d49;
diff_waveforms( \%w49, '-sub', \%s49, '-sol', \%d49 );

my %w50;
sub_waveforms( \%wave1, \%testwave, \%w50 );
my %s50;
neg_waveform( \%s49, \%s50 );
my %d50;
diff_waveforms( \%w50, '-sub', \%s50, '-sol', \%d50 );

my %w51;
sub_waveforms( \%testwave, \%wave3, \%w51 );
my %s51 = (
  4 => 1,
  4.5 => 2.5,
  5 => 4,
  5.5 => 5.5,
  6 => 7,
  7 => 5.0625,
  8 => 1
);
my %d51;
diff_waveforms( \%w51, '-sub', \%s51, '-sol', \%d51 );

my %w52;
sub_waveforms( \%wave3, \%testwave, \%w52 );
my %s52;
neg_waveform( \%s51, \%s52 );
my %d52;
diff_waveforms( \%w52, '-sub', \%s52, '-sol', \%d52 );

cmp_ok( scalar keys %d49, '==', 0, 'sub_waveform, without interpolation, should subtract each y-value to the other corresponding y-value' ); #49
cmp_ok( scalar keys %d50, '==', 0, 'sub_waveform, without interpolation, should subtract each y-value to the other corresponding y-value' ); #50
cmp_ok( scalar keys %d51, '==', 0, 'sub_waveform, with interpolation, should subtract each y-value to the other corresponding y-value and interpolated y-values and truncate values for length mismatch' ); #51
cmp_ok( scalar keys %d52, '==', 0, 'sub_waveform, with interpolation, should subtract each y-value to the other corresponding y-value and interpolated y-values and truncate values for length mismatch' ); #52

$wave3{3} = 3; # Add point to wave 3
my %d53;
diff_waveforms( \%wave2, '-w2', \%wave3, '-w3', \%d53 );
cmp_ok( scalar keys %d53, '==', 7, 'diff_waveform should find differences between different waveforms' ); #53
my %s54 = (
  '0-w2' => 9,
  '1-w2' => 8,
  '2-w2' => 7,
  '3-w2' => 6,
  '3-w3' => 3,
  '7-w3' => 0,
  '8-w3' => -1
);
my %d54;
diff_waveforms( \%d53, '-w2', \%s54, '-w3', \%d54 );
cmp_ok( scalar keys %d54, '==', 0, 'diff_waveform found the expected differences between different waveforms' ); #54

BAIL_OUT( 'Cannot find ./t/MeasSeus.sti for testing .sti subroutines' ) unless -e './t/MeasSeus.sti';
BAIL_OUT( 'Cannot read ./t/MeasSeus.sti for testing .sti subroutines' ) unless -r './t/MeasSeus.sti';

open my $in_fh, "<./t/MeasSeus.sti" or BAIL_OUT( "Error: Cannot open ./t/MeasSeus.sti for read due to: $!" );
my %w55;
get_signal_sti( $in_fh, 'filewave', 'V', \%w55 );
close $in_fh;
my %s55 = (
  '0' => -2,
  '1' => -1,
  '2' => 0,
  '2.5' => 1,
  '3' => 2
);
my %d55;
diff_waveforms( \%w55, '-file', \%s55, '-sol', \%d55 );
cmp_ok( scalar keys %d55, '==', 0, 'get_signal_sti read the signal from file correctly' ); #55

copy( './t/MeasSeus.sti', './t/MeasSeus.sti.2' ) or BAIL_OUT( "Error: Cannot copy file ./t/MeasSeus.sti due to: $!" );
open my $app_fh, ">>./t/MeasSeus.sti.2" or BAIL_OUT( "Error: Cannot open ./t/MeasSeus.sti.2 for append due to: $!" );
put_signal_sti( $app_fh, 'wave1', 'V', \%wave1 );
put_signal_sti( $app_fh, 'wave2', 'I', \%wave2 );
close $app_fh;

open $in_fh, "<./t/MeasSeus.sti.2" or BAIL_OUT( "Error: Cannot open ./t/MeasSeus.sti.2 for read due to: $!" );
my %w56;
get_signal_sti( $in_fh, 'wave1', 'V', \%w56 );
my %w57;
get_signal_sti( $in_fh, 'wave2', 'I', \%w57 );
my %w58;
get_signal_sti( $in_fh, 'filewave', 'V', \%w58 );
close $in_fh;
my %d56;
diff_waveforms( \%w56, '-file', \%wave1, '-sol', \%d56 );
cmp_ok( scalar keys %d56, '==', 0, 'put_ and get_signal_sti wrote and read the signal to/from file correctly' ); #56
my %d57;
diff_waveforms( \%w57, '-file', \%wave2, '-sol', \%d57 );
cmp_ok( scalar keys %d57, '==', 0, 'put_ and get_signal_sti wrote and read the signal to/from file correctly' ); #57
my %d58;
diff_waveforms( \%w58, '-file', \%s55, '-sol', \%d58 );
cmp_ok( scalar keys %d58, '==', 0, 'get_signal_sti read the signal from file correctly' ); #58
unlink './t/MeasSeus.sti.2' or warn "Warning: Could not unlink './t/MeasSeus.sti.2' due to: $!";

done_testing();

