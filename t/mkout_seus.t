#!/usr/bin/perl -w
use strict;
use warnings;

use IPC::Run 'run';
use Test::More tests => 53;

# SEUS Package's mkout_seus.t:
#   Test script for mkout_seus.pl
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $SUT = './lib/mkout_seus.pl'; # Script Under Test
my $DC_OUT = './t/mkout_seus.dc'; # The .o output file (deckname) from a DC simulation
my $TRAN_OUT = './t/mkout_seus.tran'; # The .o output file (deckname) from a transient simulation

# Procedure: remove_test_files
#
#   Delete files (if present) that will/could be generated during this test.
#   Need to pass the deckname (filename without filetype) as an input argument.
sub remove_test_files {
  my $file = shift;
  if( -e "$file.csv" ) {
    unlink "$file.csv" or warn "Warning: Could not unlink '$file.csv' due to: $!";
  }
  if( -e "$file.dat" ) {
    unlink "$file.dat" or warn "Warning: Could not unlink '$file.dat' due to: $!";
  }
  if( -e "$file.sti" ) {
    unlink "$file.sti" or warn "Warning: Could not unlink '$file.sti' due to: $!";
  }
  if( -e "$file.txt" ) {
    unlink "$file.txt" or warn "Warning: Could not unlink '$file.txt' due to: $!";
  }
}


# Run with no arguments to get usage
remove_test_files( $DC_OUT );
remove_test_files( $TRAN_OUT );
my @cmd = ($SUT);
my $in;
my $out;
my $err;
run( \@cmd, \$in, \$out, \$err );
like( $err, qr/^Usage: mkout_seus.pl/, "$SUT should print usage if no arguments provided" ); #1
ok( !(-e "$DC_OUT.csv"), "After running $SUT with no arguments, no .csv output should exist" ); #2
ok( !(-e "$DC_OUT.dat"), "After running $SUT with no arguments, no .dat output should exist" ); #3
ok( !(-e "$DC_OUT.sti"), "After running $SUT with no arguments, no .sti output should exist" ); #4
ok( !(-e "$DC_OUT.txt"), "After running $SUT with no arguments, no .txt output should exist" ); #5
ok( !(-e "$TRAN_OUT.csv"), "After running $SUT with no arguments, no .csv output should exist" ); #6
ok( !(-e "$TRAN_OUT.dat"), "After running $SUT with no arguments, no .dat output should exist" ); #7
ok( !(-e "$TRAN_OUT.sti"), "After running $SUT with no arguments, no .sti output should exist" ); #8
ok( !(-e "$TRAN_OUT.txt"), "After running $SUT with no arguments, no .txt output should exist" ); #9


# Run with no options to generate all output file types from DC simulation and display license
remove_test_files( $DC_OUT );
@cmd = ( $SUT, $DC_OUT );
run( \@cmd, \$in, \$out, \$err );
like( $out, qr/Copyright \(C\) 2009-2020/, "$SUT should print license since not suppressed" ); #10
like( $out, qr/Plotted signals for "$DC_OUT" are: Voltage V\(in\) V\(out\) I\(vdd\) I\(vss\)/, "$SUT should generate output file types of DC netlist without error" ); #11
ok( -e "$DC_OUT.csv", "After running $SUT, DC output .csv should exist" ); #12
ok( -e "$DC_OUT.dat", "After running $SUT, DC output .dat should exist" ); #13
ok( -e "$DC_OUT.sti", "After running $SUT, DC output .sti should exist" ); #14
ok( -e "$DC_OUT.txt", "After running $SUT, DC output .txt should exist" ); #15


# Run with -l option to generate all output file types from transient simulation and suppress displaying license
remove_test_files( $TRAN_OUT );
@cmd = ( $SUT, '-l', $TRAN_OUT );
run( \@cmd, \$in, \$out, \$err );
unlike( $out, qr/Copyright \(C\) 2009-2020/, "$SUT -l should not print license since suppressed" ); #16
like( $out, qr/Plotted signals for "$TRAN_OUT" are: Time V\(en\) V\(nandout\) V\(inv1out\) V\(inv2out\) I\(vdd\) I\(vss\)/, "$SUT -l should generate output file types of transient netlist without error" ); #17
ok( -e "$TRAN_OUT.csv", "After running $SUT -l, transient output .csv should exist" ); #18
ok( -e "$TRAN_OUT.dat", "After running $SUT -l, transient output .dat should exist" ); #19
ok( -e "$TRAN_OUT.sti", "After running $SUT -l, transient output .sti should exist" ); #20
ok( -e "$TRAN_OUT.txt", "After running $SUT -l, transient output .txt should exist" ); #21


# Run with -c option to generate only .csv output from DC simulation
remove_test_files( $DC_OUT );
@cmd = ( $SUT, '-c', $DC_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( -e "$DC_OUT.csv", "After running $SUT -c, DC output .csv should exist" ); #22
ok( !(-e "$DC_OUT.dat"), "After running $SUT -c, DC output .dat should not exist" ); #23
ok( !(-e "$DC_OUT.sti"), "After running $SUT -c, DC output .sti should not exist" ); #24
ok( !(-e "$DC_OUT.txt"), "After running $SUT -c, DC output .txt should not exist" ); #25


# Run with -c option to generate only .csv output from transient simulation
remove_test_files( $TRAN_OUT );
@cmd = ( $SUT, '-c', $TRAN_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( -e "$TRAN_OUT.csv", "After running $SUT -c, transient output .csv should exist" ); #26
ok( !(-e "$TRAN_OUT.dat"), "After running $SUT -c, transient output .dat should not exist" ); #27
ok( !(-e "$TRAN_OUT.sti"), "After running $SUT -c, transient output .sti should not exist" ); #28
ok( !(-e "$TRAN_OUT.txt"), "After running $SUT -c, transient output .txt should not exist" ); #29


# Run with -d option to generate only .dat output from DC simulation
remove_test_files( $DC_OUT );
@cmd = ( $SUT, '-d', $DC_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( !(-e "$DC_OUT.csv"), "After running $SUT -d, DC output .csv should not exist" ); #30
ok( -e "$DC_OUT.dat", "After running $SUT -d, DC output .dat should exist" ); #31
ok( !(-e "$DC_OUT.sti"), "After running $SUT -d, DC output .sti should not exist" ); #32
ok( !(-e "$DC_OUT.txt"), "After running $SUT -d, DC output .txt should not exist" ); #33


# Run with -d option to generate only .dat output from transient simulation
remove_test_files( $TRAN_OUT );
@cmd = ( $SUT, '-d', $TRAN_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( !(-e "$TRAN_OUT.csv"), "After running $SUT -d, transient output .csv should not exist" ); #34
ok( -e "$TRAN_OUT.dat", "After running $SUT -d, transient output .dat should exist" ); #35
ok( !(-e "$TRAN_OUT.sti"), "After running $SUT -d, transient output .sti should not exist" ); #36
ok( !(-e "$TRAN_OUT.txt"), "After running $SUT -d, transient output .txt should not exist" ); #37


# Run with -s option to generate only .sti output from DC simulation
remove_test_files( $DC_OUT );
@cmd = ( $SUT, '-s', $DC_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( !(-e "$DC_OUT.csv"), "After running $SUT -s, DC output .csv should not exist" ); #38
ok( !(-e "$DC_OUT.dat"), "After running $SUT -s, DC output .dat should not exist" ); #39
ok( -e "$DC_OUT.sti", "After running $SUT -s, DC output .sti should exist" ); #40
ok( !(-e "$DC_OUT.txt"), "After running $SUT -s, DC output .txt should not exist" ); #41


# Run with -s option to generate only .sti output from transient simulation
remove_test_files( $TRAN_OUT );
@cmd = ( $SUT, '-s', $TRAN_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( !(-e "$TRAN_OUT.csv"), "After running $SUT -s, transient output .csv should not exist" ); #42
ok( !(-e "$TRAN_OUT.dat"), "After running $SUT -s, transient output .dat should not exist" ); #43
ok( -e "$TRAN_OUT.sti", "After running $SUT -s, transient output .sti should exist" ); #42
ok( !(-e "$TRAN_OUT.txt"), "After running $SUT -s, transient output .txt should not exist" ); #45


# Run with -x option to generate only .txt output from DC simulation
remove_test_files( $DC_OUT );
@cmd = ( $SUT, '-x', $DC_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( !(-e "$DC_OUT.csv"), "After running $SUT -x, DC output .csv should not exist" ); #46
ok( !(-e "$DC_OUT.dat"), "After running $SUT -x, DC output .dat should not exist" ); #47
ok( !(-e "$DC_OUT.sti"), "After running $SUT -x, DC output .sti should not exist" ); #48
ok( -e "$DC_OUT.txt", "After running $SUT -x, DC output .txt should exist" ); #49


# Run with -x option to generate only .txt output from transient simulation
remove_test_files( $TRAN_OUT );
@cmd = ( $SUT, '-x', $TRAN_OUT );
run( \@cmd, \$in, \$out, \$err );
ok( !(-e "$TRAN_OUT.csv"), "After running $SUT -x, transient output .csv should not exist" ); #50
ok( !(-e "$TRAN_OUT.dat"), "After running $SUT -x, transient output .dat should not exist" ); #51
ok( !(-e "$TRAN_OUT.sti"), "After running $SUT -x, transient output .sti should not exist" ); #52
ok( -e "$TRAN_OUT.txt", "After running $SUT -x, transient output .txt should exist" ); #53

remove_test_files( $DC_OUT );
remove_test_files( $TRAN_OUT );


done_testing();

