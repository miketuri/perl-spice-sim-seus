#!/usr/bin/perl -w
use strict;
use warnings;

use File::Copy;
use File::Compare;
use IPC::Run 'run';
use Test::More tests => 46;

# SEUS Package's netgen_seus.t:
#   Test script for netgen_seus.pl
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $SUT = './lib/netgen_seus.pl'; # Script Under Test
my $NET_NAME = './examples/amp-csrc-dc'; # Netlist to use for tests
my $DECK_NAME = './t/netgen_seus'; # The deckname to use for copies of the test netlist

# Procedure: remove_test_files
#
#   Delete files (if present) that will/could be generated during this test.
sub remove_test_files {

  my @files = ( "$DECK_NAME.i",
                "$DECK_NAME.o",
                "$DECK_NAME.sti",
                "$DECK_NAME.gen.i",
                "$DECK_NAME.gen.o",
                "$DECK_NAME.gen.sti",
                "$DECK_NAME.gen-np.i",
                "$DECK_NAME.gen-np.o",
                "$DECK_NAME.gen-np.sti",
                "$DECK_NAME.gen.2.i",
                "$DECK_NAME.gen.3.i",
                "$DECK_NAME.gen.3.o",
                "$DECK_NAME.gen.3.sti",
                "$DECK_NAME.gen-np.3.i",
                "$DECK_NAME.gen-np.3.o",
                "$DECK_NAME.gen-np.3.sti",
                "$DECK_NAME.gen.4.i",
                "$DECK_NAME.gen.4.o",
                "$DECK_NAME.gen.4.sti"
              );

  foreach (@files) {
    if( -e ) {
      unlink or warn "Warning: Could not unlink $_ due to: $!";
    }
  }
}


# Run with no arguments to get usage and print license
remove_test_files();
my @cmd = ($SUT);
my $in;
my $out;
my $err;
run( \@cmd, \$in, \$out, \$err );
like( $err, qr/Usage: netgen_seus.pl/, "#1: $SUT should print usage if no arguments provided" ); #1
like( $err, qr/Copyright \(C\) 2009-2020/, "#2: $SUT should print license since not suppressed" ); #2

copy( "$NET_NAME.i", "$DECK_NAME.i" ) or BAIL_OUT( "Error: Cannot copy file $NET_NAME.i due to: $!" );


# Run with -i and -o to specify input netlist and location for generated netlist, and
#   run with -l to suppress printing license
@cmd = ( $SUT, '-li', "$DECK_NAME.i", '-o', "$DECK_NAME.gen.i" );
run( \@cmd, \$in, \$out, \$err );
unlike( $err, qr/Copyright \(C\) 2009-2020/, "#3: $SUT should not print license since suppressed" ); #3

open my $in_fh, "<$DECK_NAME.gen.i" or BAIL_OUT( "Error: Cannot open $DECK_NAME.gen.i for read due to: $!" );
my @in_gen = <$in_fh>;
my $gen_netlist = join '', @in_gen;
close $in_fh;

is( $gen_netlist, $out, '#4: generated netlist is output to STDOUT and output file with -o option' ); #4

unlike( $gen_netlist, qr/\.param\s+/i, '#5: generated netlist does not have any .param declarations since parameters are substituted' ); #5
like( $out, qr/\s+\.SUBCKT amp inp out vcc vgnd\s+/, '#6: generated netlist still has subcircuit definition (.SUBCKT) since subcircuits are not expanded' ); #6
like( $out, qr/\s+mp out out vcc  vcc  pmos l=\{0\.35u\} w=\{3u\} m=\{3\}\s+/, '#7: generated netlist still has original mosfet transistor "mp" since fingers are not expanded and subcircuits are not expanded' ); #7
unlike( $out, qr/\s+mp\.\d+ /, '#8: generated netlist does not have any mosfet transistor fingers for "mp" since fingers are not expanded' ); #8
like( $out, qr/\s+mn out inp vgnd vgnd nmos l=\{0\.35u\} w=\{3u\} m=2/, '#9: generated netlist still has original mosfet transistor "mn" since fingers are not expanded and subcircuits are not expanded' ); #9
unlike( $out, qr/\s+mn\.\d+ /, '#10: generated netlist does not have any mosfet transistor fingers for "mn" since fingers are not expanded' ); #10
like( $out, qr/\s+\.ENDS amp\s+/, '#11: generated netlist still has subcircuit definition (.ENDS) since subcircuits are not expanded' ); #11
like( $out, qr/\s+xinv in out vdd vss amp\s+/, '#12: generated netlist still has subcircuit instantiation since subcircuits are not expanded' ); #12

my @run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', $DECK_NAME );
run( \@run_cmd, \$in, \$out, \$err );
@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', "$DECK_NAME.gen" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$DECK_NAME.sti", "$DECK_NAME.gen.sti" ), 0, '#13: simulation results of the generated netlist should be identical to the original netlist' ); #13


# Run with -i and -o to specify input netlist and location for generated netlist, -l to
#   suppress printing license, and run with -p to not perform parameter substitutions on
#   the netlist
@cmd = ( $SUT, '-lpi', "$DECK_NAME.i", '-o', "$DECK_NAME.gen-np.i" );
run( \@cmd, \$in, \$out, \$err );

open $in_fh, "<$DECK_NAME.gen-np.i" or BAIL_OUT( "Error: Cannot open $DECK_NAME.gen-np.i for read due to: $!" );
@in_gen = <$in_fh>;
my $gen_netlist_np = join '', @in_gen;
close $in_fh;

is( $gen_netlist_np, $out, '#14: generated netlist is output to STDOUT and output file with -o option and no .param substitution (-p option)' ); #14

like( $gen_netlist_np, qr/\.param\s+/i, '#15: generated netlist still has .param declarations since parameters are not substituted' ); #15

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', $DECK_NAME );
run( \@run_cmd, \$in, \$out, \$err );
@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', "$DECK_NAME.gen-np" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$DECK_NAME.sti", "$DECK_NAME.gen-np.sti" ), 0, '#16: simulation results of the generated netlist without .param substitution should be identical to the original netlist' ); #16


# Run with -o to specify location for generated netlist, and -s to not print netlist to
#   STDOUT, and -z to suppress printing to STDERR.  STDIN will be used for the input netlist.
open $in_fh, "<$DECK_NAME.i" or BAIL_OUT( "Error: Cannot open $DECK_NAME.i for read due to: $!" );
my @in_net = <$in_fh>;
my $in_netlist = join '', @in_net;
close $in_fh;

@cmd = ( $SUT, '-zso', "$DECK_NAME.gen.2.i" );
run( \@cmd, \$in_netlist, \$out, \$err );
is( $out, '', "#17: $SUT -zso should not print to STDOUT since suppressed" ); #17
is( $err, '', "#18: $SUT -zso should not print to STDERR since suppressed" ); #18
is( compare( "$DECK_NAME.gen.i", "$DECK_NAME.gen.2.i" ), 0, '#19: generating netlist from STDIN should produce same netlist as generating from input file with -i' ); #19


# Run with -i and -o to specify input netlist and location for generated netlist, and
#   run with -f to expand mosfet transistor fingers in the netlist (note, parameters are 
#   substituted)
@cmd = ( $SUT, '-fli', "$DECK_NAME.i", '-o', "$DECK_NAME.gen.3.i" );
run( \@cmd, \$in, \$out, \$err );

unlike( $out, qr/\s+mp /, '#20: generated netlist does not have original mosfet transistor "mp" when its fingers are expanded' ); #20
like( $out, qr/\s+mp\.1 out out vcc  vcc  pmos l=\{0\.35u\} w=\{3u\} m=1\s+/, '#21: generated netlist has first mosfet transistor finger for "mp" when its fingers are expanded' ); #21
like( $out, qr/\s+mp\.2 out out vcc  vcc  pmos l=\{0\.35u\} w=\{3u\} m=1\s+/, '#22: generated netlist has second mosfet transistor finger for "mp" when its fingers are expanded' ); #22
like( $out, qr/\s+mp\.3 out out vcc  vcc  pmos l=\{0\.35u\} w=\{3u\} m=1\s+/, '#23: generated netlist has third mosfet transistor finger for "mp" when its fingers are expanded' ); #23
unlike( $out, qr/\s+mp\.[^123]\S* /, '#24: generated netlist only has three mosfet transistor fingers for "mp" when its fingers are expanded' ); #24
unlike( $out, qr/\smn /, '#25: generated netlist does not have original mosfet transistor "mn" when its fingers are expanded' ); #25
like( $out, qr/\s+mn\.1 out inp vgnd vgnd nmos l=\{0\.35u\} w=\{3u\} m=1\s+/, '#26: generated netlist has first mosfet transistor finger for "mn" when its fingers are expanded' ); #26
like( $out, qr/\s+mn\.2 out inp vgnd vgnd nmos l=\{0\.35u\} w=\{3u\} m=1\s+/, '#27: generated netlist has second mosfet transistor finger for "mn" when its fingers are expanded' ); #27
unlike( $out, qr/\s+mn\.[^12]\S* /, '#28: generated netlist only has two mosfet transistor fingers for "mn" when its fingers are expanded' ); #28

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', "$DECK_NAME.gen.3" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$DECK_NAME.sti", "$DECK_NAME.gen.3.sti" ), 0, '#29: simulation results of the generated netlist (with mosfet fingers expanded) should be identical to the original netlist' ); #29


# Run with -i and -o to specify input netlist and location for generated netlist, -f to
#   expand mosfet transistor fingers in the netlist, and run with -p to not perform
#   parameter substitutions on the netlist
@cmd = ( $SUT, '-flpi', "$DECK_NAME.i", '-o', "$DECK_NAME.gen-np.3.i" );
run( \@cmd, \$in, \$out, \$err );

like( $out, qr/\s+mp out out vcc  vcc  pmos l=\{l\} w=\{w\} m=\{pfingers\}\s+/, '#30: generated netlist still has original mosfet transistor "mp" since there is no .param substitution' ); #30
unlike( $out, qr/\s+mp\.\d+ /, '#31: generated netlist does not have any mosfet transistor fingers for "mp" since there is no .param substitution and the fingers were specified with a parameter' ); #31
unlike( $out, qr/\smn /, '#32: generated netlist does not have original mosfet transistor "mn" when its fingers are expanded' ); #32
like( $out, qr/\s+mn\.1 out inp vgnd vgnd nmos l=\{l\} w=\{w\} m=1\s+/, '#33: generated netlist has first mosfet transistor finger for "mn" when its fingers are expanded' ); #33
like( $out, qr/\s+mn\.2 out inp vgnd vgnd nmos l=\{l\} w=\{w\} m=1\s+/, '#34: generated netlist has second mosfet transistor finger for "mn" when its fingers are expanded' ); #34
unlike( $out, qr/\s+mn\.[^12]\S* /, '#35: generated netlist only has two mosfet transistor fingers for "mn" when its fingers are expanded' ); #35

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', "$DECK_NAME.gen-np.3" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$DECK_NAME.sti", "$DECK_NAME.gen-np.3.sti" ), 0, '#36: simulation results of the generated netlist (with mosfet fingers expanded and no .param substitution) should be identical to the original netlist' ); #36


# Run with -i and -o to specify input netlist and location for generated netlist, and
#   run with -x to expand subcircuits in the netlist (note, parameters are substituted)
@cmd = ( $SUT, '-xli', "$DECK_NAME.i", '-o', "$DECK_NAME.gen.4.i" );
run( \@cmd, \$in, \$out, \$err );

unlike( $out, qr/\s+\.SUBCKT /, '#37: generated netlist does not have original subcircuit definition when subcircuits are expanded (no .SUBCKT)' ); #37
unlike( $out, qr/\s+mp /, '#38: generated netlist does not have original mosfet transistor "mp" when subcircuits are expanded' ); #38
unlike( $out, qr/\s+mn /, '#39: generated netlist does not have original mosfet transistor "mn" when subcircuits are expanded' ); #39
unlike( $out, qr/\s+\.ENDS /, '#40: generated netlist does not have original subcircuit definition when subcircuits are expanded (no .ENDS)' ); #40
unlike( $out, qr/\s+xinv /, '#41: generated netlist does not have original subcircuit instantiation when subcircuits are expanded' ); #41
like( $out, qr/\s+m\.xinv\.mp out out vdd vdd pmos l=\{0\.35u\} w=\{3u\} m=\{3\}\s+/, '#42: generated netlist has mosfet transistor "m.xinv.mp" when subcircuits are expanded' ); #42
like( $out, qr/\s+m\.xinv\.mn out in vss vss nmos l=\{0\.35u\} w=\{3u\} m=2\s+/, '#43: generated netlist has mosfet transistor "m.xinv.mn" when subcircuits are expanded' ); #43
like( $out, qr/\s+\.ic v\(in\)=0 v\(out\)=3\.3\s+/, '#44: generated netlist has correct initial conditions (.ic) control line when subcircuits are expanded' ); #44

@run_cmd = ( './lib/run_seus.pl', '-u', './lib', '-lns', "$DECK_NAME.gen.4" );
run( \@run_cmd, \$in, \$out, \$err );

is( compare( "$DECK_NAME.sti", "$DECK_NAME.gen.4.sti" ), 0, '#45: simulation results of the generated netlist (with subcircuits expanded) should be identical to the original netlist' ); #45


# Run with -i to specify input netlist, and -s to not print netlist to STDOUT,
#   and -d to print parameter details to STDOUT
my $expect_param_details = ''.
"l=0.35u\n".
"w=3u\n".
"vddval=3.3\n".
"tempc=27\n".
"pfingers=3\n".
"1000000T=l\n".
"1000001T=w\n".
"1000002T=pfingers\n".
"1000003T=l\n".
"1000004T=w\n".
"1000005T=vddval\n".
"1000006T=vddval\n".
"1000007T=tempc\n";
@cmd = ( $SUT, '-dsi', "$DECK_NAME.i" );
run( \@cmd, \$in, \$out, \$err );
is( $out, $expect_param_details, '#46: the expected parameter details have been printed to STDOUT' ); #46

remove_test_files();

done_testing();

