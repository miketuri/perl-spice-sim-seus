#!/usr/bin/perl -w
use strict;
use warnings;

my $VERSION = '1.1.4'; # Need to consider .ic for subcircuits (name netutils vs. netgen?)

# SEUS Package's netgen_seus.pl:
#   Correctly generates a netlist for an ngspice simulation
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'netgen_seus.pl';

# License
my $License = "\n".
"SEUS $Script_Name Copyright (C) 2009-2020 Michael A. Turi\n".
"This program comes with ABSOLUTELY NO WARRANTY.  This is free software,\n".
"  and you are welcome to redistribute it under certain conditions.\n".
"Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')\n".
"  in the SEUS package for more details.\n\n";

# Constants:
my $Placeholder_Prefix = 'T'; # Must be a Spice Prefix (so the simulator won't error when doing a listing expand)
my $Init_Placeholder = 1000000;

# Usage:
my $Usage = ''.
"Usage: $Script_Name  [OPTIONS]\n".
"Options:\n".
"  -d           Print parameter details to STDOUT and substitute placeholders\n".
"               for parameter expressions\n".
"                 STDOUT format:\n".
"                   param1=value1\n".
"                   param2=value2\n".
"                   ".$Init_Placeholder.$Placeholder_Prefix.'=param_exp1 (using "'.$Placeholder_Prefix."\" prefix for placeholder)\n".
"                   ".($Init_Placeholder+1).$Placeholder_Prefix."=param_exp2\n".
"                   * Comment (beginning of netlist if no \"-s\" option used)\n".
"  -f           Expand mosfet transistor fingers into separate mosfets\n".
"  -i filename  Use \"filename\" for the input netlist instead of STDIN\n".
"  -l           Suppress display of SEUS's AGPL-3.0-or-later license information\n".
"  -o filename  Use \"filename\" for netlist output\n".
"                 (with/without STDOUT output--look at -s)\n".
"  -p           Do not perform parameter substitutions on netlist\n".
"  -s           Do not print netlist output to STDOUT\n".
"                 (use -o to print to file instead)\n".
"  -x           Substitute and expand subcircuits\n".
"  -z           Suppress output to STDERR (only show output for errors)\n";
my $Min_Arg_Num = 0;

# Procedure: read_netlist
#
#   Reads a spice netlist from a file handle, and returns an array with the contents;
#   e.g., code, comments, etc.
#
# Parameter: A file handle of a netlist to read
#
# Output: Returns an array holding the spice netlist
#
# Note: This does not check to see if the file handle is valid for reading
sub read_netlist {
  my $net_fh = shift;
  my @netlist_array = <$net_fh>;
  return @netlist_array;
}

# Procedure: rm_comments
#
#   Removes all commented lines in a spice netlist array.  Blank or whitespace only lines
#   and lines beginning with the comment character "*" or whitespace will be removed.
#   The first line of the netlist (the title) is interpreted as a comment (and is usually
#   a comment), but will be kept.  In addition, one blank line will be present at the end
#   of the netlist (usually after .END).
#
# Parameter: A reference to an array holding a spice netlist
#
# Note: Comments with ";" and inline comments (e.g., vdd 1 0 dc 1V * Supply Voltage)
#       are ignored
sub rm_comments {
  my $net_ref = shift;

  # Remove first line (the netlist title is always treated as a comment)
  my $net_title = shift @{$net_ref}; # We'll put the title back at the end

  @{$net_ref} = grep !/^\*/, @{$net_ref}; # Remove lines starting with a comment
  # Note, leaving comments which are inline with code; it is difficult to discern between
  #   an inline comment and a multiplication used in a parameter expression, e.g., {a * b}

  # Remove blank lines (and lines with only whitespace)
  @{$net_ref} = grep !/^\s*$/, @{$net_ref};

  # Remove lines beginning with a whitespace (ngspice ignores these lines anyway)
  @{$net_ref} = grep !/^\s+/, @{$net_ref};

  unshift @{$net_ref}, $net_title; # Put the title line back in the netlist
  push @{$net_ref}, "\n"; # Add a blank line at the end of the netlist

  return;
}

# Procedure: expand_includes
#
#   Searches through a spice netlist array to find any ".include" statements and replaces
#   these statements with the contents of the .include file
#
# Parameter: A reference to an array holding a spice netlist
#
# Note: This does not check to see if the file handle is valid for reading.
sub expand_includes {
  my $net_ref = shift;

  my $fh;
  for( my $i = 0; $i < scalar @{$net_ref}; $i++ ) {
    if( ${$net_ref}[$i] =~ m/^\.include\s+(\S+)/i ) { # Found an include statement
      # Open the include file and splice it into the netlist
      open $fh, "<$1" or die 'Error: Cannot open ', $1, ' due to: ', $!;
      my @include = <$fh>;
      close $fh;

      splice @{$net_ref}, $i, 1, @include;
      redo; # This line number ($i) is now the first line of the spliced in include file
    }
  }
  return;
}

# Procedure: expand_mosfet_fingers
#
#   Makes copies of a mosfet transistor in the netlist if there are multiple fingers
#   (represented by m != 1).  For example, the following mosfet:
#      m1 d g s b pmos l={l} w={w} m=3
#   is replaced by:
#      m1.1 d g s b pmos l={l} w={w} m=1
#      m1.2 d g s b pmos l={l} w={w} m=1
#      m1.3 d g s b pmos l={l} w={w} m=1
#   Note that parameters must be substituted into the mosfet or mosfets with .param
#   variable values for the number of fingers will not be expanded, for example:
#      m1 d g s b pmos l={l} w={w} m={np}
#
# Parameters: 1st) A reference to an array holding a spice netlist
#             2nd) A scalar index value (line number) of the mosfet to try to expand
#             3rd) A scalar name of the mosfet to try to expand
#
# Note: This does not check to see if mosfet transistors in the netlist already exist
#       with the same name as an expanded finger.  For example, if expanding mosfet
#       transistor "m1", this will not check to see if "m1.1", "m1.2", etc. already exists.
sub expand_mosfet_fingers {
  my $net_ref = shift;
  my $idx = shift;
  my $mosfet_name = shift;
  my $mosfet_line = ${$net_ref}[$idx];
  if( $mosfet_line =~ m/\s+m\s*=\s*\{?(\d+)\}?\s+/i ) { # This mosfet has fingers specified
    my $fingernum = $1;
    if( $fingernum > 1 ) { # This mosfet has multiple fingers
      $mosfet_line =~ s/\s+m\s*=\s*\{?\d+\}?/ m=1/i; # Change the transistor to only one finger
      my @fingers;
      while( $fingernum > 0 ) {
        $mosfet_line =~ s/^m[\w.]*\s+/$mosfet_name.$fingernum /i; # Append the finger number to the transistor (e.g., mp.nfingers, mp.nfingers-1, ... mp.2, mp.1)
        push @fingers, $mosfet_line;
        $fingernum--;
      }
      splice @{$net_ref}, $idx, 1, reverse @fingers;
    }
  }
  return;
}

# Procedure: expand_subcircuits
#
#   Searches through a spice netlist array to find subcircuit definitions and subcircuit
#   instantiations.  The subcircuit definitions are removed from the netlist and the
#   subcircuit instantiations are replaced with code for the subcircuit instance.
#   For example, the following subcircuit definition:
#      .SUBCKT inv in out vdd vss
#      mp out in vdd vdd pmos l={l} w={w} m=1
#      mn out in vss vss nmos l={l} w={w} m=1
#      .ic v(in)=0 v(out)=1
#      .ENDS inv
#   Will be used to replace the subcircuit instantiation:
#      xinv inp out vdd gnd inv
#   With the following code:
#      m.xinv.mp out inp vdd vdd pmos l={l} w={w} m=1
#      m.xinv.mn out inp gnd gnd nmos l={l} w={w} m=1
#      .ic v(inp)=0 v(out)=1
#
# Parameter: A reference to an array holding a spice netlist
#
# Note: This does not check to see if devices in the netlist already exist with the same
#       name as an expanded subcircuit (a name in the format of
#       "device_char.subckt_instance_name.device_name"; e.g., "m.xinv.mp" or "v.x1.vcc");
#       in the example above, this does not check to see if "m.inv.mp" or "m.inv.mn"
#       already exist.
#       This also tries not to substitute a net name for a parameter name; this could
#       happen if a subcircuit port name (listed in the subcircuit definition) is the same
#       as a parameter used in the subcircuit.  However, this function will substitute
#       a net name for a model name (e.g., if the subcircuit example above had a port
#       named "pmos", that port would be replaced by the net name and will replace the
#       model name of transistor mp (a pmos mosfet); to avoid this, do not give a port the
#       same name as a model used in the subcircuit or use the same net name as the port
#       name when you instantiate the subcircuit.
sub expand_subcircuits {
  my $net_ref = shift;

  # Find all defined subcircuits and remove the definitions
  my %subckt_headers;
  my %subckt_code;
  my $i;
  for( $i = 0; $i < scalar @{$net_ref}; $i++ ) {
    if( ${$net_ref}[$i] =~ m/^\.subckt\s+(\S+)([^*]*)/i ) {
      #Verbose: print 'Found subcircuit "', $1, '" with IOs: ', $2, "\n";
      my $subckt_start = $i;
      $subckt_headers{$1} = $2;

      my @sub_code;
      while( ++$i < scalar @{$net_ref} and ${$net_ref}[$i] !~ m/^\.ends\s+$1/i ) {
        # Checking for .ENDS "subckt_name"
        push @sub_code, ${$net_ref}[$i]; # Add this line of code to the subcircuit
      }

      if( $i >= scalar @{$net_ref} ) { # Exit if end of subcircuit not found
        print STDERR 'Error with subcircuit "', $1, '" definition: End of subcircuit (ends) not found', "\n";
        return;
      }

      $subckt_code{$1} = join '', @sub_code;
      #Verbose: print "And code:\n$subckt_code{$1}\n";
      splice @{$net_ref}, $subckt_start, $i-$subckt_start+1; # Remove subcircuit definition
      $i = $subckt_start-1; # Correct the iterator so we look through the entire netlist
    }
  }

  # Find all instantiated subcircuits and expand them
  for( $i = 0; $i < scalar @{$net_ref}; $i++ ) {
    if( ${$net_ref}[$i] =~ m/^(x\S+)\s+([^*]+)/i ) {
      my $instance_name = $1;
      my @subckt_nets = split ' ', $2;
      my $subckt_name = pop @subckt_nets;
      #Verbose: print 'Found instantiated subcircuit "', $instance_name, '" of "', $subckt_name , '" with IOs: ', (join ' ', @subckt_nets), "\n";
      unless( exists $subckt_headers{$subckt_name} ) {
        print STDERR
          "Error: Could not find the header from subcircuit $subckt_name\'s definition\n";
        return;
      }
      unless( exists $subckt_code{$subckt_name} ) {
        print STDERR
          "Error: Could not find the code from subcircuit $subckt_name\'s definition\n";
        return;
      }
      my @subckt_ports = split ' ', $subckt_headers{$subckt_name};
      #Verbose: print scalar @subckt_nets, ' nets will go to ', scalar @subckt_ports, " ports\n";
      #Verbose: for( my $j = 0; $j < scalar @subckt_ports; $j++ ) {
      #  print 'Net ', $subckt_nets[$j], ' will go to port ', $subckt_ports[$j], "\n";
      #}
      my @instance_code = split "\n", $subckt_code{$subckt_name};
      foreach my $instance_line (@instance_code) {
        $instance_line .= "\n"; # Place the newline character back on the line
        if( $instance_line =~ m/^\w/ ) { # Found a circuit component (starts with a letter)
          $instance_line =~ s/^(\w)(\S*)\s+/$1.$instance_name.$1$2 /;
            # Renaming subcircuit component using the subcircuit instance name
            #   (e.g., mosfet transistor "mp" in "xinv" becomes "m.xinv.mp" and
            #          voltage source "v1" in "xamp" becomes "v.xamp.v1")
          for( my $j = 0; $j < scalar @subckt_ports; $j++ ) {
            next if $subckt_ports[$j] eq $subckt_nets[$j];
              # No need to substitute if the net name is the same as the subcircuit port name
              #   (This would cause an infinite loop below anyway)
            while( $instance_line =~ m/([^-+*\/={(])\s+$subckt_ports[$j]\s+([^-+*\/)}])/i ) {
              # Make substitutions as long as this name is not a parameter (not next to a
              #   brace {}, parenthesis (), math operator +-*/, or following an equal sign =
              # Need to do this with a loop rather than just one substitution in case
              #   substitutions need to be made on neighboring nets/ports (checking
              #   neighboring characters to ensure not substituting for parameter name may
              #   not make these substitutions)
              $instance_line =~
                s/([^-+*\/={(])\s+$subckt_ports[$j]\s+([^-+*\/)}])/$1 $subckt_nets[$j] $2/gi;
            }
          }
        } elsif( $instance_line =~ m/^\./ ) { # This is a control line (starts with period)
          # This could be an .ic (initial conditions), .print, .options, etc.
          #   Do not modify name of control line, just make net name substitutions
          for( my $j = 0; $j < scalar @subckt_ports; $j++ ) {
            # Substitute net name if the subcircuit port name is on the control line
            #   e.g.,  .ic v(port_name)=0  ==>  .ic v(net_name)=0
            $instance_line =~ s/v\(\s*$subckt_ports[$j]\s*\)/v($subckt_nets[$j])/gi;
          }
        } # End if; keep the line, but do not substitute net names if the line is not a
          #   device or control line (may be a comment or whitespace)
      }
      splice @{$net_ref}, $i, 1, @instance_code;
        # Replace the subcircuit instantiation, e.g., xinv, with the actual code
      redo; # Restart the loop from the start of this subcircuit code, in case this
            #   subcircuit uses a subcircuit instantiation
    }
  }
  return;
}

# Procedure: write_netlist
#
#   Writes a the contents of a spice netlist array to a file handle
#
# Parameters: 1) A file handle for the file to write
#             2) A reference to an array holding a spice netlist
#
# Output: None
#
# Note: This does not check to see if the file handle is valid for writing
sub write_netlist {
  my $net_fh = shift;
  my $net_ref = shift;
  print $net_fh @{$net_ref};
  return;
}

# netgen_seus.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. and Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is a Perl script used to remove comments, remove whitespace, insert
# .include files, and substitute .param variables into a netlist prior to
# running a simultation.  This script is called by run_seus.pl and
# init_batch_seus.pl.  I have seen an ngspice-based simulator (the University
# of Florida Double-Gate (UFDG) MOSFET model for FinFETs) incorrectly handle
# .param variable substitutions (results would vary depending on whitespace
# and comments).  After this script is run, ngspice will only need to
# substitute for subcircuits and compute math expressions (where .param
# variables had been replaced by this script).
# 
# Usage: By default this script takes an input script through STDIN and
#        outputs the resultant script to STDOUT.  Options allow the input and
#        output to come from or go to files.  Parameter substitutions can be
#        disabled, and details about .param variables can be printed.
#        Parameter details are printed to STDOUT, and are made up of sets of
#        parameter names and values, then are followed by sets of placeholder
#        names and expressions.  If parameter substitution is on, then
#        placeholders are substituted into the netlist, which if output to
#        STDOUT, will appear directly following the sets of placeholder names
#        and expressions.
# 
# Note: Placeholders for parameter expressions use the 'T' (tera-) prefix and
#       values beginning with one million (1000000).  If you use a value in
#       the spice netlist of at least 1000000T, then this may be misinterpreted
#       as a placeholer for a parameter expression and cause an error or
#       incorrect netlist to be generated.
#
#       In order for this script to perform parameter substitution, the variable
#       or expression used for the parameter must be surrounded by curly braces.
#       E.g., w={mywidth} or w={avgwidth+widthvariation}

my $argnum = scalar @ARGV;

my $i = 0;
my $arg = shift;
my $op_param_details; # -d
my $op_expand_fingers; # -f
my $op_in_fname; # -i filename
my $op_skip_lic; # -l
my $op_out_fname; # -o filename
my $op_no_param_sub; # -p
my $op_out_no_stdout; # -s
my $op_expand_subckts; # -x
my $op_out_no_stderr; # -z
while( defined $arg and $arg =~ m/^-/ ) {
  if( $arg =~ m/^-(\S+)/ ) { # Found options
    my $argmatch = $1;

    # Check single letter options first
    if( $argmatch =~ m/d/i ) {
      $op_param_details = 1; # Print details about .param variables (.param name and value plus placeholders)
    }
    if( $argmatch =~ m/f/i ) {
      $op_expand_fingers = 1; # Expand mosfet transistor fingers into separate mosfets
    }
    if( $argmatch =~ m/l/i ) { # Suppress printing license information
      $op_skip_lic = 1;
    }
    if( $argmatch =~ m/p/i ) {
      $op_no_param_sub = 1; # Do not perform parameter substitutions on netlist
    }
    if( $argmatch =~ m/s/i ) {
      $op_out_no_stdout = 1; # Do not print output to STDOUT
    }
    if( $argmatch =~ m/x/i ) {
      $op_expand_subckts = 1; # Substitute and expand subcircuits
    }
    if( $argmatch =~ m/z/i ) {
      $op_out_no_stderr = 1; # Do not print output to STDERR (unless there is actually an error)
    }

    # Check options which require another argument
    if( $argmatch =~ m/i/i ) {
      $op_in_fname = shift; # Use this file instead of STDIN for input
    }
    if( $argmatch =~ m/o/i ) {
      $op_out_fname = shift; # Print output to this file (may be in addition to STDOUT)
    }
  }
  $arg = shift;
}
# Print script name, license, and usage unless output to STDERR is suppressed
print STDERR "** $Script_Name ** SEUS Package Version: $VERSION **\n" unless defined $op_out_no_stderr;
print STDERR $License unless defined $op_skip_lic or defined $op_out_no_stderr;
print STDERR $Usage unless defined $op_out_no_stderr; 

##########
my @spice; # The spice netlist which will be modified
##########

my $fh;
if( defined $op_in_fname ) {
  open $fh, "<$op_in_fname" or die 'Error: Cannot open ', $op_in_fname, ' due to: ', $!;
  @spice = read_netlist $fh;
  die 'Error: ', $op_in_fname, ' is an empty file containing no netlist' unless scalar @spice > 0;
} else {
  open $fh, '<-' or die 'Error: Cannot open STDIN due to ', $!;
  @spice = read_netlist $fh;
  die 'Error: STDIN contains no data to read' unless scalar @spice > 0;
}
close $fh;

expand_includes \@spice; # Insert include files into the netlist
rm_comments \@spice; # Remove comments in the netlist
expand_subcircuits \@spice if defined $op_expand_subckts;
  # Substitute and expand any subcircuits in the netlist

##########
my %params; # A hash of .param variable names (as key) and the respective values
my @params_ordered; # (for op_param_details) An array of the .param variable names (in the order declared in the netlist); use instead of "keys %params" to maintain order
##########

for( $i = 0; $i < scalar @spice; $i++ ) {
  if( $spice[$i] =~ m/^\.param\s+(\w+)\s*=\s*\{(.+)\}/i ) { # Found a param definition which required other parameter(s)
    my $name = lc $1;
    my $expression = $2;
    if( defined $op_param_details ) { # Don't substitute for the expression, just getting details now
      $params{$name} = '{'.$expression.'}'; # Add this .param name and value (an expression) to the hash
      push @params_ordered, $name; # Add param name in ordered array
    } elsif( !defined $op_no_param_sub ) { # Now we'll substitute for the expression; No reason to do this if $op_no_param_sub is chosen
      # Split the spice expression by words (.param variable names), and keep the non-words (i.e. math operators)
      my @exp_parts = split /(\W)/, $expression;
      foreach (@exp_parts) {
        while((my $param_name, my $val) = each %params) {
          if( m/\w+/ ) { s/^$param_name$/$val/i; } # Substitute the value if a .param variable is found
        }
      }
      $expression = join '', @exp_parts; # Combine the parts to form the expression again
      $params{$name} = '('.$expression.')'; # Add parentheses to maintain order of operations, and place in the hash
        # Note: Let .param X=5 and .param Y={1+1}; If another expression .param PROD={X*Y}, then {5*1+1} is different than {5*(1+1)}
    }
    if( !defined $op_no_param_sub ) {
      splice @spice, $i, 1; # Remove the definition if we are substituting for .param variables
      redo; # This line number ($i) is now the line directly after the .param definition
    }
  } elsif( $spice[$i] =~ m/^\.param\s+(\w+)\s*=\s*(\S+)/i ) { # Found a simple param definition (.param "name" = "value")
    $params{lc $1} = $2; # Add this .param name and value to the hash
    push @params_ordered, $1; # Add param name in ordered array
    if( !defined $op_no_param_sub ) {
      splice @spice, $i, 1; # Remove the definition if we are substituting for .param variables
      redo; # This line number ($i) is now the line directly after the .param definition
    }
  }
}

if( defined $op_param_details ) { # If printing .param details, first print the .param names and the respective values
  foreach my $param_name (@params_ordered) {
    print $param_name, '=', $params{$param_name}, "\n";
  }
}

##########
my $spice_line; # A line from the spice netlist
my @spice_line_parts; # The $spice_line split up around the curly braces {} so parameter substitution or details can be done
my $spice_exp_ref; # A reference to an expression in curly braces {} involving possible .param variables
my @spice_exp_parts; # The $spice_exp_ref split in order to do substitution of .param variables in the expression
my $placeholder = $Init_Placeholder; # The placeholder value, beginning at the initial value
##########

foreach $spice_line (@spice) {
  if( $spice_line =~ m/\{.+\}/ ) {
    # Split the spice line by { and } chars. (char. class).  Keep { and } if not substituting
    #   with placeholders (op_param_details) so Spice3 can calculate the math expression
    #   (placeholders substitute for the entire expression).
    @spice_line_parts = defined $op_param_details ? split /[{}]/, $spice_line : split /([{}])/, $spice_line;

    # Value of $i, given line "m1 d fg s bg psg l={l} w={h} m={np}"
    #  op_param_details (placeholders):  #0:"m1 d fg s bg psg l="  #1:"l"  #2:" w="  #3:"h"  #4:" m="  #5:"np"  #6:"\n"
    #  Normal (make substitution):       #0:"m1 d fg s bg psg l="  #1:"{"  #2:"l"    #3:"}"  #4:" w="  #5:"{"   #6:"h" ...
    $i = defined $op_param_details ? 1 : 2; # Location of first expression (see above)

    while( $i < scalar @spice_line_parts ) { # This is a little ugly
      $spice_exp_ref = \$spice_line_parts[$i]; # Reference the spice expression so $i can be incremented
      $i += defined $op_param_details ? 2 : 4; # Location of next expression, if it exists (see above)
      if( defined $op_param_details ) {
        print $placeholder, $Placeholder_Prefix, '=', ${$spice_exp_ref}, "\n"; # Print the placeholder and the expression it represents
        ${$spice_exp_ref} = $placeholder.$Placeholder_Prefix unless defined $op_no_param_sub; # If .param substitutions ok, then substitute in the placeholder
        $placeholder++;
        next; # If getting details, don't continue farther in the loop--no need to substitute for .param variables in the expression
      }
      next if defined $op_no_param_sub; # If not substituting for .param variables, don't continue farther in the loop

      # Split the spice expression by words (.param variable names), and keep the non-words (i.e. math operators)
      @spice_exp_parts = split /(\W)/, ${$spice_exp_ref};
      foreach (@spice_exp_parts) {
        while((my $param_name, my $val) = each %params) {
          if( m/\w+/ ) { s/^$param_name$/$val/i; } # Substitute the value if a .param variable is found
        }
      }
      ${$spice_exp_ref} = join '', @spice_exp_parts; # Replace the original expression with numerals and math operators, ngspice/Spice3 can compute this at runtime
    }
    $spice_line = join '', @spice_line_parts; # Replace the original spice line of the netlist with this line with substituted expressions
  }
}

# One more substitution to do, ngspice doesn't like expressions on the .OPTION line for some reason.
# Have Perl try to compute the expression if it hasn't been replaced by a placeholder (getting details) and if .param variables are being substituted
if( !defined $op_param_details and !defined $op_no_param_sub ) {
  foreach (@spice) {
    if( m/^\.OPTION/i ) { # Found the option line
      my @option_parts = split /[{}]/; # Split the line by the curly braces {}, but don't keep them.
      my $answer;
      for( $i=1; $i < scalar @option_parts; $i+=2 ) { # Every other part will be an expression (e.g., .OPTION ABSTOL=1E-6 TEMP={tempc} FOO={b*b-4*a*c}\n)
        # Not sure if eval will calculate the answer, best to avoid expressions in the .option line
        eval { $answer = $option_parts[$i]; }; # Try to evaluate the expression (hopefully ngspice/Spice3 suffixes are not used (i.e. n=1e-9, p=1e-12, etc.))
        die 'Error: Cannot compute expression {', $option_parts[$i], '} on .option line due to: ', $@ if $@; # Not sure if eval will actually generate an error though
        $option_parts[$i] = $answer; # Replace the expression with the answer
      }
      $_ = join '', @option_parts; # Replace the .OPTION line with the updated version
    }
  }
}

if( defined $op_expand_fingers ) { # Last step, expand mosfet fingers if requested
  for( $i = 0; $i < scalar @spice; $i++ ) {
    if( $spice[$i] =~ m/^(m[\w.]*)\s+/i ) {
      expand_mosfet_fingers \@spice, $i, $1; # Found a mosfet, try to expand the fingers if possible
    }
  }
}

if( defined $op_out_fname ) { # If the user requested, print the netlist to the output file

  open $fh, ">$op_out_fname" or die 'Error: Cannot open ', $op_out_fname, ' due to: ', $!;
  write_netlist $fh, \@spice;
  close $fh;
}

unless( defined $op_out_no_stdout ) { # Unless the user wanted no STDOUT, print the netlist to STDOUT
  open $fh, '>-' or die 'Error: Cannot open STDOUT due to ', $!;
  write_netlist $fh, \@spice;
  close $fh;
}


=head1 NAME

netgen_seus.pl - A Perl script from the SEUS (Scripts for Easier Use of Spice) Package used to correctly generate a netlist for an ngspice simulation by removing comments, inserting include files, and making parameter substitutions

=head1 VERSION

Version 1.1.4

=head1 SYNOPSYS

    netgen_seus.pl  [OPTIONS]

Run netgen_seus.pl with options (as needed) to correctly generate a netlist for an ngspice simulation by removing all comments, inserting include files, and making parameter substitutions.  By default this script expects the input netlist to come from STDIN and the generated, output netlist to go to STDOUT.  Options allow the input and output to come from or go to files.

The reason for netgen_seus.pl is that an ngspice-based simulator (University of Florida's Spice3-UFDG (Linux version 3.71)) incorrectly handles parameter substitutions; the results vary depending on whitespace and comments.

In order for netgen_seus.pl to make parameter substitutions, then the parameter, or expression which uses the parameter, B<must> be surrounded by curly braces, for example:

    w={mywidth}

    w={avgwidth+widthvariation}

This made it much easier for this script to make parameter substitutions, but this also protects the netlist in the event that a net or device shares the same name as a parameter (a Spice error would occur if a parameter value was accidentally substituted for a net name or device name).

netgen_seus.pl will print its usage to STDERR, but wait for input netlist data from STDIN if it is run without any options.

=head1 OPTIONS

To see all available options, run netgen_seus.pl without any options to print its usage, including all options, to STDERR; however, the script will also be waiting for input netlist data from STDIN.

By default, if no options are provided, this script expects the input netlist to be available via STDIN and the generated output netlist will be printed to STDOUT.  The script will remove comments, insert include files, and make parameter substitutions.

=head2 -d  Print parameter details to STDOUT and substitute placeholders for parameter expressions

This option is not meant to be used publicly.  init_batch_seus.pl uses this option to insert placeholders in parameter expressions (any use of curly braces {}).  This happens before init_batch_seus.pl runs ngspice's "listing expand" operation to expand the subcircuits in a netlist because ngspice will also evaluate parameter expressions during "listing expand".

This option outputs the details of the parameter substitutions to STDOUT followed by the netlist with placeholders unless the -s option is used.  The format of the parameter details is:

=over

=item 1.

First, the parameters and values are output, line by line, to STDOUT.  For example:

=over

=item C<l=0.35u>

=item C<w=3u>

=back

=item 2.

Second, the placeholders and parameter expressions are output, line by line, to STDOUT.  For example:

=over

=item C<1000000T=l>

=item C<1000001T=w>

=item C<1000002T=l>

=item C<1000003T=2*w>

=back

=back

=head3 Limitation of -d option

Placeholders for parameter expressions use the "T" (tera-) prefix and values beginning with one million (1000000).  While unlikely, if the spice netlist contains a value of at least 1000000T, then this may be misinterpreted as a placeholder for a parameter expression and cause an error or incorrect netlist to be generated.

=head2 -f  Expand mosfet transistor fingers into separate mosfets

If the -f option is passed, then netgen_seus.pl will expand mosfet transistor fingers into separate mosfets.  For example, a mosfet transistor named "m1" with three fingers (m=3) will be expanded into three separate mosfet transistors named "m1.1", "m1.2", and "m1.3" and the original transistor "m1" is deleted; i.e., the original mosfet transistor with fingers is replaced by the separate mosfet transistors representing the fingers.  Leading zeros are not added if working with large numbers of fingers; e.g., m1.1, m1.2, ..., m1.9, m1.10, ... m1.99, m1.100, ...

Note, mosfet transistor finger expansion will only occur if the fingers, m, are defined by a constant number or by a parameter that will be substituted a value by netgen_seus.pl.  If not using netgen_seus.pl to substitute parameters, see the -p option, or if substituting placeholders for parameters, see the -d option, then transistors with parameters for the number of fingers, m, which have not be substituted a value will not be expanded.

Also note, netgen_seus.pl will not check the other transistor names in the netlist when expanding mosfet transistors; this can cause Spice syntax errors to occur.  Avoid naming transistors similar to the numbering scheme used by netgen_seus.pl; for example, if you have a mosfet transisor named "m1" with fingers that you wish to expand, do not name another transistor "m1.#" in your circuit (where # is a number which is greater than or equal to 1).

=head2 -i filename  Use "filename" for the input netlist instead of STDIN

By default, netgen_seus.pl expects the input netlist to come from STDIN.  Use this option for the input netlist to come from a file.

=head2 -l  Suppress display of SEUS's AGPL-3.0-or-later license information

If the -l option is passed, then the short description of the license is not printed. The seus.pl script, the main entry point for running SEUS scripts, passes the -l option since it prints the license information itself.

=head2 -o filename  Use "filename" for netlist output (with or without STDOUT output)

By default, netgen_seus.pl will print the netlist output to STDOUT.  Use this option for the netlist output to also go to a file.  To not print the netlist output to STDOUT, then also use the -s option.

=head2 -p  Do not perform parameter substitutions on netlist

By default, netgen_seus.pl will remove all comments, insert include files, and make parameter substitutions.  Use this option to instead have ngspice make the parameter substitutions.

Note: The reason for netgen_seus.pl is that an ngspice-based simulator (University of Florida's Spice3-UFDG (Linux version 3.71)) incorrectly handles parameter substitutions; the results vary depending on whitespace and comments.

=head2 -s  Do not print netlist output to STDOUT

By default, netgen_seus.pl will print the netlist output to STDOUT.  Use this option to not print the netlist output to STDOUT.  However, in order to obtain the netlist output, use the -o option to specify a file to write the netlist output.

=head2 -x  Substitute and expand subcircuits

If the -x option is passed, then netgen_seus.pl will substitute and expand subcircuits within the netlist.  Subcircuit definitions are removed from the netlist and the subcircuit instantiations are replaced with code for the subcircuit instance.  For example, the following subcircuit definition:

C<.SUBCKT inv in out vdd vss>

C<mp out in vdd vdd pmos l={l} w={w} m=1>

C<mn out in vss vss nmos l={l} w={w} m=1>

C<.ic v(in)=0 v(out)=1>

C<.ENDS inv>

Will be used to replace the subcircuit instantiation:

C<xinv inp out vdd gnd inv>

With the following code:

C<m.xinv.mp out inp vdd vdd pmos l={l} w={w} m=1>

C<m.xinv.mn out inp gnd gnd nmos l={l} w={w} m=1>

C<.ic v(inp)=0 v(out)=1>

Note, netgen_seus.pl will not check the other transistor names in the netlist when expanding subcircuits; this can cause Spice syntax errors to occur.  Avoid naming transistors similar to the naming scheme used by ngspice and netgen_seus.pl; for example, if you have a mosfet transisor named "mp" in a subcircuit "xinv" that you are expanding, do not name another transistor "m.xinv.mp" in your circuit.

Also note, netgen_seus.pl also tries not to substitute a net name for a parameter name; this could happen if a subcircuit port name (listed in the subcircuit definition) is the same as a parameter used in the subcircuit.  However, this netgen_seus.pl option will substitute a net name for a model name (e.g., if the subcircuit example above had a port named "pmos", that port would be replaced by the net name and will replace the model name of transistor mp (a pmos mosfet); to avoid this, do not give a port the same name as a model used in the subcircuit or use the same net name as the port name when you instantiate the subcircuit.

=head2 -z  Suppress output to STDERR (only show output for errors)

By default, netgen_seus.pl will print the script name, version number, license information (unless the -l option is specified), and script usage information to STDERR; printing this information to STDERR makes it easier to parse STDOUT for the netlist or parameter details (because these may be printed to STDOUT).  Use this option to not print any of this information to STDERR; note, STDERR will still be used to output any errors that occur.

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

