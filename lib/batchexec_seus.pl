#!/usr/bin/perl -w
use strict;
use warnings;

use Path::Iterator::Rule;

my $VERSION = '1.1.4';

# SEUS Package's batchexec_seus.pl:
#   Batch executes spice simulations.  Typically swept parameters or varied
#   parameters (Monte Carlo variation).
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'batchexec_seus.pl';

# License
my $License = "\n".
"SEUS $Script_Name Copyright (C) 2009-2020 Michael A. Turi\n".
"This program comes with ABSOLUTELY NO WARRANTY.  This is free software,\n".
"  and you are welcome to redistribute it under certain conditions.\n".
"Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')\n".
"  in the SEUS package for more details.\n\n";

# Requires script:
my $Run_Seus = 'run_seus.pl';

# Constants:
my $Default_Run_Opts = 'lt'; # Suppress printing license and do not use a timer
my $Failures_Output_Filename = 'batchexec_failures.txt'; # Filename for any simulation fails
my $Netlist_Filetype = 'i';

# Signal (interrupt) handler for SIGUSR1 (#10):
my $num_complete = 0;
my $num_total = 0;
my $last_finishtime = "None";
# Procedure: update_handler
# Summary:
#   Prints the current progress of the script
sub update_handler {
  print 'Update from ', $Script_Name, ': ', $num_complete, ' of ', $num_total,
        " simulations are complete\n",
        'Last sim finished at ', $last_finishtime, "\n\n";
  $SIG{'USR1'} = \&update_handler;
}
$SIG{'USR1'} = \&update_handler;

# Signal (interrupt) handler for SIGTERM (#15):
my %children;
# Procedure: clean_term_handler
# Summary:
#   If the terminate signal is given, also kills each simulation child process
sub clean_term_handler {
  kill 'TERM', keys %children;
  exit 128+15;
}
$SIG{'TERM'} = \&clean_term_handler;

# Usage:
my $Usage = ''.
'Usage: '.$Script_Name."  [OPTIONS]  directory_name\n".
"Options:\n".
"  -0            Do not simulate the nominal netlist in any directory (0.i)\n".
"  -b N          Begin batch simulations with the N'th netlist (ordered by name)\n".
"                  in the directory\n".
"  -e N          End batch simulations with the N'th netlist (ordered by name)\n".
"                  in the directory\n".
"  -l            Suppress display of SEUS's AGPL-3.0-or-later license information\n".
"  -n N          Limit the number of simulations to N; once N sims finish\n".
"                  without failures, terminate all other busy sims\n".
"  -p N          At a maximum, only run N simulations in parallel\n".
"                  (for process friendliness on a shared server)\n".
"  -r run_opts   Use \"run_opts\" as the options for \"".$Run_Seus."\"\n".
"                  [cmd will be: ".$Run_Seus." -run_opts]\n".
"                  Note: By default, the -t option (no timer) is used.\n".
"                  The \"run_opts\" will be used instead\n".
"  -s            Execute all netlists in subdirectories of \"directory_name\"\n".
"                  Note: -0 will not simulate 0.i in any subdirectory and\n".
"                  -b and -e will simulate a number of netlists (ordered by name)\n".
"  -u path  Use \"path\" for location of the SEUS Package (if SEUS not installed)\n".
"  -x exec_name  Batch execute \"exec_name\" instead of \"".$Run_Seus."\"\n";
my $Min_Arg_Num = 1;

# batchexec_seus.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. and Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is a Perl script used to run a set of simulations from a
# directory of netlists.  Often used in tandem with init_batch_seus.pl.
# 
# Usage: Provide the directory name, and the netlists in the directory will be
#        simulated using Spice.  The run_seus.pl script will be used unless a
#        different executable name is specified with the -x option.

print "** $Script_Name ** SEUS Package Version: $VERSION **\n";
die $Usage if scalar @ARGV < 1;
my $argnum = scalar @ARGV;

my $start_time = localtime;
my $calling_cmd_line = join ' ', $Script_Name, @ARGV;
my $i = 0;
my $arg = shift;
my $op_rm0; # -0
my $op_begfile; # -b N
my $op_endfile; # -e N
my $op_skip_lic; # -l
my $op_sim_limit; # -n N
my $op_max_parallel; # -p N
my $run_opts = $Default_Run_Opts; # -r run_opts
my $op_subdirs; # -s
my $seus_path; # -u path
my $exec_name; # -x exec_name
while( $arg =~ m/^-/ ) {
  if( $arg =~ m/^-(\S+)/ ) { # Found options
    my $argmatch = $1;

    # Check single letter options first
    if( $argmatch =~ m/0/i ) {
      $op_rm0 = 1; # Do not simulate 0.i (if it exists)
    }
    if( $argmatch =~ m/l/i ) { # Suppress printing license information
      $op_skip_lic = 1;
    }
    if( $argmatch =~ m/s/i ) { # Execute netlists in subdirectories
      $op_subdirs = 1;
    }

    # Check options which require another argument
    if( $argmatch =~ m/b/i ) { # Begin batch simulations with netlist #N
      $op_begfile = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
      $op_begfile--; # Change notation from 1..#files to 0..#files-1
    }
    if( $argmatch =~ m/e/i ) { # End batch simulations with netlist #N
      $op_endfile = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
      $op_endfile--; # Change notation from 1..#files to 0..#files-1
    }
    if( $argmatch =~ m/n/i ) { # Use a limit of N sims
      $op_sim_limit = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/p/i ) { # Only run N sims in parallel
      $op_max_parallel = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/r/i ) { # Use the run_opts as the options for run_seus.pl
      $run_opts = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/u/i ) { # Use a path to a local location of the SEUS Package
      $seus_path = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/x/i ) { # Batch execute "exec_name"
      $exec_name = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
  }
  die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
  $arg = shift;
}
print $License unless defined $op_skip_lic;
die "Error: Not enough parameters\n\n$Usage" if $argnum < $Min_Arg_Num;

my $deck_dir = $arg;
if( $deck_dir !~ m/\/$/ ) { $deck_dir .= '/'; } # Add the trailing slash if it doesn't exist

if( defined $seus_path ) { # Use SEUS path if provided
  if( $seus_path !~ m/\/$/ ) { $seus_path .= '/'; } # Add trailing slash if it doesn't exist
  $exec_name = $seus_path.$Run_Seus.' -u '.$seus_path.' -'.$run_opts unless defined $exec_name;
} else { # else just call script names and hope the SEUS package is installed
  $exec_name = $Run_Seus.' -'.$run_opts unless defined $exec_name;
}

die 'Error: Cannot find directory "', $deck_dir, '"' unless -d $deck_dir;
opendir D, $deck_dir;
# Get a list of all spice input files (.i files) from the deck directory
#   Note: If traversing subdirectories, then @files will have entire paths, else, @files only
#         contain the basenames.  This is done so numerical sorting is possible if simulating
#         within the top directory.
my $rule = Path::Iterator::Rule->new->name("*.$Netlist_Filetype"); # Use rule if subdirs
my @files_with_suffix = defined $op_subdirs ? $rule->all( substr $deck_dir, 0, -1 ) 
  : grep /\.$Netlist_Filetype$/i, readdir D;
  # Removing trailing slash if using rule to traverse subdirs (it adds a duplicate /)
closedir D;
# Remove the ".i" suffix from the end of each file (with map and substr)
my @files = map { substr $_, 0, (rindex $_, '.') } @files_with_suffix;
# If files have character names, then sort alphabetically, else, sort numerical names numerically
@files = (join '', @files) =~ m/[a-z]/i ? sort @files : sort { $a <=> $b} @files;

if( defined $op_rm0 ) { # Ignore the nominal netlist (0.i) when simulating a directory

  if( defined $op_subdirs ) { # Subdirs are traversed
    my $deckname0 = $deck_dir.'0';
    @files = grep !/^$deckname0$/, @files; # Remove main directory's 0.i from list of files
    @files = grep !/\/0$/, @files; # Also remove 0.i in subdirs from list of files
    
  } else {
    @files = grep !/^0$/, @files; # No subdirs traversed, so can just remove "0" from list of files
  }
}

my $op_begfile_print;
my $op_endfile_print;
if( defined $op_begfile and defined $op_endfile ) { # Use defined beginning/ending boundaries from user if defined
  $op_begfile = 0 unless $op_begfile >= 0 and $op_begfile < scalar @files;
  $op_endfile = (scalar @files)-1 unless $op_endfile >= 0 and $op_endfile < scalar @files and $op_endfile >= $op_begfile;
  $op_begfile_print = ($op_begfile+1).' ('.$files[$op_begfile].'.'.$Netlist_Filetype.')';
  $op_endfile_print = ($op_endfile+1).' ('.$files[$op_endfile].'.'.$Netlist_Filetype.')';
  @files = @files[$op_begfile..$op_endfile];
} elsif( defined $op_begfile ) {
  $op_begfile = 0 unless $op_begfile >= 0 and $op_begfile < scalar @files;
  $op_begfile_print = ($op_begfile+1).' ('.$files[$op_begfile].'.'.$Netlist_Filetype.')';
  $op_endfile_print = 'END ('.$files[-1].'.'.$Netlist_Filetype.')';
  @files = @files[$op_begfile..(scalar @files)-1];
} elsif( defined $op_endfile ) {
  $op_endfile = (scalar @files)-1 unless $op_endfile >= 0 and $op_endfile < scalar @files;
  $op_begfile_print = 'BEGIN ('.$files[0].'.'.$Netlist_Filetype.')';
  $op_endfile_print = ($op_endfile+1).' ('.$files[$op_endfile].'.'.$Netlist_Filetype.')';
  @files = @files[0..$op_endfile];
}

$num_total = scalar @files;
die 'Error: No spice netlists found in directory "', $deck_dir, '"' unless $num_total > 0;

if( defined $op_begfile or defined $op_endfile ) {
  print 'Files ', $op_begfile_print, ' through ', $op_endfile_print, " will be simulated\n";
  sleep 2;
}

my $child_pid;
my $pid;
my $cur_parallel = 0;
my $retval;
my @failures;
foreach (@files) {
  if( !(defined $op_max_parallel) or $cur_parallel < $op_max_parallel ) { # Spawn more child simulations
    $child_pid = fork();
    die "Error: Fork in $Script_Name failed" unless defined $child_pid;
    # Keep track of the children pid numbers (hash value is filename, which is reported if simulation failure occurs)
    $children{$child_pid} = $_;
    $cur_parallel++;
    if( !$child_pid ) { # The child executes the executable (by default: run_seus.pl)
      my $full_deck_path = defined $op_subdirs ? $_ : $deck_dir.$_;
      exec "$exec_name $full_deck_path" or die "Error: Couldn't exec \"$exec_name $full_deck_path\"";
    }
  } else { # Must wait for a child simulation to finish before starting any more
    do {
      $pid = wait();
    } while( !exists $children{$pid} );
    $retval = $? >> 8;
    if( $retval != 0 ) { # This child simulation died due to an error
      push @failures, $children{$pid};
    }
    delete $children{$pid};
    $cur_parallel--;
    $num_complete++;
    $last_finishtime = localtime;
    if( defined $op_sim_limit and $num_complete >= ($op_sim_limit+(scalar @failures)) ) { # Limit has been reached, end all other simulations
      kill 'TERM', keys %children;
      $cur_parallel = 0; # All children should be dead (do not want to enter the while loop below)
      last;
    }
    redo;
  }
}

while( $cur_parallel > 0 ) { # Wait if child simulations are still running
  do {
    $pid = wait();
  } while( !exists $children{$pid} );
  $retval = $? >> 8;
  if( $retval != 0 ) { # This child simulation died due to an error
    push @failures, $children{$pid};
  }
  delete $children{$pid};
  $cur_parallel--;
  $num_complete++;
  $last_finishtime = localtime;
  if( defined $op_sim_limit and $num_complete >= ($op_sim_limit+(scalar @failures)) ) { # Limit has been reached, end all other simulations
    kill 'TERM', keys %children;
    last;
  }
}

if( scalar @failures > 0 ) { # Some simulation failures occurred, record this
  open OUT, ">$deck_dir$Failures_Output_Filename" or die "Error: Cannot open $deck_dir$Failures_Output_Filename due to: ", $!;
  my $failure_list = join "\n", @failures;
  print OUT "$failure_list\n";
  close OUT;
}


=head1 NAME

batchexec_seus.pl - A Perl script from the SEUS (Scripts for Easier Use of Spice) Package used to batch execute spice simulations.  This is typically used to simulate netlists of swept or varied parameters (Monte Carlo simulations).

=head1 VERSION

Version 1.1.4

=head1 SYNOPSYS

    batchexec_seus.pl  [OPTIONS]  directory_name

Run batchexec_seus.pl on a command line to execute a set of simulations from netlists in the specified directory_name.  This is often used in tandem with init_batch_seus.pl.  By default, the run_seus.pl is called to run each simulation (unless a different executable name is specified with the -x option) in parallel using fork and exec.  If there are any simulation failures, then a "batchexec_failures.txt" file will be created in directory_name; this file contains a list of all netlists (one per line) with a simulation that failed.

batchexec_seus.pl will print its usage to STDERR and exit if it is run without any command-line arguments.

=head1 OPTIONS

To see all available options, run batchexec_seus.pl without any command-line arguments to print its usage, including all options, to STDERR.

By default, if no options are provided, then all netlists in the specified directory (but not its subdirectories) will be simulated all at once in parallel.

=head2 -0  Do not simulate the nominal netlist (0.i) in any directory

A 0.i netlist is created by init_batch_seus.pl when initializing a directory with a batch of swept or varied netlists; this netlist is functionally identical to the original netlist (it does not have any swept or varied parameters).  This option will skip simulating 0.i.  This can be useful if calculating the performance statistics of the batch of netlists; the 0.i simulation should not be measured, and thus does not need to be simulated in the first place.

=head2 -b N  Begin batch simulations with the N'th netlist (ordered by name) in the directory

Only netlists in the specified directory from netlist I<N> to the final netlist will be simulated.  For example:

If a directory contains 0.i, 1.i, 2.i, 3.i, 4.i, and 5.i, then

=over

=item *

-b 3 will only simulate netlists 2.i, 3.i, 4.i, and 5.i (netlist 0.i is the first netlist, 1.i is the second netlist, ...)

=item *

-0b 3 will only simulate netlists 3.i, 4.i, and 5.i (now netlist 1.i is the first netlist, 2.i is the second netlist, ...)

=back

If a directory contains 0.i, a.i, b.i, c.i, d.i, and e.i, then, similarly,

=over

=item *

-b 3 will only simulate netlists b.i, c.i, d.i, and e.i (netlist 0.i is the first netlist, a.i is the second netlist, ...)

=item *

-0b 3 will only simulate netlists c.i, d.i, and e.i (now netlist a.i is the first netlist, b.i is the second netlist, ...)

=back

If the -s option is also used to simulate netlists in subdirectories, and if the batch directory contains 0.i, bar/0.i, bar/a.i, bar/b.i, bar/c.i, foo/0.i, foo/1.i, foo/2.i, and foo/3.i, then

=over

=item *

-sb 5 will only simulate netlists bar/c.i, foo/0.i, foo/1.i, foo/2.i, and foo/3.i (netlist 0.i is the first netlist, bar/0.i is the second netlist, ...)

=item *

-0sb 5 will only simulate netlists foo/2.i and foo/3.i (now netlist bar/a.i is the first netlist, bar/b.i is the second netlist, ...)

=back

If the -e option is also used to specify where to end batch simulations, then a range of netlists can be specified.  If a directory contains 0.i, 1.i, 2.i, 3.i, 4.i, and 5.i, then

=over

=item *

-b 2 -e 4 will only simulate netlists 1.i, 2.i, and 3.i (netlist 0.i is the first netlist, 1.i is the second netlist, ...)

=item *

-0b 2 -e 4 will only simulate netlists 2.i, 3.i, and 4.i (now netlist 1.i is the first netlist, 2.i is the second netlist, ...)

=back

=head2 -e N  End batch simulations with the N'th netlist (ordered by name) in the directory

Only netlists in the specified directory from the first netlist to netlist I<N> will be simulated.  For example:

If a directory contains 0.i, 1.i, 2.i, 3.i, 4.i, and 5.i, then

=over

=item *

-e 3 will only simulate netlists 0.i, 1.i, and 2.i (netlist 0.i is the first netlist, 1.i is the second netlist, ...)

=item *

-0e 3 will only simulate netlists 1.i, 2.i, and 3.i (now netlist 1.i is the first netlist, 2.i is the second netlist, ...)

=back

If a directory contains 0.i, a.i, b.i, c.i, d.i, and e.i, then, similarly,

=over

=item *

-e 3 will only simulate netlists 0.i, a.i, and b.i (netlist 0.i is the first netlist, a.i is the second netlist, ...)

=item *

-0e 3 will only simulate netlists a.i, b.i, and c.i (now netlist a.i is the first netlist, b.i is the second netlist, ...)

=back

If the -s option is also used to simulate netlists in subdirectories, and if the batch directory contains 0.i, bar/0.i, bar/a.i, bar/b.i, bar/c.i, foo/0.i, foo/1.i, foo/2.i, and foo/3.i, then

=over

=item *

-se 5 will only simulate netlists 0.i, bar/0.i, bar/a.i, bar/b.i, and bar/c.i (netlist 0.i is the first netlist, bar/0.i is the second netlist, ...)

=item *

-0se 5 will only simulate netlists bar/a.i, bar/b.i, bar/c.i, foo/1.i and foo/2.i (now netlist bar/a.i is the first netlist, bar/b.i is the second netlist, ...)

=back

If the -b option is also used to specify where to begin batch simulations, then a range of netlists can be specified.  If a directory contains 0.i, 1.i, 2.i, 3.i, 4.i, and 5.i, then

=over

=item *

-b 2 -e 4 will only simulate netlists 1.i, 2.i, and 3.i (netlist 0.i is the first netlist, 1.i is the second netlist, ...)

=item *

-0b 2 -e 4 will only simulate netlists 2.i, 3.i, and 4.i (now netlist 1.i is the first netlist, 2.i is the second netlist, ...)

=back

=head2 -l  Suppress display of SEUS's AGPL-3.0-or-later license information

If the -l option is passed, then the short description of the license is not printed.  The seus.pl script, the main entry point for running SEUS scripts, passes the -l option since it prints the license information itself.

=head2 -n N  Limit the number of simulations to N; once N sims finish without failures, terminate all other busy sims

This option will stop batchexec_seus.pl and all other currently running simulations once N simulations finish without failure.  This is helpful when simulations may have convergence issues and run indefinitely without finishing.  More than N simulations may finish if the simulator/computer is very fast; this is because once the script realizes that N simulations have finished, more simulations may finish while the script is terminating all other simulations and exiting.

Note: The run_seus.pl script has a timer, which can be configured to terminate a simulation after a specified number of seconds.  While the timer can be configured by passing options to run_seus.pl (by using batchexec_seus.pl's -r option), this is not reliable since if many simulations are running in parallel, then each simulation will take significantly more time.  run_seus.pl's timer can work if just running one simulation at a time (by using batchexec_seus.pl's -p option), but using batchexec_seus.pl's -n option is recommended when running multiple simulations in parallel.

=head2 -p N  At a maximum, only run N simulations in parallel (for process friendliness on a shared server)

This option will limit batchexec_seus.pl to only running N simulations in parallel at a time.  This is helpful when running this script on a shared server and resources need to be shared with other users/programs.  As one simulation finishes, another simulation will be started to ensure N simulations are always running (until fewer than N simulations remain to be completed).

=head2 -r run_opts  Use "run_opts" as the options for run_seus.pl

By default, batchexec_seus.pl uses run_seus.pl to run simulations for each netlist within the specified directory.  By default, run_seus.pl's -lt options are used to run the simulation without a timer and without displaying run_seus.pl's license information (since this information is already output for batchexec_seus.pl).  To use run_seus.pl with other options (e.g., configure the run_seus.pl timer, generate a specific output format, etc.), then instead use specified "run_opts" with batchexec_seus.pl's -r option.

=head2 -s  Execute all netlists in subdirectories of "directory_name"

By default, batchexec_seus.pl will simulate netlists that are only found at the first level of the specified directory_name.  This option can be used to simulate netlists that are found within subdirectories of directory_name.

Note 1: If also using the -0 option, then this script will not simulate 0.i in directory_name or any subdirectory within

Note 2: If also using the -b and/or -e options, then this script will simulate a number of netlists ordered by name

=head2 -u path  Use "path" for the location of the SEUS Package (if SEUS Package not installed)

If the SEUS Package is successfully installed, then just providing the script name (e.g., batchexec_seus.pl) is sufficient to run the script.  If the SEUS Package is not installed, then the path to the SEUS Package must be provided since, by default, this script will call run_seus.pl.

=head2 -x exec_name  Batch execute "exec_name" instead of run_seus.pl

By default, batchexec_seus.pl will simulate netlists using run_seus.pl.  This option can be used to run a different script or executable.  For this to work properly, "exec_name" should run at the directory location where batchexec_seus.pl is run, and "exec_name" must be a file that can be executed using exec.

=head1 SIGNAL/INTERRUPT SUPPORT

This script supports and responds to the following signals/interrupts:

=head2 SIGUSR1 (Signal/Interrupt #10)

If a SIGUSR1 is issued to this script (e.g., with C<kill -10>), then this script will print its current progress:

=over

C<Update from batchexec_seus.pl: X of N simulations are complete>

C<Last sim finished at T>

X is the number of simulations currently completed.  X is greater than or equal to N, the total number of simulations to run.  T is the specific time the last simulation finished.

=back

=head2 SIGTERM (Signal/Interrupt #15)

If a SIGTERM is issued to this script (e.g., with C<kill -15>), then this script will issue a SIGTERM to each of the running simulations (its child processes) and then exit.

Note: Issuing a SIGKILL (e.g., with C<kill -9>) to batchexec_seus.pl will kill the script but running simulations will not be killed.  This script's SIGTERM handler terminates the running simulations before exiting.

=head1 TODO

The following is a list of known bugs to fix, potential revisions, or potential additions which are ordered by urgency (e.g., HIGH, MODERATE, or LOW):

=over

=item *

(LOW Revision) Simplify finding all netlists to simulate; just use one way to gather netlists regardless if simulating with/without netlists in subdirectories

=item *

(LOW Addition) Allow multiple directories to be provided in the command line arguments in order to run multiple batch-directories of netlists at once (e.g., C<batchexec_seus.pl dir1 [dir2] [dir3] [...]>)

=back

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

