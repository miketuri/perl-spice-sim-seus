#!/usr/bin/perl -w
use strict;
use warnings;

my $VERSION = '1.1.4';

# SEUS Package's mkout_seus.pl:
#   Generates multiple output file formats from ngspice .o file output
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'mkout_seus.pl';

# License
my $License = "\n".
"SEUS $Script_Name Copyright (C) 2009-2020 Michael A. Turi\n".
"This program comes with ABSOLUTELY NO WARRANTY.  This is free software,\n".
"  and you are welcome to redistribute it under certain conditions.\n".
"Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')\n".
"  in the SEUS package for more details.\n\n";

my $Fmt_Output = '.o';

my $Ezwave_Csv_Out_Format = '.csv';   # for Mentor Graphics EZWave
my $Waveview_Dat_Out_Format = '.dat'; # for Synopsys WaveView
my $Ezwave_Sti_Out_Format = '.sti';   # for EZWave or WaveView
my $Cscope_Txt_Out_Format = '.txt';   # for Synopsys CosmosScope

# Usage:
my $Usage = ''.
"Usage: $Script_Name  [OPTIONS]  spice_deck_name\n".
"Options: (by default, all output formats are generated)\n".
"  -c  Create .csv output\n".
"  -d  Create .dat output\n".
"  -l  Suppress display of SEUS's AGPL-3.0-or-later license information\n".
"  -s  Create .sti output\n".
"  -x  Create .txt output\n";
my $Min_Arg_Num = 1;

# mkout_seus.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. & Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is a Perl script used to convert the output from ngspice or an
# ngspice-based simulator (this was originally designed for the University of
# Florida Double-Gate (UFDG) MOSFET model for FinFETs) to more usable formats.
# The script converts the ngspice/Spice3 output to ".csv", ".dat", ".sti", and
# ".txt" files for use with Mentor Graphics EZWave, Synopsys CosmosScope, and
# Synopsys WaveView.
# 
# Usage: Provide the Spice deck name, and the "deck_name.o" output file will
#        be converted into "deck_name.csv", "deck_name.dat", "deck_name.sti",
#        and "deck_name.txt".
#
# Note: If the signal name is too long, ngspice/Spice3 will truncate the
#       signal name when creating the ".o" output file--this will cause an
#       error for this script.  It appears the maximum signal name size is
#       15 characters (including characters for v() or #branch).

print "** $Script_Name ** SEUS Package Version: $VERSION **\n";
die $Usage if scalar @ARGV == 0;
my $argnum = scalar @ARGV;

my $i = 0;
my $arg = shift;
my $op_csv_ezwave; # -c
my $op_dat_waveview; # -d
my $op_skip_lic; # -l
my $op_sti_ezwave; # -s
my $op_txt_cscope; # -x
my $user_def_output;
while( $arg =~ m/^-/ ) {
  if( $arg =~ m/^-(\S+)/ ) { # Found options
    my $argmatch = $1;
    if( $argmatch =~ m/c/i ) {
      $op_csv_ezwave = 1; # Create .csv output for Mentor Graphics EZwave
      $user_def_output = 1;
    }
    if( $argmatch =~ m/d/i ) {
      $op_dat_waveview = 1; # Create .dat output for Synopsys WaveView
      $user_def_output = 1;
    }
    if( $argmatch =~ m/l/i ) { # Suppress printing license information
      $op_skip_lic = 1;
    }
    if( $argmatch =~ m/s/i ) {
      $op_sti_ezwave = 1; # Create .sti output for MG EZwave or Syn. WaveView
      $user_def_output = 1;
    }
    if( $argmatch =~ m/x/i ) {
      $op_txt_cscope = 1; # Create .txt output for Synopsys CosmosScope
      $user_def_output = 1;
    }
  }
  die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
  $arg = shift;
}
print $License unless defined $op_skip_lic;
die "Error: Not enough parameters\n\n$Usage" if $argnum < $Min_Arg_Num;

my $deckname = $arg;

# Don't generate output for filetypes that the user doesn't define (unless none are defined, then generate all)
my $no_csv_ezwave;
if( !defined $op_csv_ezwave and defined $user_def_output ) {
  $no_csv_ezwave = 1;
}
my $no_dat_waveview;
if( !defined $op_dat_waveview and defined $user_def_output ) {
  $no_dat_waveview = 1;
}
my $no_sti_ezwave;
if( !defined $op_sti_ezwave and defined $user_def_output ) {
  $no_sti_ezwave = 1;
}
my $no_txt_cscope;
if( !defined $op_txt_cscope and defined $user_def_output ) {
  $no_txt_cscope = 1;
}

# --- Find the signal names and prepare for reading data ---
my @sig_names;
my @sig_hdrs;
my @fhandles;
$i = 0;
# Open the Spice output file
my $outname = $deckname.$Fmt_Output;
open IN, "<$outname" or die 'Error: Cannot open ', $outname, ' due to: ', $!;
my $match;
while( <IN> ) {
  $match = 0;
  if( m/^Index/i ) { # A signal header is found
    foreach my $hdr (@sig_hdrs) {
      $match = 1 if $_ eq $hdr;
    }
    # Only extract signal names from unique signal headers (sig. headers repeat throughout the file)
    if( $match == 0 ) {
      my @tmpsigs = split; # Tokenizes $_ (header) to get signals
      # Ignore "Index" (1st column) and only add "Time"/"Sweep"/etc. (2nd column) once
      push @sig_names, ((scalar @sig_hdrs == 0) ? splice @tmpsigs, 1 : splice @tmpsigs, 2);
      push @sig_hdrs, $_; # Add this signal header to the list
      my $dashes = <IN>; # Grab line of dashes "----"
      # Open a file handle to begin reading from Index 0 of this data
      # Multiple file handles are used for multiple pieces of data in different locations in the file
      open $fhandles[$i], "<$outname" or die 'Error: Cannot open ', $outname, ' due to: ', $!;
      seek $fhandles[$i++], (tell IN), 0; # (tell IN) is the location of Index 0 for this data
    }
  }
}
close IN;

die 'Error: No plotted signals found (is DC op point only present in ', $outname, '?)' if scalar @sig_names == 0;

# --- Initialize the new output files with the signal names ---
my $cs_txt_name = $deckname.$Cscope_Txt_Out_Format;
open CSCOPE_TXT, ">$cs_txt_name" or die 'Error: Cannot create CosmosScope ', $Cscope_Txt_Out_Format, ' output due to ', $! unless defined $no_txt_cscope;
my $wv_dat_name = $deckname.$Waveview_Dat_Out_Format;
open WAVEVIEW_DAT, ">$wv_dat_name" or die 'Error: Cannot create WaveView ', $Waveview_Dat_Out_Format, ' output due to ', $! unless defined $no_dat_waveview;
my $ez_csv_name = $deckname.$Ezwave_Csv_Out_Format;
open EZWAVE_CSV, ">$ez_csv_name" or die 'Error: Cannot create EZWave ', $Ezwave_Csv_Out_Format, ' output due to ', $! unless defined $no_csv_ezwave;
print EZWAVE_CSV ".HEADER\n..NAMES\n" unless defined $no_csv_ezwave;
my @ezwave_csv_names;
my @ezwave_sti_names;
my %sig_type; # The type of signal, voltage (V) or current (I), for a given signal name (for EZWave .sti output)
my $sim_type;
$i = 0;
foreach (@sig_names) {
  if( $i++ != 0 ) { # Delimeter beteen signal names
    print CSCOPE_TXT   '  ' unless defined $no_txt_cscope;
    print WAVEVIEW_DAT '  ' unless defined $no_dat_waveview;
    print EZWAVE_CSV   ','  unless defined $no_csv_ezwave;
  }
  if( m/^v\((\w+)\)$/i ) { # Found a voltage signal
    $sig_type{$1} = 'V';
    push @ezwave_sti_names, $1;
    print CSCOPE_TXT   "v($1)`V" unless defined $no_txt_cscope;
    print WAVEVIEW_DAT "v($1)"   unless defined $no_dat_waveview;
    print EZWAVE_CSV   "V($1)"   unless defined $no_csv_ezwave;
    push @ezwave_csv_names, "V($1)";
  } elsif( m/^(\w+)\#branch$/i ) { # Found a current signal
    $sig_type{$1} = 'I';
    push @ezwave_sti_names, $1;
    print CSCOPE_TXT   "i($1)`A" unless defined $no_txt_cscope;
    print WAVEVIEW_DAT "i($1)"   unless defined $no_dat_waveview;
    print EZWAVE_CSV   "I($1)"   unless defined $no_csv_ezwave;
    push @ezwave_csv_names, "I($1)";
  } elsif( m/^time$/i ) { # Found "time" => a TRAN analysis
    $sim_type = 'TRAN';
    print CSCOPE_TXT   't`s'  unless defined $no_txt_cscope;
    print WAVEVIEW_DAT 'TIME' unless defined $no_dat_waveview;
    print EZWAVE_CSV   'Time' unless defined $no_csv_ezwave;
    push @ezwave_csv_names, 'Time';
  } elsif( m/^v-sweep$/i ) { # Found "v-sweep" => a DC analysis (ngspice)
    $sim_type = 'DC';
    print CSCOPE_TXT   'VOLTS`V' unless defined $no_txt_cscope;
    print WAVEVIEW_DAT 'XVAL'    unless defined $no_dat_waveview;
    print EZWAVE_CSV   'Voltage' unless defined $no_csv_ezwave;
    push @ezwave_csv_names, 'Voltage';
  } elsif( m/^i-sweep$/i ) { # Found "i-sweep" => a DC analysis (ngspice)
    $sim_type = 'DC';
    print CSCOPE_TXT   'AMPS`A'  unless defined $no_txt_cscope;
    print WAVEVIEW_DAT 'XVAL'    unless defined $no_dat_waveview;
    print EZWAVE_CSV   'Current' unless defined $no_csv_ezwave;
    push @ezwave_csv_names, 'Current';
  } elsif( m/^sweep$/i ) { # Found "sweep" => a DC analysis (spice3f5)
    $sim_type = 'DC';
    print CSCOPE_TXT   'Sweep' unless defined $no_txt_cscope;
    print WAVEVIEW_DAT 'XVAL'  unless defined $no_dat_waveview;
    print EZWAVE_CSV   'Sweep' unless defined $no_csv_ezwave;
    push @ezwave_csv_names, 'Sweep';
  } else {
    die 'Cannot understand input signal: ', $_, '(was the signal name too long and truncated?)';
  }
}
print CSCOPE_TXT "\n" unless defined $no_txt_cscope;
print WAVEVIEW_DAT "\n" unless defined $no_dat_waveview;

# Output the plotted signal names
print 'Plotted signals for "', $deckname, '" are: ', (join ' ', @ezwave_csv_names), "\n";

# More fomatting for EZWave follows:
if( !defined $no_csv_ezwave ) {
  print EZWAVE_CSV "\n..UNITS\n";
  $i = 0;
  foreach (@ezwave_csv_names) {
    if( $i++ != 0 ) { # EZWave .csv is comma deliminated
      print EZWAVE_CSV ',';
    }
    if( m/^V\(/ ) { # Voltage signal measured in Volts
      print EZWAVE_CSV 'Voltage(V)';
    } elsif( m/^I\(/ ) { # Current signal measured in Amps
      print EZWAVE_CSV 'Current(A)';
    } elsif( $_ eq 'Time' ) { # Time is measured in seconds
      print EZWAVE_CSV 's';
    } elsif( $_ eq 'Voltage' ) { # Voltage sweep meas. in V (ngspice)
      print EZWAVE_CSV 'V';
    } elsif( $_ eq 'Current' ) { # Current sweep meas. in A (ngspice)
      print EZWAVE_CSV 'A';
    } elsif( $_ eq 'Sweep' ) { # An old sweep (spice3f5); don't know units
      print EZWAVE_CSV ' ';
    } else {
      die 'Corrupted EZWave input signal for .csv output: ', $_;
    }
  }
  # All data types are doubles
  print EZWAVE_CSV "\n..DATATYPES\n";
  for( $i = 1; $i < scalar @ezwave_csv_names; $i++ ) {
    print EZWAVE_CSV 'double,';
  }
  print EZWAVE_CSV "double\n..WAVEFORM_TYPES\n"; # Also prints last 'double'
  $i = 0;
  foreach (@ezwave_csv_names) {
    if( $i++ != 0 ) {
      print EZWAVE_CSV ',';
    }
    if( m/^[IV]\(/ ) { # All voltage/current signals are 'analog' type
      print EZWAVE_CSV 'analog';
    }
  }
  print EZWAVE_CSV "\n..AXIS_SPACING\nlinear\n..FOLDER_NAME\n$sim_type\n.DATA\n";
}

# --- Print the numeric data to each new output file ---
my @xaxis_vals; # The x-axis (time, sweep, etc.) values for EZWave .sti output
my %sig_vals; # The space-delimited values for a given signal name (for EZWave .sti output)
my $signum; # Keeps track of what piece of data goes with which signal for EZWave .sti output
$i = 0; # The current "Index" value that the file descriptors are at in the .o file
my $fnum; # Keeps track of how many file descriptors have been looked at for each "Index" value
while(1) {
  $fnum = 0;
  $signum = 0;
  foreach my $fh (@fhandles) {
    my $data_line;
    while( <$fh> ) { # Loop until data is found (bypasses duplicate signal headers)
      if( m/^\d/ ) { # Found an index value (always positive)
        $data_line = $_;
        last;
      }
    }
    goto THE_END unless defined $data_line;

    # Tokenize line to get each data value
    my @data = split ' ', $data_line;
    # If the index from the output file ($data[0]) is not equal
    #   to the index $i, then the end of the data has been reached.
    #   Unless the file is corrupt, $data[0] will equal 0 again when
    #   the next set of signals are reached, at this point, all data
    #   has been processed.
    goto THE_END if $data[0] != $i;
    if( $fnum++ == 0 ) { # Print "Time"/"Sweep"/etc. only once
      push @xaxis_vals, $data[1] unless defined $no_sti_ezwave;
      print CSCOPE_TXT $data[1] unless defined $no_txt_cscope;
      print WAVEVIEW_DAT $data[1] unless defined $no_dat_waveview;
      print EZWAVE_CSV $data[1] unless defined $no_csv_ezwave;
    }
    foreach (splice @data, 2) {
      my $signame = $ezwave_sti_names[$signum++];
      $sig_vals{$signame} = (exists $sig_vals{$signame} ? $sig_vals{$signame}.' '.$_ : $_) unless $no_sti_ezwave;
      # Print the data value with appropriate delimiter
      print CSCOPE_TXT "  $_" unless defined $no_txt_cscope;
      print WAVEVIEW_DAT "  $_" unless defined $no_dat_waveview;
      print EZWAVE_CSV ",$_"  unless defined $no_csv_ezwave;
    }
  }
  print CSCOPE_TXT "\n" unless defined $no_txt_cscope;
  print WAVEVIEW_DAT "\n" unless defined $no_dat_waveview;
  print EZWAVE_CSV "\n" unless defined $no_csv_ezwave;
  $i++;
}

# Close all files when done (we'll do EZWave .sti output next)
THE_END: foreach (@fhandles) {
  close $_;
}
close CSCOPE_TXT unless defined $no_txt_cscope;
close WAVEVIEW_DAT unless defined $no_dat_waveview;
close EZWAVE_CSV unless defined $no_csv_ezwave;

exit if defined $no_sti_ezwave;
# Aha, but there's more...print output for EZWave .sti output if necessary

my $ez_sti_name = $deckname.$Ezwave_Sti_Out_Format;
open EZWAVE_STI, ">$ez_sti_name" or die 'Error: Cannot create EZWave ', $Ezwave_Sti_Out_Format, ' output due to ', $!;

foreach (@ezwave_sti_names) {
  print EZWAVE_STI "\n", $sig_type{$_}, $_, ' ', $_, " 0 PWL (\n"; # E.g., Vbit bit 0 PWL (
  if( !exists $sig_vals{$_} ) {
    close EZWAVE_STI;
    die 'Internal Error: Data values of "', $_, '" not captured for EZWave ', $Ezwave_Sti_Out_Format, ' output';
  }
  my @data_vals = split ' ', $sig_vals{$_};
  if( scalar @xaxis_vals != scalar @data_vals ) {
    close EZWAVE_STI;
    die 'Internal Error: Number of x-axis values is not equal to the number of data values of "', $_, '" for EZWave ', $Ezwave_Sti_Out_Format, ' output';
  }
  for( $i = 0; $i < scalar @xaxis_vals; $i++ ) {
    print EZWAVE_STI '+ ', $xaxis_vals[$i], ' ', $data_vals[$i], "\n";
  }
  print EZWAVE_STI "+ )\n";
}

close EZWAVE_STI;


=head1 NAME

mkout_seus.pl - A Perl script from the SEUS (Scripts for Easier Use of Spice) Package used to generate multiple output file formats from ngspice .o file output

=head1 VERSION

Version 1.1.4

=head1 SYNOPSYS

    mkout_seus.pl  [OPTIONS]  spice_deck_name

Run mkout_seus.pl on a command line and provide the spice_deck_name (e.g., if a spice netlist named I<inverter.i> was simulated and generated output named I<inverter.o>, then use I<inverter> as the spice_deck_name) to convert the .o output to .csv, .dat, .sti, and .txt output formats (e.g., for I<inverter.o>, mkout_seus.pl can generate I<inverter.csv>, I<inverter.dat>, I<inverter.sti>, and I<inverter.txt>).  Mentor Graphics EZWave can use the .csv and .sti output formats.  Synopsys CosmosScope can use the .txt output format.  Synopsys WaveView can use the .dat and .sti output formats.  Or, a spreadsheet program, such as Microsoft Excel or LibreOffice Calc, can use the .csv, .dat, and .txt output formats.

Note 1: If using the SEUS Package's MeasSeus.pm to make measurements from the output, then the piecewise linear (PWL), .sti output format is required.

Note 2: The .o output file must have waveform data in order for this script to convert the output to other output file formats.  For example, if running ngspice with the -r option, then a .raw output file is generated and the .o file only contains error and warning information; this script cannot convert this .o file to another output file format since no waveform data is present.

mkout_seus.pl will print its usage to STDERR and exit if it is run without any command-line arguments.

=head1 LIMITATIONS

Note: If a signal name is too long, ngspice/Spice3 will truncate the signal name when creating the .o output file.  This limitation of ngspice/Spice3 will cause an error for this script.  The maximum signal name size appears to be 15 characters; this includes characters "v()" for voltages and "#branch" for currents.  Therefore a net name can only have a maximum of 12 characters if outputting its voltage, and a source name can only have a maximum of 8 characters if outputting its current.

=head1 OPTIONS

To see all available options, run mkout_seus.pl without any command-line arguments to print its usage, including all options, to STDERR.

By default, if no options are provided, then all output formats (.csv, .dat, .sti, and .txt) are created from the I<spice_deck_name.o> output file.

=head2 -c  Create .csv output

Create the .csv output format (a comma-separated data format) from the .o output file.  This can be used by Mentor Graphics EZWave or a spreadsheet program.

All other output formats are not created, unless additional options are provided to specify additional output formats to create.  E.g., the options -cd will create only the .csv and .dat output formats, and not the .sti and .txt output formats.

=head2 -d  Create .dat output

Create the .dat output format (a space-separated data format) from the .o output file.  This can be used by Synopsys WaveView or a spreadsheet program.

All other output formats are not created, unless additional options are provided to specify additional output formats to create.  E.g., the options -cd will create only the .csv and .dat output formats, and not the .sti and .txt output formats.

=head2 -l  Suppress display of SEUS's AGPL-3.0-or-later license information

If the -l option is passed, then the short description of the license is not printed. The seus.pl script, the main entry point for running SEUS scripts, passes the -l option since it prints the license information itself.

=head2 -s  Create .sti output

Create the .sti output format (a piecewise linear (PWL) data format) from the .o output file.  This can be used by Mentor Graphics EZWave, Synopsys WaveView, or the SEUS Package's MeasSeus.pm.

All other output formats are not created, unless additional options are provided to specify additional output formats to create.  E.g., the options -sx will create only the .sti and .txt output formats, and not the .csv and .dat output formats.

=head2 -x  Create .txt output

Create the .txt output format (a space-separated data format) from the .o output file.  This can be used by Synopsys CosmosScope or a spreadsheet program.

All other output formats are not created, unless additional options are provided to specify additional output formats to create.  E.g., the options -sx will create only the .sti and .txt output formats, and not the .csv and .dat output formats.

=head1 TODO

The following is a list of known bugs to fix, potential revisions, or potential additions which are ordered by urgency (e.g., HIGH, MODERATE, or LOW):

=over

=item *

(MODERATE Revision) Handle truncated signal names (if too long and truncated by spice)

=item *

(MODERATE Addition) Add AC support

=item *

(LOW Addition) Add DC multi-sweep support (at least 2 sweeps)

=item *

(LOW Addition) Check support of v(a, b) for v(a)-v(b)

=item *

(LOW Addition) Add more constants in Perl source code (instead of hard-coded values)

=back

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

