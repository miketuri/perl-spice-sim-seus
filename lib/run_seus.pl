#!/usr/bin/perl -w
use strict;
use warnings;

my $VERSION = '1.1.4';
my $SPICE_EXEC = 'ngspice'; # Define default Spice executable here

# SEUS Package's run_seus.pl:
#   More easily run an ngspice or ngspice-based simulation
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'run_seus.pl';

# License
my $License = "\n".
"SEUS $Script_Name Copyright (C) 2009-2020 Michael A. Turi\n".
"This program comes with ABSOLUTELY NO WARRANTY.  This is free software,\n".
"  and you are welcome to redistribute it under certain conditions.\n".
"Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')\n".
"  in the SEUS package for more details.\n\n";

# Requires scripts:
my $Mkout_Seus = 'mkout_seus.pl';
my $Netgen_Seus = 'netgen_seus.pl';
my $Netgen_Options = '-lzi';

# Constants:
my $Fmt_Input = '.i';
my $Fmt_Output = '.o';
my $Fmt_Raw = '.raw';

my $Timer_Tick_Amt = 10; # Number of seconds per 'tick'

# Signal (interrupt) handler for SIGUSR1 (#10):
my $simtime = 0;
# Procedure: get_simtime_handler
# Summary:
#   Returns the simulation duration upon exiting
sub get_simtime_handler {
  exit $simtime;
}
$SIG{'USR1'} = \&get_simtime_handler;

# Signal (interrupt) handler for SIGTERM (#15):
my $spice_pid;
my $timer_pid = -1;
# Procedure: clean_term_handler
#
#   If the terminate signal is given, also kills each simulation child process
#   (Spice and timer)
sub clean_term_handler {
  kill 'TERM', $spice_pid if defined $spice_pid;
  kill 'TERM', $timer_pid if defined $timer_pid and $timer_pid > 0;
  exit 128+15;
}
$SIG{'TERM'} = \&clean_term_handler;

# Usage:
my $Usage = ''.
'Usage: '.$Script_Name."  [OPTIONS]  spice_deck_name\n".
"Options:\n".
"  -l       Suppress display of SEUS's AGPL-3.0-or-later license information\n".
"  -n       Let simulator (instead of ".$Netgen_Seus.") insert include files and\n".
"             make .param substitutions\n".
"  -o path  Run other Spice executable at \"path\"\n".
"  -r       Generate ".$Fmt_Raw." output file for simulation output\n".
"             (".$Mkout_Seus." will not run; only errors/warnings in .o file)\n".
"  -u path  Use \"path\" for location of the SEUS Package (if SEUS not installed)\n".
"  For Timer:\n".
"    -#           Run with timer length=\"#\" sec; kill Spice if timer expires\n".
"    -t           Run with no timer\n".
"    -p filename  Print (append) the simulation duration to file \"filename\"\n".
"  For ".$Mkout_Seus." (by default, all output formats are generated):\n".
"    -m  Do not create additional output formats (do not call ".$Mkout_Seus.")\n".
"    -c  Create .csv output\n".
"    -d  Create .dat output\n".
"    -s  Create .sti output\n".
"    -x  Create .txt output\n";
my $Min_Arg_Num = 1;

# run_seus.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. and Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is a Perl script used to more-easily run ngspice or an ngspice-based
# simulator.  Originally this was designed for an ngspice-based simulator, the
# University of Florida Double-Gate (UFDG) MOSFET model for FinFETs, but this
# has also been tested for regular ngspice.  This script may no longer work
# correctly for spice3e2-based simulators.  Only the spice file's deck name
# needs to be passed as an argument.  This can also generate the .raw output
# file for the Nutmeg simulator to use (but if .raw file is generated, then
# the .o file has only error/warning info and mkout_seus.pl will not run).
# A timer is also supplied to display the simulation's run time.  It can also
# be used to kill the simulation if it runs too long.
# 
# Usage: Provide the Spice deck name and any additional options.  Creates
#        "spice_deck_name.o" (plus other output formats produced by
#        mkout_seus.pl) from "spice_deck_name.i".

print "** $Script_Name ** SEUS Package Version: $VERSION **\n";
die $Usage if scalar @ARGV < 1;
my $argnum = scalar @ARGV;

my $spice_path = $SPICE_EXEC; # Use default Spice executable
                              #   Unless option "-o path" specifies differently

my $i = 0;
my $arg = shift;
my $op_skip_lic; # -l
my $op_sim_sub; # -n
my $op_make_raw; # -r
my $seus_path; # -u path
# Options for timer
my $timer_val = 0; # -# and -t
my $simlen_file; # -p filename
# Options for mkout_seus.pl
my $op_no_mkout; # -m
my $mkout_opts = 'l'; # Can add -cdsx (-l is suppressing license)
while( $arg =~ m/^-/ ) {
  if( $arg =~ m/^-(\S+)/ ) { # Found options
    my $argmatch = $1;
    
    # Check single letter options first
    if( $argmatch =~ m/l/i ) { # Suppress printing license information
      $op_skip_lic = 1;
    }
    if( $argmatch =~ m/n/i ) { # Let simulator insert include files & make .param substitutions
      $op_sim_sub = 1;
    }
    if( $argmatch =~ m/r/i ) { # Generate .raw output file
      $op_make_raw = 1;
    }
    if( $argmatch =~ m/(\d+)/ ) { # Set timer to numerical value
      $timer_val = $1;
    }
    if( $argmatch =~ m/t/i ) { # Run with no timer
      $timer_val = -1 unless defined $simlen_file;
    }
    if( $argmatch =~ m/m/i ) { # Do not run mkout_seus.pl (do not create additional output formats)
      $op_no_mkout = 1;
    }
    if( $argmatch =~ m/c/i ) { # Create .csv output for Mentor Graphics EZwave
      $mkout_opts .= 'c';
    }
    if( $argmatch =~ m/d/i ) { # Create .dat output for Synopsys WaveView
      $mkout_opts .= 'd';
    }
    if( $argmatch =~ m/s/i ) { # Create .sti output for Mentor Graphics EZwave
      $mkout_opts .= 's';
    }
    if( $argmatch =~ m/x/i ) { # Create .txt output for Synopsys CosmosScope
      $mkout_opts .= 'x';
    }
    
    # Check options which require another argument
    if( $argmatch =~ m/o/i ) { # Use other specified Spice executable
      $spice_path = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/p/i ) { # Print the simulation duration to file (expecting .csv file)
      $simlen_file = shift;
      $timer_val = 0 if $timer_val < 0;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/u/i ) { # Use a path to a local location of the SEUS Package
      $seus_path = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
  }
  die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
  $arg = shift;
}
print $License unless defined $op_skip_lic;
die "Error: Not enough parameters\n\n$Usage" if $argnum < $Min_Arg_Num;

my $deckname = $arg;

if( defined $seus_path ) { # Use SEUS path if provided
  if( $seus_path !~ m/\/$/ ) { $seus_path .= '/'; } # Add trailing slash if it doesn't exist
} else { # else just call script names and hope the SEUS package is installed
  $seus_path = '';
}

# Need path to the spice executable
# This fails if $SPICE_EXEC is not found or an different spice executable was not passed
# via command line with "-o" option
die 'Error: The path to the spice executable is not defined' unless defined $spice_path;

# --- Check input file and delete output files ---
my $infile = $deckname.$Fmt_Input;
my $outfile = $deckname.$Fmt_Output;
my $rawfile = $deckname.$Fmt_Raw;
# Delete output files if it already exists; ngspice output may append
if( -e $outfile ) { unlink $outfile or die 'Error: "unlink ', $outfile, '" somehow failed'; }
if( -e $rawfile ) { unlink $rawfile or die 'Error: "unlink ', $rawfile, '" somehow failed'; }
die 'Error: Cannot find "', $infile, '"' unless -e $infile;

# --- First fork(); the child is the Spice executable ---
$spice_pid = fork;
die "Error: Fork of Spice failed" unless defined $spice_pid;
if( $spice_pid == 0 ) { # The child
  # Do I/O redirection manually
  close( STDIN );
  if( defined $op_sim_sub ) { # Let simulator insert include files & make .param substitutions
    open STDIN, "<$infile" or die 'Error: Cannot redirect Spice input to "', $infile, '"';
  } else { # Let netgen script insert include files and make .param substitutions
    open STDIN, "$seus_path$Netgen_Seus $Netgen_Options $infile |" or die 'Error: Cannot redirect Spice input to "', $Netgen_Seus, ' ', $Netgen_Options, ' ', $infile, ' |"';
  }
  close( STDOUT );
  open STDOUT, ">$outfile" or die 'Error: Cannot redirect Spice output to "', $outfile, '"';
  
  # Add rawfile option if needed, then exec
  $spice_path = $spice_path.' -r '.$rawfile if defined $op_make_raw;
  exec $spice_path or die 'Error: Could not exec "', $spice_path, '"';
}

# --- Second fork(); the child is the timer ---
$timer_pid = fork if $timer_val >= 0;
die 'Error: Fork of timer failed' unless defined $timer_pid;
if( $timer_pid == 0 ) { # The child
  $simtime = 1;
  if( $timer_val > 0 ) { # Run timer with alarm
    my $ticks = $timer_val / $Timer_Tick_Amt;
    my $rem = $timer_val % $Timer_Tick_Amt;
    while( $simtime <= $ticks ) {
      sleep $Timer_Tick_Amt;
      print $Timer_Tick_Amt*$simtime++, "s elapsed...\n";
    }
    if( $rem > 0 ) {
      sleep $rem;
      print $timer_val, "s elapsed...\n";
    }
  } else { # Just display timer and run forever
    while(1) {
      sleep $Timer_Tick_Amt;
      print $Timer_Tick_Amt*$simtime++, "s elapsed...\n";
    }
  }
  exit $timer_val;
}

# --- This (parent) process will wait for Spice or timer to die ---
my $pid;
do {
  $pid = wait;
} while( $pid != $spice_pid and $pid != $timer_pid );
my $retval = $? >> 8;

if( defined $simlen_file ) {  # Print the simulation duration to file (expecting .csv file)
  open OUTAPP, ">>$simlen_file" or die 'Error: Cannot open ', $simlen_file, ' for append due to', $!;
}

# --- Figure out if Spice or timer died first; kill other child ---
# --- Also, if timer died first, kill Spice and exit; sim was not completed ---
my $simlength;
if( $pid == $spice_pid ) {
  print 'Spice "', $deckname, '" died first (status=', $retval, ")--killing timer\n";
  if( $timer_val >= 0 ) { # Do a kill -SIGUSR1 "timer" and get sim length (if there is a timer)
    kill 'USR1', $timer_pid;
    do {
      $pid = wait;
    } while( $pid != $timer_pid );
    $simlength = $? >> 8;
    print $deckname, ' ran for approx. ', $simlength*$Timer_Tick_Amt, " seconds\n";
    if( defined $simlen_file ) {
      print OUTAPP $deckname, ',', $simlength*$Timer_Tick_Amt, "\n";
      close OUTAPP;
    }
  }
} else {
  print 'timer died first--killing Spice "', $deckname, "\"\n";
  kill 'TERM', $spice_pid; # Do a kill -SIGTERM "Spice"
  print $deckname, ' ran for ', $timer_val, "+ seconds\n";
  if( defined $simlen_file ) {
    print OUTAPP $deckname, ',', $timer_val, "+\n";
    close OUTAPP;
  }
  die 'Error: Spice timeout; simulation was killed';
}

# --- If Spice completed without error, make the output file(s) ---
if( $retval != 0 ) {
  print 'Warning: Spice returned with value=', $retval, "\n";
  exit $retval;
}

exit $retval if defined $op_make_raw or defined $op_no_mkout; # Do not run mkout_seus.pl (cannot run mkout_seus.pl using raw file)

# Passing options to mkout_seus.pl
my @mkout = ( "$seus_path$Mkout_Seus", '-'.$mkout_opts, $deckname);
system( @mkout ) == 0 or die 'Error: "', $Mkout_Seus, ' -', $mkout_opts, ' ', $deckname, '" failed';

exit $retval;


=head1 NAME

run_seus.pl - A Perl script from the SEUS (Scripts for Easier Use of Spice) Package used to more easily run an ngspice or ngspice-based simulation

=head1 VERSION

Version 1.1.4

=head1 SYNOPSYS

    run_seus.pl  [OPTIONS]  spice_deck_name

Run run_seus.pl on a command line and provide the spice_deck_name (e.g., to simulate a spice netlist named I<inverter.i>, use I<inverter> as the spice_deck_name) to simulate the netlist and generate the .o output file (e.g., I<inverter.o>).  This script works for ngspice and ngspice-based simulators, such as University of Florida's Spice3-UFDG (Linux version 3.71), but this script may no longer work correctly for spice3e2-based simulators.  The name of the spice executable is defined at the top of this script, or can be overridden by using the -o option.

This script calls netgen_seus.pl to remove comments, insert .include files, and make parameter substitutions.  This script also calls mkout_seus.pl to convert the .o output file to a different output format (.csv, .dat, .sti, and/or .txt output).  The run_seus.pl script can also pass the -r option to ngspice to generate the .raw output file, which can be used by the Nutmeg simulator; however, if the .raw output file is generated, then the .o output file will not contain any waveform data (it will only have error and warning information) so mkout_seus.pl will not be called.

This script will spawn (using fork) two children: (1) the ngspice simulation (using exec) and (2) a timer.  The timer is supplied to display the simulation's run time and it can be configured to terminate the simulation, if it runs too long.  This is helpful when a simulation has convergence issues and runs indefinitely without finishing.

run_seus.pl will print its usage to STDERR and exit if it is run without any command-line arguments.

=head1 OPTIONS

To see all available options, run run_seus.pl without any command-line arguments to print its usage, including all options, to STDERR.

By default, if no options are provided, then netgen_seus.pl will insert include files and make parameter substitutions prior to simulating the netlist (I<spice_deck_name.i>) using ngspice, a timer is used only to display the simulation's run time, and mkout_seus.pl will convert the output, I<spice_deck_name.o>, to the four other output file formats (.csv, .dat, .sti, and .txt).

=head2 -l  Suppress display of SEUS's AGPL-3.0-or-later license information

If the -l option is passed, then the short description of the license is not printed.  The seus.pl script, the main entry point for running SEUS scripts, passes the -l option since it prints the license information itself.

=head2 -n  Let the simulator (instead of netgen_seus.pl) insert include files and make parameter substitutions

This option will skip calling netgen_seus.pl (with options -lzi; suppressing printing the license or printing to STDERR, and reading the input netlist from file), before simulating the netlist with run_seus.pl.  The purpose of netgen_seus.pl is to remove all comments, insert include files, and make parameter substitutions in the netlist code prior to simulating the netlist.  The netlist is always redirected as the STDIN input to the simulator and the simulator's STDOUT is always redirected to the .o output file.  If netgen_seus.pl is used, the netlist, I<spice_deck_name.i>, is not modified, instead the STDOUT output from netgen_seus.pl (the modified netlist) is piped as the STDIN input to the simulator.

The reason for netgen_seus.pl is that an ngspice-based simulator (University of Florida's Spice3-UFDG (Linux version 3.71)) incorrectly handles parameter substitutions; the results vary depending on whitespace and comments.

Use this option to instead have ngspice insert include files and make parameter substitutions.

=head2 -o path  Run other Spice executable at "path"

By default, run_seus.pl will simulate netlists using "ngspice".  This option defines a different simulator to use (e.g., a different ngspice-based simulator).  For this to work properly, the simulator should run at "path" at the directory location where run_seus.pl is run, and the simulator must be a file that can be executed with exec.

=head2 -r  Generate .raw output file for simulation output (mkout_seus.pl will not run; only errors and warnings will appear in .o output file

By default, run_seus.pl executes ngspice and generates only .o file output.  Use this option to generate the .raw output file, which can be used by the Nutmeg simulator; however, if the .raw output file is generated, then the .o output file will not have any waveform data (it will only have error and warning information).  Therefore, run_seus.pl will not run mkout_seus.pl and no other output file formats can be generated.

=head2 -u path  Use "path" for the location of the SEUS Package (if SEUS Package not installed)

If the SEUS Package is successfully installed, then just providing the script name (e.g., run_seus.pl) is sufficient to run the script.  If the SEUS Package is not installed, then the path to the SEUS Package must be provided since, by default, this script will call netgen_seus.pl and mkout_seus.pl.

=head2 Options for the Timer

This script supplies a timer to display the simulation's run time, and the timer can be configured to terminate the simulation if it runs too long.  This is helpful when a simulation has convergence issues and runs indefinitely without finishing.  The following options configure the operation of the timer.

=head3 -#  Run with a timer length of "#" seconds; terminate the simulation if the timer expires

By default, the timer runs to just display the simulation's run time.  Use this option to instead terminate the simulation after # seconds; # can be any number of seconds (as long as # is a positive integer).  This is helpful when a simulation has convergence issues and runs indefinitely without finishing.

=head3 -t  Run with no timer

By default, the timer runs to just display the simulation's run time.  This script spawns a child process to be the timer.  Use this option to run without a timer.  The simulation's run time will not be displayed and the child timer process will not be spawned.

=head3 -p filename  Print (append) the simulation duration to file "filename"

Use this option to keep track of the length of the simulation.  Once either the simulation finishes, or the timer expires and terminates the simulation, the simulation duration will be appended to "filename" (on a new line).

=head2 Options for mkout_seus.pl

This script calls mkout_seus.pl once the simulation finishes to convert the .o output file into other, more usable file formats (.csv, .dat, .sti, and .txt).  Please refer to mkout_seus.pl's documentation for more information.  The following options configure the use of mkout_seus.pl.

=head3 -m  Do not create additional output formats (do not call mkout_seus.pl)

Use this option to not create any additional output formats.  mkout_seus.pl will not be called and the only output file present will be the .o output.

=head3 -c  Create .csv output

Create the .csv output format (a comma-separated data format) from the .o output file.  This can be used by Mentor Graphics EZWave or a spreadsheet program.

All other output formats are not created, unless additional options (-d, -s, and/or -x) are provided to specify additional output formats to create.  E.g., the options -cd will create only the .csv and .dat output formats, and not the .sti and .txt output formats.

=head3 -d  Create .dat output

Create the .dat output format (a space-separated data format) from the .o output file.  This can be used by Synopsys WaveView or a spreadsheet program.

All other output formats are not created, unless additional options (-c, -s, and/or -x) are provided to specify additional output formats to create.  E.g., the options -cd will create only the .csv and .dat output formats, and not the .sti and .txt output formats.

=head3 -s  Create .sti output

Create the .sti output format (a piecewise linear (PWL) data format) from the .o output file.  This can be used by Mentor Graphics EZWave, Synopsys WaveView, or the SEUS Package's MeasSeus.pm.

All other output formats are not created, unless additional options (-c, -d, and/or -x) are provided to specify additional output formats to create.  E.g., the options -sx will create only the .sti and .txt output formats, and not the .csv and .dat output formats.

=head3 -x  Create .txt output

Create the .txt output format (a space-separated data format) from the .o output file.  This can be used by Synopsys CosmosScope or a spreadsheet program.

All other output formats are not created, unless additional options (-c, -d, and/or -s) are provided to specify additional output formats to create.  E.g., the options -sx will create only the .sti and .txt output formats, and not the .csv and .dat output formats.

=head1 SIGNAL/INTERRUPT SUPPORT

This script only supports and responds to a user issuing the SIGTERM interrupt/signal.  This script internally uses SIGUSR1 (Signal/Interrupt #10) to communicate with the timer (the parent process issues the signal to obtain the simulation time and terminate the timer child process).  Issuing a SIGUSR1 to the parent run_seus.pl will terminate the process, but will not terminate the simulation or timer.  Issuing a SIGUSR1 to the timer will terminate the process, but will not terminate the parent run_seus.pl or the simulation.

=head2 SIGTERM (Signal/Interrupt #15)

If a SIGTERM is issued to this script (e.g., with C<kill -15>), then this script will issue a SIGTERM to the timer and the running simulation (its child processes) and then exit.

Note: Issuing a SIGKILL (e.g., with C<kill -9>) to run_seus.pl will kill the script but the timer and the running simulation will not be killed.  This script's SIGTERM handler terminates the timer and the running simulation before exiting.

=head1 TODO

The following is a list of known bugs to fix, potential revisions, or potential additions which are ordered by urgency (e.g., HIGH, MODERATE, or LOW):

=over

=item *

(LOW Addition) Append error messages (from .o file) to run_seus.pl output

=back

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

