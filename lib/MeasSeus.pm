package MeasSeus;

use strict;
use warnings;

use Exporter;

=head1 NAME

MeasSeus.pm - A Perl module from the SEUS (Scripts for Easier Use of Spice) Package that serves as a library of measurement procedures for a user's measurement script

=head1 VERSION

Version 1.1.4

=cut

# SEUS Package's MeasSeus.pm:
#   A library of measurement procedures for a user's measurement script
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

our $VERSION = '1.1.4';
use base 'Exporter';
our @EXPORT = qw(abs_waveform add_waveforms avg diff_waveforms get_signal_sti integ max min neg_waveform put_signal_sti sub_waveforms truncate_waveform xval yval);

=head1 SYNOPSYS

    use MeasSeus;

The subroutines contained in this Perl module support making measurements of spice simulation data.  The subroutines in this module mostly behave similar to the measurement functions from the Mentor Graphics EZWave simulator after which they were modeled.

Each subroutine uses at least one signal waveform of points as an argument; this signal typically represents a voltage or a current.  This Perl module assumes a waveform is represented by a hash; a hash key is a point's x-value and the corresponding hash value is the point's y-value, assuming that a waveform only has one y-value per x-value.  The get_signal_sti and put_signal_sti subroutines allow reading and writing waveforms to file, using piecewise linear (PWL) formatting in a .sti file.

=head1 SUBROUTINES

=cut

###############################################################################
#
# MeasSeus Perl Module (MeasSeus.pm)
#
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. & Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This Perl module defines measurement procedures to measure .sti waveform
# output from ngspice or an ngspice-based simulator.  The subroutines defined
# in this module (for the most part) behave similar to the measurement
# functions from Mentor Graphics EZWave simulator (from which they were
# modeled after).  This module assumes that waveform data is stored in hashes,
# where the key is the x-value (simulation time) and the value is the y-value
# (voltage or current value).
#
###############################################################################


###############################################################################

=head2 abs_waveform

    abs_waveform \%waveform;

Finds the absolute value of a waveform (take the absolute value of each y-value)

Input:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=back

Output:

=over

=item *

C<\%waveform> - The waveform result as a hash reference; the absolute value is taken for each of the hash's y-values

=back

=cut

sub abs_waveform {
  my $wave_ref = shift;

  foreach (keys %{$wave_ref}) {
    ${$wave_ref}{$_} = abs ${$wave_ref}{$_};
  }

  return;
}


###############################################################################

=head2 add_waveforms

    add_waveforms \%waveform_a, \%waveform_b, \%waveform_sum;

Adds two waveforms together to obtain a resultant sum waveform.  For each matching x-value "X", the y-values will be added (A + B) to form "YSUM" and this will form an xy-pair (X,YSUM) in the resultant sum waveform.  If one waveform contains an x-value not existing in the other waveform, then linear interpolation is used (i.e. "yval" is called) to generate an xy-pair to use in the sum.  If waveforms of different lengths are added, then the length of the resultant sum waveform will be shorter or equal to the length of the shorter waveform, since linear interpolation is used; the x-range will be from the larger (or later if the x-axis is time) of the two waveforms' starting x-values to the smaller (or earlier if the x-axis is time) of the two waveforms' ending x-values.

Inputs:

=over

=item *

C<\%waveform_a> - A hash reference where the hash keys are x-values and the hash values are y-values of the first waveform to add

=item *

C<\%waveform_b> - A hash reference where the hash keys are x-values and the hash values are y-values of the second waveform to add

=back

Output:

=over

=item *

C<\%waveform_sum> - The resultant sum waveform of the two input waveforms

=back

=cut

sub add_waveforms {
  my $wave_a = shift;
  my $wave_b = shift;
  my $wave_sum = shift;

  # Get a sorted lists of x-values for both waveforms (from the beginning of
  #   each waveform's x-values to end of each waveform's x-values)
  my @wave_a_xvals = sort {$a <=> $b} keys %{$wave_a};
  my @wave_b_xvals = sort {$a <=> $b} keys %{$wave_b};

  # Obtain what will be the first and last x-values of the sum waveform
  my $x_value_first = 
    ($wave_a_xvals[0] > $wave_b_xvals[0]) ? $wave_a_xvals[0] : $wave_b_xvals[0];
  my $x_value_last =
   ($wave_a_xvals[-1]<$wave_b_xvals[-1]) ? $wave_a_xvals[-1] : $wave_b_xvals[-1];

  # Shrink the ranges of the operand waveforms to do the sum correctly
  my %wave_a_trunc;
  truncate_waveform( $wave_a, $x_value_first, $x_value_last, \%wave_a_trunc );
  my %wave_b_trunc;
  truncate_waveform( $wave_b, $x_value_first, $x_value_last, \%wave_b_trunc );

  # Start sum and make sure all of wave A's x-values are included in sum waveform
  foreach( keys %wave_a_trunc ) {

    # If this x-value exists in both A and B, then just add.  If it does not
    #   exist in B, then we must call yval to interpolate it for B.
    ${$wave_sum}{$_} = $wave_a_trunc{$_} + 
     ((exists $wave_b_trunc{$_}) ? $wave_b_trunc{$_} : yval(\%wave_b_trunc, $_));
  }

  # Finish sum and ensure all of wave B's x-values are included in the sum wave
  foreach( keys %wave_b_trunc ) {

    if( !exists ${$wave_sum}{$_} ) { # Check if this x-value already added in sum

      # If this x-value does not exist in the sum yet, then it must not have
      #   existed in waveform A.  We must call yval to interpolate it for A.
      ${$wave_sum}{$_} = yval( \%wave_a_trunc, $_ ) + $wave_b_trunc{$_};
    }
  }

  return;
}


###############################################################################

=head2 avg

    my $avg_value = avg \%waveform, $x_begin, $x_end;

Finds the average y-value of a waveform over a range of x-values.  Note that linear interpolation is used if one or both of these range x-values do not exist as an x-value in the waveform.

Assumes:

=over

=item *

$x_begin and $x_end are valid positions in the waveform

=back

Inputs:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$x_begin> - A scalar x-value marking the beginning of the x-value range

=item *

C<$x_end> - A scalar x-value marking the end of the x-value range

=back

Output:

=over

=item *

C<$avg_value> - A scalar of the average y-value of the waveform's y-values corresponding to the specified x-value range

=back

Error Cases:

=over

=item *

The output will be undefined if $x_begin or $x_end is beyond the edge of the waveform's x-values; $x_begin and $x_end must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub avg {
  my $wave_ref = shift;
  my $x_begin = shift;
  my $x_end = shift;

  return integ( $wave_ref, $x_begin, $x_end )/($x_end - $x_begin);
}


###############################################################################
#
# Private Subroutine: binary_search
#
# Usage: $index = binary_search \@array, $search_value;
#
# Summary: Perform a binary search for a value in an array of numbers
#
# Assumes: The array is not empty and is sorted in increasing value
#
# Inputs: \@array - An array reference of a list of numbers sorted in increasing value
#         $search_value - A scalar value to search for in the array
#
# Output: $index - The index where the value is found, or would be found if
#                  it is not present, in the array
#
sub binary_search {
  my $array_ref = shift;
  my $search_value = shift;

  my $index; # Output value, the integration value

  # Set up indices for binary search
  my $lo = 0;
  my $hi = (scalar @{$array_ref})-1;
  my $mid;

  # Do a binary search to find the location where the search value would be
  while( $lo <= $hi ) {
    $mid = int(($lo + $hi)/2);
    if( $search_value < ${$array_ref}[$mid] ) { $hi = $mid-1; }
    else { $lo = $mid+1; }
  }

  return $mid;
}


###############################################################################

=head2 diff_waveforms

    diff_waveforms \%waveform1, $waveform1name, \%waveform2, $waveform2name, \%waveform_diff;

Performs a diff operation on two waveforms.  The result of this diff operation will be placed into the output hash "\%waveform_diff".  If the two waveform hashes are identical, then the resultant hash will be empty.  If both input hashes contain a key, but the values are different, then both key-value pairs are copied to the output hash and the waveform names will be appended to each key to identify the waveform hash from which it originated.  If only one input hash contains a key, then this key-value pair is copied to the output hash and the waveform name will be appended to the key to identify the waveform hash from which it originated.

Inputs:

=over

=item *

C<\%waveform1> - A hash reference where the hash keys are x-values and the hash values are y-values of the first waveform to diff

=item *

C<$waveform1name> - The name to use for the first waveform; this name will be appended to the hash key if there is a difference

=item *

C<\%waveform2> - A hash reference where the hash keys are x-values and the hash values are y-values of the second waveform to diff

=item *

C<$waveform2name> - The name to use for the second waveform; this name will be appended to the hash key if there is a difference

=back

Output:

=over

=item *

C<\%waveform_diff> - The resultant waveform hash of the diff.  If the hash is empty, then the waveforms are the same.  If the hash is not empty, then the name of the origin waveform is appended to each key to identify the waveform hash from which it originated.

=back

Error Cases:

=over

=item *

$waveform1name and $waveform2name must be different, or hash keys will match and values will be overwritten in the diff waveform hash

=back

=cut

sub diff_waveforms {
  my $wave1 = shift;
  my $wave1name = shift;
  my $wave2 = shift;
  my $wave2name = shift;
  my $wave_diff = shift;

  foreach( keys %{$wave1} ) { # Start by going through each key of waveform 1
  
    if( exists ${$wave2}{$_} ) { # Check if this x-value is already in waveform 2
    
      if( ${$wave1}{$_} != ${$wave2}{$_} ) {
        # This x-value is in waveform 2, but is the y-value different?
        # If yes, then add point to diff waveform (waveform 2's point will be added next)
        ${$wave_diff}{$_.$wave1name} = ${$wave1}{$_};
      }
    
    } else { # Waveform 2 does not have this x-value, so add point to diff waveform
      ${$wave_diff}{$_.$wave1name} = ${$wave1}{$_};
    }
  }
  
  foreach( keys %{$wave2} ) { # Now go through each key of waveform 2
  
    if( exists ${$wave1}{$_} ) { # Check if this x-value is already in waveform 1
    
      if( ${$wave1}{$_} != ${$wave2}{$_} ) {
        # This x-value is in waveform 1, but is the y-value different?
        # If yes, then add point to diff waveform (waveform 1's point will added previously)
        ${$wave_diff}{$_.$wave2name} = ${$wave2}{$_};
      }
    
    } else { # Waveform 1 does not have this x-value, so add point to diff waveform
      ${$wave_diff}{$_.$wave2name} = ${$wave2}{$_};
    }
  }

  return;
}


###############################################################################

=head2 get_signal_sti

    get_signal_sti $input_file_handle, $sig_name, $sig_type, \%waveform;

Imports waveform data from a piecewise linear (PWL), .sti-formatted output file into a hash

Assumes:

=over

=item *

$input_file_handle's file is open for reading and $input_file_handle points to the beginning of the file

=back

Inputs:

=over

=item *

C<$input_file_handle> - File handle of the input PWL, .sti-formatted file.  The file handle will be returned to the beginning of the file.

=item *

C<$sig_name> - Name of the signal to import

=item *

C<$sig_type> - Type of signal to import.  For a .sti file, this is typically either (V) voltage or (I) current.

=back

Output:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=back

Error Cases:

=over

=item *

The output will be empty if the signal is not found in the file

=back

=cut

sub get_signal_sti {
  my $fh = shift;
  my $sig_name = shift;
  my $sig_type = shift;
  my $wave_ref = shift;

  # Could save and restore the file handle position (if not requiring the
  #   file handle to point to the beginning of the file)
  #my $start_pos = tell $fh;

  # Search line-by-line for the signal name
  while( <$fh> ) {
    if( m/^$sig_type$sig_name\s+$sig_name\s+0\s+PWL\s\(\s*$/i ) {
      last;
    }
  }

  # If the signal is found, the following lines will be time (x-values) and
  #   voltage/current values (y-values) until the final parenthesis is found.
  # If the signal is not found, then the file handle should be at the end of
  #   file and the next while statement will be skipped.
  while( <$fh> ) {
    my @line = split;

    # Typically: 0     1           2
    #            +   time   voltage/current
    #
    # Or at end: +     )
    if( scalar @line < 3 or $line[1] eq ')' ) {
      last;
    }
    ${$wave_ref}{$line[1]+0} = $line[2]+0;
  }

  # Restore the file handle read location
  #seek $fh, $start_pos, 0; # Return file handle to its initial position
  seek $fh, 0, 0; # Return file handle to the beginning of the file

  return;
}


###############################################################################

=head2 integ

    my $integ_value = integ \%waveform, $x_begin, $x_end;

Integrates the y-values over a range of x-values for a waveform.  Note that linear interpolation is used if one or both of these range x-values do not exist as an x-value in the waveform.

Assumes:

=over

=item *

$x_begin and $x_end are valid positions in the waveform

=back

Inputs:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$x_begin> - A scalar x-value marking the beginning of the x-value range

=item *

C<$x_end> - A scalar x-value marking the end of the x-value range

=back

Output:

=over

=item *

C<$integ_value> - A scalar of the integrated y-values corresponding to the specified x-value range for the waveform

=back

Error Cases:

=over

=item *

The output will be undefined if $x_begin or $x_end is beyond the edge of the waveform's x-values; $x_begin and $x_end must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub integ {
  my $wave_ref = shift;
  my $x_begin = shift;
  my $x_end = shift;

  my $integ_value; # Output value, the integration value

  # Find the y-values at the beginning and end of the range of x-values
  my $y_begin = yval( $wave_ref, $x_begin );
  my $y_end = yval( $wave_ref, $x_end );

  # Check if these beginning and end y-values exist, fail if the do not
  return $integ_value unless (defined $y_begin && defined $y_end);

  # Get a sorted (from beginning of sim's x-values to end of sim's x-values)
  #   list of x-values
  my @x_vals = sort {$a <=> $b} keys %{$wave_ref};

  # Start calculating the integration
  $integ_value = 0;

  # Sum at the beginning of the range: from $x_begin to next x-value in array
  my $begin_index = binary_search \@x_vals, $x_begin;
  if( $x_begin < $x_vals[$begin_index] ) {
    $integ_value += ($x_vals[$begin_index] - $x_begin) *
                    (${$wave_ref}{$x_vals[$begin_index]} + $y_begin)/2;
  } elsif ( $x_begin > $x_vals[$begin_index] ) {
    $integ_value += ($x_vals[$begin_index+1] - $x_begin) *
                    (${$wave_ref}{$x_vals[$begin_index+1]} + $y_begin)/2;
    $begin_index++;
  }

  # Sum at the end of the range: from previous x-value in array to $x_end
  my $end_index = binary_search( \@x_vals, $x_end );
  if( $x_end > $x_vals[$end_index] ) {
    $integ_value += ($x_end - $x_vals[$end_index]) *
                    ($y_end + ${$wave_ref}{$x_vals[$end_index]})/2;
  } elsif ( $x_end < $x_vals[$end_index] ) {
    $integ_value += ($x_end - $x_vals[$end_index-1]) *
                    ($y_end + ${$wave_ref}{$x_vals[$end_index-1]})/2;
    $end_index--;
  }

  # Now sum between $begin_index and $end_index for every (x,y) value
  for( my $i = $begin_index; $i < $end_index; $i++ ) {
    $integ_value += ($x_vals[$i+1] - $x_vals[$i]) *
                    (${$wave_ref}{$x_vals[$i+1]}+${$wave_ref}{$x_vals[$i]})/2;
  }

  return $integ_value;
}


###############################################################################

=head2 max

    my $maximum = max \%waveform, $xrange_start, $xrange_end;

Finds the maximum y-value of a waveform.  This can find the maximum value of the entire waveform or a local maximum value within a specific range of x-values.  Use an empty string '' for $xrange_start to start at the first x-value of the waveform, or use an empty string '' for $xrange_end to end at the last x-value of the waveform.  Note that linear interpolation is used if one or both of these range x-values do not exist as an x-value in the waveform.

Assumes:

=over

=item *

$xrange_start and $xrange_end are valid positions in the waveform

=back

Inputs:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$xrange_start> - The start of a range of x-values to search.  Use an empty string '' to start at the first x-value of the original waveform.

=item *

C<$xrange_end> - The end of a range of x-values to search.  Use an empty string '' to end at the last x-value of the original waveform.

=back

Output:

=over

=item *

C<$maximum> - A scalar of the maximum y-value of the waveform (or range of waveform)

=back

Error Cases:

=over

=item *

The output will be undefined if an xrange value is beyond the edge of the waveform's x-values; the xrange values must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub max {
  my $wave_ref = shift;
  my $xrange_start = shift;
  my $xrange_end = shift;
  my $maximum;

  my %trunc_wave;
  truncate_waveform( $wave_ref, $xrange_start, $xrange_end, \%trunc_wave );

  if( scalar keys %trunc_wave > 0 ) {
    my @y_vals = sort {$b <=> $a} values %trunc_wave; # Sort values descending
    $maximum = $y_vals[0];
  }

  return $maximum;
}


###############################################################################

=head2 min

    my $minimum = min \%waveform, $xrange_start, $xrange_end;

Finds the minimum y-value of a waveform.  This can find the minimum value of the entire waveform or a local minimum value within a specific range of x-values.  Use an empty string '' for $xrange_start to start at the first x-value of the waveform, or use an empty string '' for $xrange_end to end at the last x-value of the waveform.  Note that linear interpolation is used if one or both of these range x-values do not exist as an x-value in the waveform.

Assumes:

=over

=item *

$xrange_start and $xrange_end are valid positions in the waveform

=back

Inputs:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$xrange_start> - The start of a range of x-values to search.  Use an empty string '' to start at the first x-value of the original waveform.

=item *

C<$xrange_end> - The end of a range of x-values to search.  Use an empty string '' to end at the last x-value of the original waveform.

=back

Output:

=over

=item *

C<$minimum> - A scalar of the minimum y-value of the waveform (or range of waveform)

=back

Error Cases:

=over

=item *

The output will be undefined if an xrange value is beyond the edge of the waveform's x-values; the xrange values must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub min {
  my $wave_ref = shift;
  my $xrange_start = shift;
  my $xrange_end = shift;
  my $minimum;

  my %trunc_wave;
  truncate_waveform( $wave_ref, $xrange_start, $xrange_end, \%trunc_wave );

  if( scalar keys %trunc_wave > 0 ) {
    my @y_vals = sort {$a <=> $b} values %trunc_wave; # Sort values ascending
    $minimum = $y_vals[0];
  }

  return $minimum;
}


###############################################################################

=head2 neg_waveform

    neg_waveform \%waveform_in, \%waveform_out;

Negates (multiply by -1) each y-value of a waveform.  Places the negated version of the waveform in a new hash.

Input:

=over

=item *

C<\%waveform_in> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=back

Output:

=over

=item *

C<\%waveform_out> - Identical to C<\%waveform_in>, except each of the hash's y-values are negated

=back

=cut

sub neg_waveform {
  my $wave_in = shift;
  my $wave_out = shift;

  foreach (keys %{$wave_in}) {
    ${$wave_out}{$_} = -1 * ${$wave_in}{$_};
  }

  return;
}


###############################################################################

=head2 put_signal_sti

    put_signal_sti $append_file_handle, $sig_name, $sig_type, \%waveform;

Exports waveform data from a hash into a piecewise linear (PWL), .sti-formatted output file

Assumes:

=over

=item *

$append_file_handle's file is open for appending

=back

Inputs:

=over

=item *

C<$append_file_handle> - File handle of output (append) PWL, .sti-formatted file

=item *

C<$sig_name> - Name of the signal to export

=item *

C<$sig_type> - Type of signal to export.  For a .sti file, this is typically either (V) voltage or (I) current.

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=back

=cut

sub put_signal_sti {
  my $fh = shift;
  my $sig_name = shift;
  my $sig_type = shift;
  my $wave_ref = shift;

  # For example: Vbit bit 0 PWL (
  print $fh "\n", $sig_type, $sig_name, ' ', $sig_name, " 0 PWL (\n";

  # Obtain all x-values (time values) for the signal
  my @x_vals = sort {$a <=> $b} keys %{$wave_ref};

  foreach (@x_vals) {
    # For example: + 5.000000e-11 1.000000e+00
    #                   time      voltage/current
    print $fh '+ ', $_, ' ', ${$wave_ref}{$_}, "\n";
  }

  # Mark the end of the signal with a parenthesis
  print $fh "+ )\n";

  return;
}


###############################################################################

=head2 sub_waveforms

    sub_waveforms \%waveform_a, \%waveform_b, \%waveform_diff;

Subtracts two waveforms to obtain a resultant difference waveform.  For each matching x-value "X", the y-values will be subtracted (A - B) to form "YDIFF" and this will form an xy-pair (X,YDIFF) in the resultant difference waveform.  If one waveform contains an x-value not existing in the other waveform, then linear interpolation is used (i.e. "yval" is called) to generate an xy-pair to use in the difference.  If waveforms of different lengths are subtracted, then the length of the resultant difference waveform will be shorter or equal to the length of the shorter waveform, since linear interpolation is used; the x-range will be from the larger (or later if the x-axis is time) of the two waveforms' starting x-values to the smaller (or earlier if the x-axis is time) of the two waveforms' ending x-values.

Inputs:

=over

=item *

C<\%waveform_a> - A hash reference where the hash keys are x-values and the hash values are y-values of the first waveform

=item *

C<\%waveform_b> - A hash reference where the hash keys are x-values and the hash values are y-values of the second waveform; this waveform will be subtracted from the first (e.g., A-B)

=back

Output:

=over

=item *

C<\%waveform_diff> - The resultant difference waveform of the two input waveforms

=back

=cut

sub sub_waveforms {
  my $wave_a = shift;
  my $wave_b = shift;
  my $wave_diff = shift;

  my %neg_wave_b;

  neg_waveform( $wave_b, \%neg_wave_b );

  add_waveforms( $wave_a, \%neg_wave_b, $wave_diff );

  return;
}


###############################################################################

=head2 truncate_waveform

    truncate_waveform \%orig_wave, $xrange_start, $xrange_end, \%trunc_wave;

Obtains a truncated window of a waveform.  Will create a hash with only the xy-pairs between $xrange_start and $xrange_end (the xy-pairs outside this range will not be included in the resultant hash).  Use an empty string '' for $xrange_start to start at the first x-value of the waveform, or use an empty string '' for $xrange_end to end at the last x-value of the waveform.  Note that linear interpolation will be used if one or both of these range x-values do not exist as an x-value in the original waveform; the interpolated xy-pair(s) will be added to the resultant truncated hash (this will be the first and/or last xy-pair in the truncated hash).

Assumes:

=over

=item *

$xrange_start and $xrange_end are valid positions in the waveform; note, if these are at the very beginning and end of the waveform, then a copy of the original waveform will be returned

=back

Inputs:

=over

=item *

C<\%orig_wave> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$xrange_start> - The starting x-value for the truncated waveform.  Use an empty string '' to start at the first x-value of the original waveform.

=item *

C<$xrange_end> - The ending x-value for the truncated waveform.  Use an empty string '' to end at the last x-value of the original waveform.

=back

Output:

=over

=item *

C<\%trunc_wave> - The truncated waveform with x-values now beginning at $xrange_start and ending at $xrange_end

=back

Error Cases:

=over

=item *

The output will be unchanged if an xrange value is beyond the edge of the waveform's x-values; the xrange values must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub truncate_waveform {
  my $wave_ref = shift;
  my $xrange_start = shift;
  my $xrange_end = shift;
  my $trunc_wave = shift;

  # The starting and ending y-values of the start and end of the x-range
  my $yval_start;
  my $yval_end;

  # Get a sorted list of x-values (from the beginning of waveform's x-values
  #   to end of waveform's x-values)
  my @x_vals = sort {$a <=> $b} keys %{$wave_ref};
  my $x_vals_idx_first = 0; # Assume starting at the beginning of the waveform,
                        #   unless the start of an x-range is specified
  if( $xrange_start ne '' ) { # The start of a range of x-values is specified
    # Find where the missing x-value would be located
    my $index = binary_search( \@x_vals, $xrange_start );

    if( $x_vals[$index] == $xrange_start ) {
      # $index is exactly at the start of the range; only adjust first x-value
      $x_vals_idx_first = $index;

    } elsif( $x_vals[$index] < $xrange_start ) {
      # $index is just smaller than the start of the range, so $index+1 is just
      #   larger than the start of the range
      $x_vals_idx_first = $index+1;

      $yval_start = yval( $wave_ref, $xrange_start );
      return unless defined $yval_start; # Return if yval could not interpolate

    } elsif( $xrange_start < $x_vals[$index] ) {
      # $index is just larger than the start of the range
      $x_vals_idx_first = $index;

      $yval_start = yval( $wave_ref, $xrange_start );
      return unless defined $yval_start; # Return if yval could not interpolate
    }
  }

  my $x_vals_idx_last = (scalar @x_vals)-1; # Assume ending at the end of the
                                        #   waveform, unless the end of an
                                        #   x-range is specified
  if( $xrange_end ne '' ) { # The end of a range of x-values is specified
    # Find where the missing x-value would be located
    my $index = binary_search( \@x_vals, $xrange_end );

    if( $x_vals[$index] == $xrange_end ) {
      # $index is exactly at the end of the range; only adjust last x-value
      $x_vals_idx_last = $index;

    } elsif( $x_vals[$index] < $xrange_end ) {
      # $index is just smaller than the end of the range
      $x_vals_idx_last = $index;

      $yval_end = yval( $wave_ref, $xrange_end );
      return unless defined $yval_end; # Return if yval could not interpolate

    } elsif( $xrange_end < $x_vals[$index] ) {
      # $index is just larger than the end of the range, so $index-1 is just
      #   smaller than the end of the range
      $x_vals_idx_last = $index-1;

      $yval_end = yval( $wave_ref, $xrange_end );
      return unless defined $yval_end; # Return if yval could not interpolate
    }
  }

  # Now assemble the truncated waveform:

  # Insert the interpolated value for the start of the range (if required)
  ${$trunc_wave}{$xrange_start} = $yval_start if defined $yval_start;

  # Copy values within the truncation window from the original waveform to the
  #   truncated waveform one-by-one (from x_vals_idx_first index through to
  #   x_vals_idx_last index)
  for( my $i = $x_vals_idx_first; $i < $x_vals_idx_last+1; $i++ ) {
    ${$trunc_wave}{$x_vals[$i]} = ${$wave_ref}{$x_vals[$i]};
  }

  # Insert the interpolated value for the end of the range (if required)
  ${$trunc_wave}{$xrange_end} = $yval_end if defined $yval_end;

  return;
}


###############################################################################

=head2 xval

    my @xvalues = xval \%waveform, $yvalue, $xrange_start, $xrange_end;

Obtains the x-value(s) (if applicable) of a waveform for a specific y-value.  Can find all applicable x-values of the waveform or can search within a particular range of x-values.  Use an empty string '' for $xrange_start to start at the first x-value of the waveform, or use an empty string '' for $xrange_end to end at the last x-value of the waveform.  Note that linear interpolation is used if one or both of these range x-values do not exist as an x-value in the waveform.

Assumes:

=over

=item *

$xrange_start and $xrange_end are valid positions in the waveform, and are at least separated by one timestep of the simulation (they are not infinitesimally close together)

=back

Inputs:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$yvalue> - Scalar of the y-value to use when finding possible x-value(s).  Linear interpolation is used if the y-value does not exactly exist in the waveform (if the value $yval does not exist in the hash).

=item *

C<$xrange_start> - The start of a range of x-values to search.  Use an empty string '' to start at the first x-value of the original waveform.

=item *

C<$xrange_end> - The end of a range of x-values to search.  Use an empty string '' to end at the last x-value of the original waveform.

=back

Output:

=over

=item *

C<@xvalues> - An array of the x-value(s) corresponding to the specified y-value of the waveform (linear interpolation may be used).  The array may be empty if the y-value does not exist in the waveform or in the particular range of x-values.

=back

Error Cases:

=over

=item *

The output will be undefined if an xrange value is beyond the edge of the waveform's x-values; the xrange values must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub xval {
  my $wave_ref = shift;
  my $yval = shift;
  my $xrange_start = shift;
  my $xrange_end = shift;

  my @output_x_values; # The x-values for the particular y-value

  my %trunc_wave;
  truncate_waveform( $wave_ref, $xrange_start, $xrange_end, \%trunc_wave );

  return @output_x_values unless scalar keys %trunc_wave > 0; # Trunc error abort

  # Get a sorted list of x-values (from the beginning of waveform's x-values
  #   to end of waveform's x-values)
  my @x_vals = sort {$a <=> $b} keys %trunc_wave;
  my $x_vals_idx_first = 0; # Starting at the beginning of the truncated waveform
  my $x_vals_idx_last = (scalar @x_vals)-1; # Ending at the end of trunc. wave.

  # Now search for the y-value or search for crossings and use interpolation
  #   to find the y-value
  for( my $i = $x_vals_idx_first; $i < $x_vals_idx_last; $i++ ) {
  
    if( $trunc_wave{$x_vals[$i]} == $yval ) {
      push @output_x_values, $x_vals[$i];

    } elsif( ( ( $trunc_wave{$x_vals[$i]}   < $yval ) &&
               ( $trunc_wave{$x_vals[$i+1]} > $yval ) )
          || ( ( $trunc_wave{$x_vals[$i]}   > $yval ) &&
               ( $trunc_wave{$x_vals[$i+1]} < $yval ) ) ) {
      # There is a crossing (either rising-edge or falling-edge) that occurs
      #   between these two timesteps
      
      # Using point-slope form for linear interpolation: x = (y-y0)/m + x0
      my $int_x; # Interpolated x-value
      my $y_diff; # Change in rise to use for slope
      my $x_diff; # Change in run to use for slope
      my $slope; # Slope to use for linear interpolation

      $y_diff = $trunc_wave{$x_vals[$i+1]} - $trunc_wave{$x_vals[$i]};
      $x_diff = $x_vals[$i+1] - $x_vals[$i];
      $slope = $y_diff / $x_diff;
      $int_x = (($yval-$trunc_wave{$x_vals[$i]})/$slope) + $x_vals[$i];

      push @output_x_values, $int_x;
    }
  }

  # Need to check final x-value (time point), the loop above skips this
  if( $trunc_wave{$x_vals[$x_vals_idx_last]} == $yval ) {
    push @output_x_values, $x_vals[$x_vals_idx_last];
  }

  return @output_x_values;
}


###############################################################################

=head2 yval

    my $yvalue = yval \%waveform, $xvalue;

Obtains the y-value of the waveform given a particular x-value

Assumes:

=over

=item *

An x-value will only correlate to one y-value (there are no repetitions of the x-values in the waveform)

=back

Inputs:

=over

=item *

C<\%waveform> - A hash reference where the hash keys are x-values and the hash values are y-values of the waveform

=item *

C<$xvalue> - Scalar of the x-value to use when finding the y-value.  Linear interpolation is used if the x-value does not exactly exist in the waveform (if the key $xval does not exist in the hash).

=back

Output:

=over

=item *

C<$yvalue> - A scalar of the y-value corresponding to the specified x-value of the waveform (linear interpolation may be used)

=back

Error Cases:

=over

=item *

The output will be undefined if the x-value is beyond the edge of the waveform's x-values; $xvalue must not be before the first x-value (e.g., 0 sec) or past the last x-value, or else linear interpolation cannot occur

=back

=cut

sub yval {
  my $wave_ref = shift;
  my $xval = shift;

  # Check if $xval exists--this is the easy case, no interpolation needed
  if( exists ${$wave_ref}{$xval} ) { return ${$wave_ref}{$xval}; }

  # Get a sorted (from beginning of sim's x-values to end of sim's x-values)
  #   list of x-values
  my @x_vals = sort {$a <=> $b} keys %{$wave_ref};
  my $x_vals_idx_first = 0;
  my $x_vals_idx_last = (scalar @x_vals)-1;

  # Find where the missing x-value would be located
  my $index = binary_search( \@x_vals, $xval );

  # Using point-slope form for linear interpolation: y = m(x-x0) + y0
  my $int_y; # Interpolated y-value
  my $y_diff; # Change in rise to use for slope
  my $x_diff; # Change in run to use for slope
  my $slope; # Slope to use for linear interpolation

  # In addition to checking if $index is smaller or larger than $xval, we must
  #   also check if $index is not at the edge of the waveform's x-values (it
  #   must not be earlier than the first x-value (e.g., 0 sec) and it must not
  #   be later than the last x-value or else we cannot do linear interpolation)
  # If $index is at the edge of the waveform's x-values, then just return an
  #   undefined y-value
  if( ($xval > $x_vals[$index]) && ($index < $x_vals_idx_last) ) {
    # $index is just smaller than $xval and $index+1 is just larger than $xval
    $y_diff = ${$wave_ref}{$x_vals[$index+1]} - ${$wave_ref}{$x_vals[$index]};
    $x_diff = $x_vals[$index+1] - $x_vals[$index];
    $slope = $y_diff / $x_diff;
    $int_y = ($slope*($xval-$x_vals[$index])) + ${$wave_ref}{$x_vals[$index]}; 
    # Could add this new x-value:y-value pair to the hash, but let's not
    #   This could affect the accuracy of a future integration or average
    # ${$wave_ref}{$xval} = $int_y;
  } elsif( ($xval < $x_vals[$index]) && ($index > $x_vals_idx_first) ) {
    # $index is just larger than $xval and $index-1 is just smaller than $xval
    $y_diff = ${$wave_ref}{$x_vals[$index]} - ${$wave_ref}{$x_vals[$index-1]};
    $x_diff = $x_vals[$index] - $x_vals[$index-1];
    $slope = $y_diff / $x_diff;
    $int_y = ($slope*($xval-$x_vals[$index])) + ${$wave_ref}{$x_vals[$index]}; 
    # Could add this new x-value:y-value pair to the hash, but let's not
    #   This could affect the accuracy of a future integration or average
    # ${$wave_ref}{$xval} = $int_y;
  }

  return $int_y;
}


=head1 TODO

The following is a list of known bugs to fix, potential revisions, or potential additions which are ordered by urgency (e.g., HIGH, MODERATE, or LOW):

=over

=item *

(LOW Revision) Standardize the parameters, and the order of parameters, of the subroutines

=back

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

1;

