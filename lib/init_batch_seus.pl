#!/usr/bin/perl -w
use strict;
use warnings;

use IPC::Run 'run';
use File::Path 'remove_tree';

my $VERSION = '1.1.4';
my $SPICE_EXEC = 'ngspice'; # Define default Spice executable here

# SEUS Package's init_batch_seus.pl:
#   Initializes a batch of (typically) Monte Carlo spice netlists in a directory
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'init_batch_seus.pl';

# License
my $License = "\n".
"SEUS $Script_Name Copyright (C) 2009-2020 Michael A. Turi\n".
"This program comes with ABSOLUTELY NO WARRANTY.  This is free software,\n".
"  and you are welcome to redistribute it under certain conditions.\n".
"Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')\n".
"  in the SEUS package for more details.\n\n";

# Requires script:
my $Netgen_Seus = 'netgen_seus.pl';
my $Netgen_Options = 'lz';

# Constants:
my $Fmt_Input = '.i';
my $Default_Std_Dev_Factor = 30; # std_dev = mean/30 (3*std_dev = mean/10)

my $Netlist_Offset = 1; # The title (1st comment line) is on line #0, line 1 begins code for the netlist

# Filenames for the summaries of sweeps and Monte Carlo variations
my $Sweep_Sum_Filename = 'sweep_summary.csv';
my $Sweep_Table_Spacing = 15; # The printf spacing for the sweep summary table (for display to stdout)
my $Vary_Sum_Filename = 'vary_summary.csv';

# Corner names: np (e.g., fs-->n=fast; p=slow)
my @Corner_Names = ( 'ff', 'fs', 'sf', 'ss' );
my $Corner_Count = scalar @Corner_Names;
# Mult. factors for n- and p-type FinFETs (1=+n std. dev; -1=-n std. dev)
my @Corner_N_Factors = ( 1, 1, -1, -1 );
my @Corner_P_Factors = ( 1, -1, 1, -1 );

# Usage:
my $Usage = ''.
'Usage: '.$Script_Name."  [OPTIONS]  spice_deck_name  batch_netlist_directory\n".
"           init_batch_def_file  [mc_values_file]  [#sims]\n".
"Options\n".
# TODO: "  -c N     Run N number of corners (+/- N standard deviations, e.g., N=3)\n".
"  -f       Use $Netgen_Seus to expand mosfet transistor fingers into separate mosfets\n".
"  -l       Suppress display of SEUS's AGPL-3.0-or-later license information\n".
"  -o path  Run other Spice executable at \"path\" (must support \'listing expand\')\n".
"  -u path  Use \"path\" for location of the SEUS Package (if SEUS not installed)\n";
my $Min_Arg_Num = 3;

# Procedure: expand_sweep_expression
#
#   Expands a sweep expression by returning (1st) a comma delimited string of
#   values represented by the sweep expression and returning (2nd) a count of
#   how many values are in the comma delimited string.
#
# Parameter: 1st param = sweep expression to expand
#            2nd param = descriptive name to be used in error messages if
#                        expansion of sweep expression fails
sub expand_sweep_expression {
  my $sw_expression = shift;
  my $sw_errorname = shift;

  my $sw_vals; # 1st return value, a comma delimited list of the sweep values represented by the expression
  my $sw_vals_cnt = 1; # 2nd return value, the number (count) of sweep values in the comma delimited list
  if( $sw_expression =~ m/^\s*=\s*(\S+\s*,.+)/ ) { # E.g., 5e-9, ...
    $sw_vals = $1; # Sweep values are already provided as a comma delimited list
    chomp $sw_vals;
    my @split_vals = split /[ ,]+/, $sw_vals;
    $sw_vals_cnt = scalar @split_vals;
  } elsif( $sw_expression =~ m/^\s*=\s*(\S+)\s*:\s*(\S+)\s*:\s*(\S+)/i ) { # E.g., 2e-9:0.5e-9:4e-9 (equals 2e-9, 2.5e-9, 3e-9. 3.5e-9, 4e-9)
    my $s_beg = $1;
    my $s_end = $3;
    my $s_inc = $2; # Note: Increment can be positive or negative (for loop compensates for this) [E.g., 4e-9:-0.5e-9:2e-9 (equals 4e-9, 3.5e-9, 3e-9. 2.5e-9, 2e-9)]
    $sw_vals = $s_beg;
    die 'Error: Increment value of 0 found for sweeping ', $sw_errorname if $s_inc == 0;
    die 'Error: Illegal start:increment:finish for sweeping ', $sw_errorname if (($s_beg < $s_end and $s_inc < 0) or ($s_beg > $s_end and $s_inc > 0));
    # Use start:increment:finish to make comma delimited list of values
    for( my $j = $s_beg+$s_inc; ($j <= $s_end and $s_inc > 0) or ($j >= $s_end and $s_inc < 0); $j+=$s_inc ) {
      $sw_vals = $sw_vals.','.$j;
      $sw_vals_cnt++;
    }
  } elsif( $sw_expression =~ m/^\s*=\s*(\S+)\s*;\s*(\S+)\s*%\s*:\s*(\S+)\s*%\s*:\s*(\S+)\s*%/ ) { # E.g., 0.2; -10%:5%:10% (equals 0.18, 0.19, 0.2, 0.21, 0.22)
    my $s_ave = $1;
    my $s_beg = $2/100;
    my $s_end = $4/100;
    my $s_inc = $3/100; # Note: Increment can be positive or negative (for loop compensates for this) [E.g., 0.2; 10%:-5%:-10% (equals 0.22, 0.21, 0.2, 0.19, 0.18)]
    $sw_vals = $s_ave + $s_ave * $s_beg;
    die 'Error: Increment value of 0 found for sweeping ', $sw_errorname if $s_inc == 0;
    die 'Error: Illegal start:increment:finish for sweeping ', $sw_errorname if (($s_beg < $s_end and $s_inc < 0) or ($s_beg > $s_end and $s_inc > 0));
    # Use start:increment:finish to make comma delimited list of values
    for( my $j = $s_beg+$s_inc; ($j <= $s_end and $s_inc > 0) or ($j >= $s_end and $s_inc < 0); $j+=$s_inc ) {
      $sw_vals = $sw_vals.','.($s_ave + $s_ave * $j);
      $sw_vals_cnt++;
    }
  } else {
    die 'Error: Could not understand sweeping "', $sw_expression, '" for ', $sw_errorname;
  }
  die 'Error: Zero sweep values found in "', $sw_expression, '" for ', $sw_errorname unless $sw_vals_cnt > 0;
  return ($sw_vals, $sw_vals_cnt);
}

# init_batch_seus.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. and Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is by far the most complex of this package of scripts.  This is a Perl
# script used to initialize a directory of spice netlists with varied (e.g.,
# for Monte Carlo simulations) or swept transistor parameters, supply
# voltages, or options (such as temperature).  Afterwards, batchexec_seus.pl
# can be used to run all files and a measurement script can be used to
# summarize results of interest.  By default, this script assumes the use of
# ngspice or an ngspice-based simulator.  This script requires the
# "listing expand" operation provided by ngspice.
# 
# Usage: Provide the Spice deck name, the directory to put the batch of netlists
#        (if a pre-existing directory name is used, then it is first deleted so
#        this new batch directory can take its place) and the filename of the
#        "init_batch_def_file" (the syntax of this is described in the README
#        file).  If just sweeping parameters, this script will output a summary
#        table of the swept parameters/variables and the batch_netlist_directory
#        will be initialized with the swept spice netlists.  If varying parameters,
#        this script will just output a summary table of varied parameters/variables.
#        
#        Provide the Spice deck name, the directory to put the batch netlists,
#        the filename of the "init_batch_def_file", the directory to put the
#        batch of netlists (if a pre-existing directory name is used, then it
#        is first deleted so this new batch directory can take its place), the 
#        filename of the "mc_values_file", and the number of simulations (#sims)
#        in order to initialize this directory of spice netlists with varied
#        parameters.  The "mc_values_file" is expected to be a space-delimited
#        list of Monte Carlo values to assign to each dimension; the number of
#        lines in this file should be equal to the number of simulations you wish
#        to run (#sims).  You can use some type of random number generator to
#        create an mc_values_file.  Personally, I use the Carnegie Mellon University
#        quasi-Monte Carlo (CMU QMC) program "gennorm" for this [1].  Contact the
#        authors for the CMU QMC application if interested (see citation below).
#        
#        [1] Amith Singhee and Rob A. Rutenbar, "From finance to flip-flops:
#            A study of fast quasi-Monte Carlo methods from computational finance
#            applied to statistical circuit analysis", International Symposium on
#            Quality Electronic Design, 2007.
# 
# Note: Currently this script supports varying (or sweeping) netlist parameters
#       (or variables) and model parameters for MOSFET transistors (in Spice,
#       transistors with names beginning with "m").
#       
#       In addition, the hashes used to store model and parameter information in
#       this script make a few assumptions:
#         (1) The colon (:) is not a valid character for a model or parameter
#             name--this is used as a delimiter character within a few hashes
#         (2) No model or FET is named "variable"--this is used in a few hashes to
#             identify parameters not associated with a model (such as simulation
#             temperature)
#         (3) A model does not have the same name as a FET--if a transistor's model
#             parameters (not an inline parameter such as length or width) are
#             varied, then the model is copied and named after that FET in the
#             generated netlists (the parameters of the new model are varied as
#             defined by the user).  If a model already has the same name as a FET,
#             then this script may malfunction and/or two models with the same name
#             will be present on the generated Spice netlists (and cause an error
#             during Spice execution).

print "** $Script_Name ** SEUS Package Version: $VERSION **\n";
die $Usage if scalar @ARGV < 1;
my $argnum = scalar @ARGV;

my $spice_path = $SPICE_EXEC; # Use default Spice executable
                              #   Unless option "-o path" specifies differently

my $i = 0;
my $arg = shift;
my $op_corners; # TODO: Support option for corner simulations -c N
my $op_skip_lic; # -l
my $seus_path; # -u path
while( $arg =~ m/^-/ ) {
  if( $arg =~ m/^-(\S+)/ ) { # Found options
    my $argmatch = $1;

    # Check single letter options first
    if( $argmatch =~ m/f/i ) { # netgen_seus.pl will expand transistor fingers
      $Netgen_Options .= 'f';
    }
    if( $argmatch =~ m/l/i ) { # Suppress printing license information
      $op_skip_lic = 1;
    }

    # Check options which require another argument
    # TODO: Support corner simulations (maybe via a init_batch profile?)
    #if( $argmatch =~ m/c/i ) {
    #  $op_corners = shift; # Set to number of standard deviations to use
    #  die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    #}
    if( $argmatch =~ m/o/i ) { # Use other specified Spice executable
      $spice_path = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
    if( $argmatch =~ m/u/i ) { # Use a path to a local location of the SEUS Package
      $seus_path = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
  }
  die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
  $arg = shift;
}
print $License unless defined $op_skip_lic;
die "Error: Not enough parameters\n\n$Usage" if $argnum < $Min_Arg_Num;

my $deckname = $arg;
my $batch_dir = shift;
if( $batch_dir !~ m/\/$/ ) { $batch_dir .= '/'; } # Add trailing slash if it doesn't exist
my $init_defs_file = shift;
my $mc_vals_file = shift;
my $sim_num = defined $op_corners ? $Corner_Count : shift;

if( defined $seus_path ) { # Use SEUS path if provided
  if( $seus_path !~ m/\/$/ ) { $seus_path .= '/'; } # Add trailing slash if it doesn't exist
} else { # else just call script names and hope the SEUS package is installed
  $seus_path = '';
}

# Need path to the spice executable
# This fails if it not valid and was not passed via command line with "-o" option
die 'Error: The path to the spice executable is not defined' unless defined $spice_path;

print 'Note: The default std_dev = mean/', $Default_Std_Dev_Factor, ' (3*std_dev = mean/', $Default_Std_Dev_Factor/3, ")\n\n";

##########
my @sweep_titles; # Holds the names/titles (defined by the user in the init_batch_def_file) for the directories to be created for a swept netlist

my %sweep_level_to_names; # Holds the model:param (or variable:param) combination(s) for a given sweep level number (or "default")
my %sweep_name_to_level; # Holds the sweep level number (or "default") for a given model:param (or variable:param) combination
my %sweep_level_steps; # Holds the number of steps for a given sweep level number (or "default")

my %sweep_params; # Holds the names of swept transistor parameters (as a space delimited string) for a given model name
my %sw_param_vals; # Holds the sweep values (comma delimited) for a given model:param combination

my @sweep_vars; # Holds the names of swept .param variables
my %sw_var_vals; # Holds the sweep values (comma delimited) for a given .param variable name

my %sweep_name_to_val_list; # Holds the sweep values (comma delimited) for a given model:param (or variable:param) combination
                            # This is a combination of the %sw_param_vals and %sw_var_vals hashes

my @vary_vars; # Holds the names of varied .param variables
my %vy_var_means; # Holds the mean for a given .param variable name
my %vy_var_stddevs; # Holds the standard deviation for a given .param variable name

my %vary_params; # Holds the names of varied transistor parameters (as a space delimited string) for a given model name
my %vy_param_means; # Holds the mean for a given model:param combination
my %vy_param_stddevs; # Holds the standard deviation for a given model:param combination
##########

# Open the init batch definitions file and parse for .param and model variables to vary and their means and standard deviations
open DEFS, "<$init_defs_file" or die 'Error: Cannot open ', $init_defs_file, ' due to: ', $!;
$i = 0;
while( <DEFS> ) {
  $i++;
  if( m/^\s*VARY\s+/i ) { # Found start of a VARY block
    VARY_LOOP: while( <DEFS> ) {
      $i++;
      if( m/^\s*END\s+VARY\s+/i ) { last; } # Found end of a VARY block
      if( m/^\s*(\w+)\.(\w+)(.*)/i ) { # Found a parameter or model
        my $type = lc $1; # param or model
        my $name = lc $2; # param_name or model_name
        my $rest = $3;

        if( $type eq 'param' or $type eq 'params' ) { # Place in ".param variable" vary variables
          push @vary_vars, $name; # Add to list of .param variables to vary
          if( $rest =~ m/^\s*=\s*(\S+)\s*;\s*stddev\s*=\s*(\S+)/i ) {
            $vy_var_means{$name} = $1; # Use provided mean
            $vy_var_stddevs{$name} = $2; # Use provided standard deviation
          } elsif( $rest =~ m/^\s*=\s*(\S+)/ ) {
            $vy_var_means{$name} = $1; # Use provided mean
            $vy_var_stddevs{$name} = 'default'; # Use defualt standard deviation
          } else {
            $vy_var_means{$name} = 'file'; # Use value from file as mean
            $vy_var_stddevs{$name} = 'file'; # Use value from file as standard deviation
          }
        } elsif( $type eq 'model' or $type eq 'models' ) { # Place in "param" vary variables for variations in model parameters
          while( <DEFS> ) {
            $i++;
            if( m/^\s*END\s+VARY\s+/i ) { last VARY_LOOP; } # Found end of a VARY block
            if( m/^\s*\w+\.\w+/ ) {  # Found new model, option, or voltage
              # "Put" this back in <DEFS> stream, so it can be properly refound
              seek DEFS, -1*length, 1;
              last;
            }
            if( m/^\s*\+?\s*(\w+)(.*)/ ) {
              my $param = lc $1;
              my $rest = $2;
              # Add to list of this model's params to vary
              $vary_params{$name} = exists $vary_params{$name} ? $vary_params{$name}.' '.$param : $param;
              if( $rest =~ m/^\s*=\s*(\S+)\s*;\s*stddev\s*=\s*(\S+)/i ) {
                $vy_param_means{$name.':'.$param} = $1; # Use provided mean
                $vy_param_stddevs{$name.':'.$param} = $2; # Use provided standard deviation
              } elsif( $rest =~ m/^\s*=\s*(\S+)/ ) {
                $vy_param_means{$name.':'.$param} = $1; # Use provided mean
                $vy_param_stddevs{$name.':'.$param} = 'default'; # Use default standard deviation
              } else {
                $vy_param_means{$name.':'.$param} = 'file'; # Use value from file as mean
                $vy_param_stddevs{$name.':'.$param} = 'file'; # Use value from file as standard deviation
              }
            }
          }
        } else {
          print 'Warning: Type "', $type, '" from ', $init_defs_file, " was not understood and was ignored\n\n";
        }
      }
    }
  } elsif( m/^\s*SWEEP\s+/i ) { # Found start of a SWEEP block
    if( m/^\s*SWEEP\s+(NAME|TITLE)S?\s+(.*)/i ) { # Actually just found the titles for the swept simulation
      push @sweep_titles, split /[ ,]+/, $2;
      next;
    }
    my $level = 'default';
    $level = $1 if m/^\s*SWEEP\s+LEVEL\s*=\s*(\d+)/; # Grab the sweep level if it is defined, else just use "default"
    my $lvl_steps;
    SWEEP_LOOP: while( <DEFS> ) {
      $i++;
      if( m/^\s*END\s+SWEEP\s+/i ) { last; } # Found end of a SWEEP block
      if( m/^\s*(\w+)\.(\w+)(.*)/i ) { # Found a parameter or model
        my $type = lc $1; # param or model
        my $name = lc $2; # param_name or model_name
        my $rest = $3;

        if( $type eq 'param' or $type eq 'params' ) { # Place in ".param variable" sweep variables
          # Note: In the future, it would be nice to both sweep and vary a parameter
          #  (e.g., vbias is swept to 0.4, 0.2, and 0V and then Monte Carlo simulations vary
          #  each vbias level: 0.4V +/- x%, and 0.2V +/- x%, and 0V +/- x% groups of sims)
          die 'Error: Cannot both sweep and vary .param variable: ', $name if exists $vy_var_means{$name};

          push @sweep_vars, $name; # Add to list of .param variables to sweep
          ($sw_var_vals{$name}, $lvl_steps) = expand_sweep_expression $rest, '.param variable: '.$name; # Get the sweep values (expand from def. file)
          $sweep_name_to_val_list{'variable'.':'.$name} = $sw_var_vals{$name};

          die 'Error: Number of sweep steps do not agree (', $sweep_level_steps{$level}, ' and ', $lvl_steps, ') for sweep level ', $level
            if (exists $sweep_level_steps{$level} and $lvl_steps != $sweep_level_steps{$level});
          $sweep_level_steps{$level} = $lvl_steps; # Set the number of steps for this sweep level
          # Add this .param variable to the list of variables/parameters swept at this sweep level
          $sweep_level_to_names{$level} = exists $sweep_level_to_names{$level} ? $sweep_level_to_names{$level}.' '.'variable'.':'.$name : 'variable'.':'.$name;
          $sweep_name_to_level{'variable:'.$name} = $level;

        } elsif( $type eq 'model' or $type eq 'models' ) { # Place in "param" sweep variables for sweeping model parameters
          while( <DEFS> ) {
            $i++;
            if( m/^\s*END\s+SWEEP\s+/i ) { last SWEEP_LOOP; } # Found end of a SWEEP block
            if( m/^\s*\w+\.\w+/i ) {  # Found new model, option, or voltage
              # "Put" this back in <DEFS> stream, so it can be properly refound
              seek DEFS, -1*length, 1;
              last;
            }
            if( m/^\s*\+?\s*(\w+)(.*)/i ) {
              my $param = lc $1;
              my $rest = $2;

              # Note: In the future, it would be nice to both sweep and vary a parameter
              #  (e.g., vbias is swept to 0.4, 0.2, and 0V and then Monte Carlo simulations vary
              #  each vbias level: 0.4V +/- x%, and 0.2V +/- x%, and 0V +/- x% groups of sims)
              die 'Error: Cannot both sweep and vary parameter: ', $name, ':', $param if exists $vy_param_means{$name.':'.$param};

              $sweep_params{$name} = exists $sweep_params{$name} ? $sweep_params{$name}.' '.$param : $param; # Add to list of this model's params to sweep
              ($sw_param_vals{$name.':'.$param}, $lvl_steps) = expand_sweep_expression $rest, 'model '.$name.'\'s parameter '.$param; # Get the sweep values (expand from def. file)
              $sweep_name_to_val_list{$name.':'.$param} = $sw_param_vals{$name.':'.$param};

              die 'Error: Number of sweep steps do not agree (', $sweep_level_steps{$level}, ' and ', $lvl_steps, ') for sweep level ', $level
                if (exists $sweep_level_steps{$level} and $lvl_steps != $sweep_level_steps{$level});
              $sweep_level_steps{$level} = $lvl_steps; # Set the number of steps for this sweep level
              # Add this model:param to the list of variables/parameters swept at this sweep level
              $sweep_level_to_names{$level} = exists $sweep_level_to_names{$level} ? $sweep_level_to_names{$level}.' '.$name.':'.$param : $name.':'.$param;
              $sweep_name_to_level{$name.':'.$param} = $level;
            }
          }
        } else {
          print 'Warning: Type "', $type, '" from ', $init_defs_file, " was not understood and was ignored\n\n";
        }
      }
    }
  }
}
close DEFS;

die 'Error: Sweep block missing level (if using levels of hierarchy, all sweep blocks must have a level value)'
  if (exists $sweep_level_to_names{'default'} and scalar (keys %sweep_level_to_names) > 1);

# Create the directory for the varied simulations to go (remove prior existing directory if neccessary)
my $deckfile = $deckname.$Fmt_Input;
die 'Error: "', $deckfile, '" does not exist' unless -e $deckfile;
remove_tree( $batch_dir, {error => \my $rmerr} );
die 'Error: Removal of "', $batch_dir, '" somehow failed' if $rmerr && @$rmerr;
mkdir $batch_dir or die 'Error: Cannot "mkdir ', $batch_dir, '" due to: ', $!;

# Call netgen_seus.pl to remove comments and insert include files
#  If .param variables will be varied, then netgen_seus.pl will retrieve
#    parameter details and substitute parameters with placeholders
#  If .param variables will not be varied, then netgen_seus.pl will
#    substitute the proper values for the parameters
my $deckfile0 = $batch_dir.'0'.$Fmt_Input;
# @spice will hold the netlist (modified by netgen_seus.pl)
my @spice = (scalar @vary_vars > 0 or scalar @sweep_vars > 0) ? `$seus_path$Netgen_Seus -d$Netgen_Options -i $deckfile -o $deckfile0` : `$seus_path$Netgen_Seus -$Netgen_Options -i $deckfile -o $deckfile0`;
die 'Error: "', $Netgen_Seus, '" script failed, aborting initialization' unless $? == 0;
print "\n";

##########
my %spice_params; # Holds the value (from the netlist) for a given .param variable name
my @spice_params_ordered; # An array of the .param variable names (in the order declared in the
                          #  netlist); use instead of "keys %spice_params" to maintain order
                          # Must maintain order because there can be .param variable dependencies
                          #  that must be resolved in proper order (e.g., .param a = {10*b})
my %spice_placeholders; # Holds the expression (of one or more .param variables) for a given placeholder (generated by netgen_seus.pl)
##########

if( scalar @vary_vars > 0 or scalar @sweep_vars > 0 ) { # If .param variables are swept or varied, must deal with the placeholders that netgen_seus.pl inserted

  my $detail = shift @spice; # Using shift since .param variable and placeholder information is not part of the netlist and should be removed
  chomp $detail;
  while( $detail =~ m/^([a-z]\w*)=(.+)$/i ) { # First set of lines from netgen_seus.pl contain: param1=value1
    $spice_params{lc $1} = $2;
    push @spice_params_ordered, lc $1; # Place this .param variable name in the order declared in the netlist
    $detail = shift @spice;
    chomp $detail;
  }
  while( $detail =~ m/^(\d\S*)=(.+)$/i ) { # Second set of lines from netgen_seus.pl contain: placeholder1=param_exp1 (e.g., 1000000T=100p*simlen or 1000001T=h)
    $spice_placeholders{$1} = $2;
    $detail = shift @spice;
    chomp $detail;
  }
  while( (substr $detail, 0, 1) !~ m/^\*/ ) { # Check to see if the title of the spice netlist is next (after the placeholder info from netgen_seus.pl)
    print 'Internal Warning: "', $detail, "\" was expected to be title of spice netlist; It will be ignored\n";
    $detail = shift @spice;
    chomp $detail;
  }
  unshift @spice, $detail."\n"; # Put back the title of the spice netlist so that @spice contains the entire netlist

  my @rm_vars;
  foreach my $pvar (@vary_vars, @sweep_vars) {
    my $usage = 0; # Flag to check if each .param variable swept or varied is actually used in the netlist
    foreach (grep /^\{.*\}$/, values %spice_params) { # Checking if .param variable is used in the definition of another .param variable (e.g., .param a = {10*b})
      if( m/^\{$pvar\}$/i or m/\W$pvar\W/i ) {
        $usage = 1;
        last;
      }
    }
    if( $usage < 1 ) {
      foreach (values %spice_placeholders) { # Checking if .param variable is used in the netlist by looking through the placeholders
        if( m/^$pvar$/i or m/\W$pvar\W/i ) {
          $usage = 1;
          last;
        }
      }
    }
    if( $usage < 1 ) { # .param variable is not used in the netlist
      print 'Warning: .param "', $pvar, "\" in init batch def. file was not found in netlist, so it will not be swept or varied\n";
      push @rm_vars, $pvar; # Add this .param variable name to the list for deletion

    } else { # Check if .param variable was declared in the netlist (print a warning if was not, but can continue with the sweep or vary)
      my $declared = 0;
      foreach (keys %spice_params) {
        if( $_ eq $pvar ) {
          $declared = 1;
          last;
        }
      }
      if( $declared < 1 ) {
        print 'Warning: .param "', $pvar, "\" in init batch def. file not declared in netlist, but will still be swept or varied\n";
        $spice_params{$pvar} = 0;
      }
    }
  }

  foreach (@rm_vars) { # Some .param variable names are to be removed (if scalar @rm_vars > 0) since they are unused in the netlist
    for( $i = 0; $i < scalar @vary_vars; $i++ ) {
      if( $_ eq $vary_vars[$i] ) {
        splice @vary_vars, $i, 1;
        last;
      }
    }
    for( $i = 0; $i < scalar @sweep_vars; $i++ ) {
      if( $_ eq $sweep_vars[$i] ) {
        splice @sweep_vars, $i, 1;
        last;
      }
    }
  }
}


# Grab the option, print, and save lines from the original netlist since
#   ngspice's "listing expand" omits these in its output
# If a line is not present (e.g., no .SAVE line), then grep returns an empty string ''
my @grep_lines = grep /^\.option/i, @spice; # Format .OPTION ABSTOL=1e-6 ...
my $option_line = scalar @grep_lines < 1 ? "" : $grep_lines[0];
chomp $option_line;
@grep_lines = grep /^\.print/i, @spice; # Format .PRINT TRAN v(foo) ...
my $print_line = scalar @grep_lines < 1 ? "" : $grep_lines[0];
chomp $print_line;
@grep_lines = grep /^\.save/i, @spice; # Format .SAVE v(foo) ...
my $save_line = scalar @grep_lines < 1 ? "" : $grep_lines[0];
chomp $save_line;
print "\nOption line: ", $option_line, "\n";
print 'Print line:  ', $print_line, "\n";
print 'Save line:   ', $save_line, "\n";

# Use ngspice to expand the netlist (expand subcircuits prior to duplicating models
#   for varied transistors within subcircuit)
# Save the resultant netlist as 0.i in the simulation directory (will be overwritten later)
my @spice_cmd = ( $spice_path, '-i', $deckfile0 );
my $spice_in = "listing expand > $deckfile0\nquit";
my $spice_out;
my $spice_err;
run( \@spice_cmd, \$spice_in, \$spice_out, \$spice_err );
die 'Error: Listing expansion through ', $spice_path, ' failed due to: ', $spice_err unless $spice_err eq '';

# Open the resultant netlist
open IN, "<$deckfile0" or die 'Error: Cannot open ', $deckfile0, ' due to: ', $!;
@spice = <IN>;
close IN;

# Remove spacing before * for first line's comment
$spice[0] =~ s/^\s*\*/\*/;
# Remove silly line numbers from "listing expand" operation (e.g., "   17 : minvp...")
foreach (@spice) {
  s/^\s*\d+\s*:\s*//;
}

##########
my %model_linenum; # Holds the line number of the declaration for a given model
my %model_fettype; # Holds the fet type (nmos or pmos) for a given model
my %model_inline_param; # Holds if the model:param combination declared inline (with each instance of a transistor, e.g., l & w) for a given model:param
##########

# Look through the netlist for the declarations of models with varied parameters
for( $i = 0; $spice[$i] !~ m/^.end/i and $i < scalar @spice; $i++ ) {
  my $model;
  my $param;
  if( $spice[$i] =~ m/^\.MODEL\s+([\w.]+)\s+([NP]MOS)/i ) { # Format .MODEL NSG NMOS LEVEL=17
    $model = lc $1;
    $model_fettype{$model} = lc $2;
    $model_linenum{$model} = $i;
    if( exists $vary_params{$model} ) { # There are varied parameters for this model
      foreach (split ' ', $vary_params{$model}) {
        if( $spice[$i] !~ m/\s+$_\s*=\s*\S+\s*/i ) { # Is each parameter defined in the model declaration?
          # This model:param combination must be declared inline
          $model_inline_param{$model.':'.$_} = 1; # Or could maybe use: $model_inline_param{$model} = (exists $model_inline_param{$model}) ? $model_inline_param{$model}.' '.$_ : $_;
        }
      }
    }
    if( exists $sweep_params{$model} ) { # There are swept parameters for this model
      foreach (split ' ', $sweep_params{$model}) {
        if( $spice[$i] !~ m/\s+$_\s*=\s*\S+\s*/i ) { # Is each parameter defined in the model declaration?
          # This model:param combination must be declared inline
          $model_inline_param{$model.':'.$_} = 1; # Or could maybe use: $model_inline_param{$model} = (exists $model_inline_param{$model}) ? $model_inline_param{$model}.' '.$_ : $_;
        }
      }
    }
  }
}
# Overwrite the ".END" in the netlist (will be appending to file)
$spice[$i] = "\n";
# Append the .OPTION line
push @spice, $option_line."\n" unless length $option_line == 0;
# Append the .PRINT line
push @spice, $print_line."\n" unless length $print_line == 0;
# Append the .SAVE line
push @spice, $save_line."\n" unless length $save_line == 0;
push @spice, "\n";

##########
my %data_column; # Holds the column number for a given model:param (or variable:param) combination
my $col = 0; # Column count for the number of data columns required for this netlist
##########

print "\nSummary of Spice Deck:\n";

print "For .param variables\n";
print "  Sweep:\n";
print "    No .param variables to sweep\n" if scalar @sweep_vars < 1;
foreach (sort @sweep_vars) { # If .param variables are swept
  my $sw_lvl = $sweep_name_to_level{'variable'.':'.$_};
  print '    ', $_, ': ', $sw_var_vals{$_},
        ";\n      (level = ", $sw_lvl, '; # steps = ', $sweep_level_steps{$sw_lvl}, ")\n";
  # Print the .param variable name, its values (comma delimited), the sweep level, and how many sweep level steps
}
print "  Vary:\n";
print "    No .param variables to vary\n" if scalar @vary_vars < 1;
foreach (sort @vary_vars) { # If .param variables are varied
  print "    Warning: There is no support to vary .param variables for corner simulations\n",
        "             (.param values will remain unchanged from the netlist)\n" if defined $op_corners;
  last if defined $op_corners;

  # Count this variable in the number of required data columns
  $data_column{'variable:'.$_} = $col++; # Use "variable" as the fetname (it's ok, fetnames begin with m)

  # Print variable summary (its name, mean, and standard deviation)
  print '    ', $_, ': mean=', $vy_var_means{$_}, '; std_dev=', $vy_var_stddevs{$_}, "\n";
}

# Print model parameter summary
foreach (sort keys %model_linenum) {
  my $model = $_;
  print 'For ', $model_fettype{$model}, ' model "', $model, "\"\n";
  print "  Sweep:\n";
  if( exists $sweep_params{$model} ) {
    foreach (split ' ', $sweep_params{$model}) {
      my $mpkey = $model.':'.$_;
      my $sw_lvl = $sweep_name_to_level{$mpkey};
      print '    ', $_, ': ', $sw_param_vals{$mpkey}, ';   inst. ', exists $model_inline_param{$mpkey} ? 'inline' : 'in model',
            "\n      (level = ", $sw_lvl, '; # steps = ', $sweep_level_steps{$sw_lvl}, ")\n";
      # Print the parameter name, its values (comma delimited), where it was instantiated, the sweep level, and how many sweep level steps
    }
  } else {
    print "    No parameters to sweep\n";
  }
  print "  Vary:\n";
  if( exists $vary_params{$model} ) {
    foreach (split ' ', $vary_params{$model}) {
      my $mpkey = $model.':'.$_;
      print '    ', $_, ': mean=', $vy_param_means{$mpkey}, '; std_dev=', $vy_param_stddevs{$mpkey},
            ';   inst. ', exists $model_inline_param{$mpkey} ? 'inline' : 'in model', "\n";
      # Print the parameter name, its mean, its standard deviation, and where it was instantiated
    }
  } else {
    print "    No parameters to vary\n";
  }
}

##########
my %sweep_vals; # A space delimited list of all values to be used for a given model:param name
                #   or variable:param name (for .param variables)
                # Note, this will include duplicate values for a hierarchical sweep (all
                #   values for all sweep combinations are included)
my $sw_num_combos = 1; # The number of sweep combinations (also equal to the number of
                       #   directories required for sweep).  This is equal to the number of
                       #   sweep steps for a single sweep level, but it is the product of
                       #   all sweep steps for a hierarchical sweep (multiple sweep levels)
##########

# This is a bit complicated to find all of the sweep levels.  Here is an example and how the code modifies it:
#   Sweep level = 1: "vdd_val" has values (0.9, 1.0, 1.1)
#   Sweep level = 2: "l" has values (17e-9, 30e-9) and "h" has values (30e-9, 75e-9)

if( scalar (keys %sweep_level_steps) > 0 ) { # If sweeping values (at least one sweep level--even "default"), must figure out sweep combinations
  my @sw_levels = keys %sweep_level_steps;
  @sw_levels = sort {$b <=> $a} @sw_levels unless exists $sweep_level_steps{'default'}; # Sort numerically descending

  foreach my $name (split ' ', $sweep_level_to_names{$sw_levels[0]}) { # Get variable:param and model:param names for the lowest level sweep
    $sweep_vals{$name} = join ' ', (split /[ ,]+/, $sweep_name_to_val_list{$name}); # Get the values for each param name and space delimit them
  }
  my $num_copies = $sweep_level_steps{$sw_levels[0]}; # The number of copies higher sweep levels will need of each value

  # Example: %sweep_vals now has:
  #   $sweep_vals{"vdd_val"} = undefined (no key "vdd_val" yet)
  #   $sweep_vals{"l"} = "17e-9 30e-9"
  #   $sweep_vals{"h"} = "30e-9 75e-9"

  for( $i = 1; $i < scalar @sw_levels; $i++ ) { # Go to the next lowest level sweep (go one sweep level up each iteration)
    foreach my $name (split ' ', $sweep_level_to_names{$sw_levels[$i]}) { # Get the variable:param and model:param names for this sweep level
      $sweep_vals{$name} = '';
      foreach (split /[ ,]+/, $sweep_name_to_val_list{$name}) { # Get the values for each param name
        $sweep_vals{$name} .= ($_.' ') x $num_copies; # Must make multiple copies of each value in order to correctly pair with lower sweep level values
      }
    }

    # Example: %sweep_vals now has:
    #   $sweep_vals{"vdd_val"} = "0.9 0.9 1.0 1.0 1.1 1.1"
    #   $sweep_vals{"l"} = "17e-9 30e-9"
    #   $sweep_vals{"h"} = "30e-9 75e-9"

    for( my $j = 0; $j < $i; $j++ ) { # Go to the lower sweep levels again
      foreach my $name (split ' ', $sweep_level_to_names{$sw_levels[$j]}) { # Get the variable:param and model:param names for this lower sweep level
        # Must make multiple copies of each set of values in order to correctly pair with the lower sweep level values
        $sweep_vals{$name} = ($sweep_vals{$name}.' ') x $sweep_level_steps{$sw_levels[$i]}; 
      }
    }

    # Example: %sweep_vals now has:
    #   $sweep_vals{"vdd_val"} = " 0.9   0.9   1.0   1.0   1.1   1.1"
    #   $sweep_vals{"l"} =       "17e-9 30e-9 17e-9 30e-9 17e-9 30e-9"
    #   $sweep_vals{"h"} =       "30e-9 75e-9 30e-9 75e-9 30e-9 75e-9"
    #
    # Repeat for higher orders of sweep hierarchy

    $num_copies *= $sweep_level_steps{$sw_levels[$i]}; # Update the number of copies required for the next higher sweep level
  }

  foreach (values %sweep_level_steps) { # Obtain number of sweep combinations
    $sw_num_combos *= $_;
  }

  # Print the sweep summary to file (within the batch directory) and as a table to stdout
  open SWEEP_SUM, ">$batch_dir$Sweep_Sum_Filename"
    or die "Error: Cannot create $batch_dir$Sweep_Sum_Filename for output due to ", $!;

  print "\nSummary of Sweep:\n";
  foreach (('Name', sort keys %sweep_vals)) { # Print the model:param and variable:param names
    printf '%*s  ', $Sweep_Table_Spacing, $_;
    print SWEEP_SUM $_, ',';
  }
  print "\n";
  print SWEEP_SUM "\n";
  my $dash = (('-' x $Sweep_Table_Spacing).'  ');
  print $dash x (1 + scalar keys %sweep_vals); # Make some dashes for clarity
  print "\n";
  for( $i = 0; $i < $sw_num_combos; $i++ ) {
    if( scalar @sweep_titles > $i ) { # Print the sweep title (if there is one) in the first column
      printf '%*s  ', $Sweep_Table_Spacing, $sweep_titles[$i];
      print SWEEP_SUM $sweep_titles[$i], ',';
      $sweep_titles[$i] .= '/' unless $sweep_titles[$i] =~ m/\/$/;
    } else { # Use a number if there are not enough sweep titles for the sweep combinations
      printf '%*d  ', $Sweep_Table_Spacing, $i+1;
      print SWEEP_SUM $i+1, ',';
      $sweep_titles[$i] = sprintf '%d/', $i+1;
    }
    mkdir $batch_dir.$sweep_titles[$i] or die 'Error: Cannot "mkdir ', $batch_dir.$sweep_titles[$i], '" due to: ', $!; # Create directory from the sweep title
    foreach (sort keys %sweep_vals) { # Print the correct value (for the sweep name) for each model:param and variable:param name
      my @vals = split ' ', $sweep_vals{$_};
      print ' ' x ($Sweep_Table_Spacing-(length $vals[$i])); # Place spacing for stdout table
      print $vals[$i], '  ';
      print SWEEP_SUM $vals[$i], ',';
    }
    print "\n";
    print SWEEP_SUM "\n";
  }

  close SWEEP_SUM;

  print "\nWarning: There are ", scalar @sweep_titles, ' sweep titles for ', $sw_num_combos,
    ' sweep combinations; ', "Sweep simulations may not be named as anticipated\n"
    unless scalar @sweep_titles == $sw_num_combos;
}

##########
my %var_linenum; # Holds the line number for the declaration of a given .param variable
##########

if( scalar @vary_vars > 0 or scalar @sweep_vars > 0 ) { # If .param variables are varied or swept, place their declaration back into the netlist
  my @param_def_code; # Will become all of the .param declarations
  foreach my $var (@spice_params_ordered) {
    if( exists $vy_var_means{$var} ) { # Place varied .param variable declaration (defined as its mean) and keep track of its line number
      $var_linenum{$var} = $Netlist_Offset + scalar @param_def_code;
      push @param_def_code, '.param '.$var.' = '.$vy_var_means{$var}."\n";
    } elsif( exists $sw_var_vals{$var} ) { # Place swept .param variable declaration (defined as its 1st swept value) and keep track of its line number
      $var_linenum{$var} = $Netlist_Offset + scalar @param_def_code;
      my @all_vals = split /[ ,]/, $sw_var_vals{$var};
      push @param_def_code, '.param '.$var.' = '.$all_vals[0]."\n";
    } else { # Place non-varied and non-swept .param variable declaration (defined as its appropriate value)
      push @param_def_code, '.param '.$var.' = '.$spice_params{$var}."\n";
    }
  }
  splice @spice, $Netlist_Offset, 0, @param_def_code; # Add the .param declarations to the spice netlist
  foreach (keys %model_linenum) { # Must now correct the line numbers that were previously stored since @param_def_code was inserted at the top of the netlist
                                  # Only the model line numbers were affected
    $model_linenum{$_} += scalar @param_def_code;
  }

  foreach (@spice) {
    while((my $placeholder, my $expression) = each %spice_placeholders) { # Look through the spice netlist, and replace placeholders with the proper .param variable expressions
      s/$placeholder/\{$expression\}/ig;
    }
  }
}

##########
my %fet_linenum; # Holds the line number for the declaration of a given fet
my %fet_model; # Holds the model name for the declaration of a given fet
my %model_fets; # Holds the fet names (as a space delimited string) for a given model name
##########

for( $i = 0; $i < scalar @spice; $i++ ) {
  if( $spice[$i] =~ m/^(m[\w.]*)\s+[\w.]+\s+[\w.]+\s+[\w.]+\s+[\w.]+\s+([\w.]+)/i ) {
    # Found a transistor
    my $fetname = lc $1;
    my $model = lc $2;
    my $made_phys_copy_model = 0;
    die 'Error: ', $fetname, ' (line #', $i+1, ') refers to nonexistant model "', $model, "\"\n" if not exists $model_linenum{$model};

    if( exists $vary_params{$model} ) { # This transistor has varied parameters
      foreach (split ' ', $vary_params{$model}) { # Iterate through all parameters that are varied in this model
        # Count this fetname:param in the number of required data columns
        $data_column{$fetname.':'.$_} = $col++;
        if( exists $model_inline_param{$model.':'.$_} ) { # This parameter is declared inline
          # Append the parameter (with mean) on line if not already there
          if( $spice[$i] !~ m/\s+$_\s*=\s*\S+\s+/i ) {
            chomp $spice[$i];
            $spice[$i] = $spice[$i].' '.$_.'='.$vy_param_means{$model.':'.$_}."\n";
          }
        } elsif( $made_phys_copy_model == 0 ) { # This parameter is declared in the model declaration (and the model dec. must be duplicated)
          # Make a physical copy of the model and name the copy "fetname" (this is only done once)
          $made_phys_copy_model = 1;
          my $fet_model_linenum = scalar @spice;
          # Duplicate the spice model
          push @spice, $spice[$model_linenum{$model}];
          # Change the fet's model name to $fetname
          die 'Error: The model name ', $model, ' was matched multiple times (net and transistor names cannot be equal to model name) on spice line ',
            $i+1, ":\n", $spice[$i], "\n" if ($spice[$i] =~ s/\s+$model\s+/ $fetname /ig) > 1; # Note: "s/ / /" returns number of substitutions made
          # Change the model's name to $fetname
          $spice[$fet_model_linenum] =~ s/^\.MODEL\s+$model\s+/\.MODEL $fetname /i;
          # Update hashes (duplicate entries for this duplication of the model)
          $model_linenum{$fetname} = $fet_model_linenum;
          $model_fettype{$fetname} = $model_fettype{$model};
          $vary_params{$fetname} = $vary_params{$model};
          my @inline_keys = keys %model_inline_param;
          foreach (@inline_keys) {
            if( m/^$model:(\w+)$/i ) {
              $model_inline_param{$fetname.':'.$1} = $model_inline_param{$model.':'.$1};
            }
          }
          if( exists $sweep_params{$model} ) {
            my @sw_modelparam = keys %sweep_vals;
            foreach (@sw_modelparam) {
              if( m/^$model:(\w+)$/i ) {
                $sweep_vals{$fetname.':'.$1} = $sweep_vals{$model.':'.$1};
              }
            }
          }
        }
      }
    }

    # Keep track of the models and line numbers for each transistor, plus the names of the transistors for each model
    #  (do this here since the fetname could be modified if the transistor has multiple fins/fingers)
    $fet_linenum{$fetname} = $i;
    $fet_model{$fetname} = $model;
    $model_fets{$model} = exists $model_fets{$model} ? $model_fets{$model}.' '.$fetname : $fetname;
  }
}

push @spice, "\n";
push @spice, ".END\n"; # Appending .END at end of netlist
push @spice, "\n";

##########
my $req_col; # The number of data columns required for the netlist
my %r_data_column; # Holds the model:param (or option:param) combination for a given column number
##########

print "\nSummary of data columns for Parameter Variations:\n" unless scalar (keys %sweep_vals) > 0; # Not printing data columns for vary if sweeping
%r_data_column = reverse %data_column;
for( $i = 0; exists $r_data_column{$i}; $i++ ) {
  print 'Column ', $i+1, ' --> ', $r_data_column{$i}, "\n";
}
print "\n";

$req_col = $i; # $i holds the required number of columns from the for loop

# Output the modified netlist to 0.i in the simulation directory (it is functionally equivalent to the original netlist)
open OUT, ">$deckfile0" or die 'Error: Cannot open ', $deckfile0, ' due to: ', $!;
print OUT @spice;
close OUT;

if( scalar (keys %sweep_vals) > 0 ) { # Values are swept, so generate netlists for each sweep combination
  for( my $sweep_iter = 0; $sweep_iter < $sw_num_combos; $sweep_iter++ ) {
    while((my $sw_name, my $sw_lst) = each %sweep_vals) {
      my @sw_vals = split /[ ,]/, $sw_lst;

      die 'Internal error: Cannot extract fetname:param from ', $sw_name unless $sw_name =~ m/([\w.]+):(\w+)/i;
      my $model = $1;
      my $param = $2;
      if( $model eq 'variable' ) { # Sweeps a .param variable
        die 'Internal error: line number missing for .param variable: ', $param unless exists $var_linenum{$param};
        $spice[$var_linenum{$param}] =~ s/\s+$param\s*=\s*\S+/ $param=$sw_vals[$sweep_iter]/i;

      } else { # This is a model:param being swept
        if( !exists $model_inline_param{$sw_name} ) { # Normal fet parameter -- defined in .MODEL
          # Note: model name for a fet parameter defined in .MODEL is renamed to be the same as fetname
          die 'Internal error: line number missing for model: ', $model unless exists $model_linenum{$model};
          $spice[$model_linenum{$model}] =~ s/\s+$param\s*=\s*\S+/ $param=$sw_vals[$sweep_iter]/i;

        } else { # Inline fet parameter
          die 'Internal error: fetnames missing for model ', $model unless exists $model_fets{$model};
          foreach my $fet_name (split ' ', $model_fets{$model}) {
            die 'Internal error: line number missing for fet: ', $fet_name unless exists $fet_linenum{$fet_name};
            $spice[$fet_linenum{$fet_name}] =~ s/\s+$param\s*=\s*\S+/ $param=$sw_vals[$sweep_iter]/i;
          }
        }
      }
    }

    my $deckfile_sw0 = $batch_dir.$sweep_titles[$sweep_iter].'0'.$Fmt_Input; # Make a 0.i for each sweep combination
    open OUT, ">$deckfile_sw0" or die 'Error: Cannot open ', $deckfile_sw0, ' due to: ', $!;
    print OUT @spice;
    close OUT;
  }
  exit 0; # Cannot sweep and vary parameters at the same time, so exit here
}

if( !defined $op_corners ) { # Open the Monte Carlo values file to obtain data columns for sweep or Monte Carlo initialization (if not doing corner simulations)
  die "Monte Carlo values file not included in arguments, quitting...\n" unless defined $mc_vals_file;
  die "Number of Monte Carlo simulations not included in arguments, quitting...\n" unless defined $sim_num and $sim_num > 0;
  open VALUES, "<$mc_vals_file" or die "Error: Cannot open $mc_vals_file due to: $!";
}

print "Generating netlists...\n";
##########
my $copy_num = 0; # The number of the netlist the loop is currently generating
my @vals; # An array of the randomized values for the current netlist being generated
my $val_col; # The size of @vals (the number of columns the randomized number set has)
##########

# Print the Monte Carlo variation values summary to file (in the batch directory)
open VARY_SUM, ">$batch_dir$Vary_Sum_Filename" or die "Error: Cannot create $batch_dir$Vary_Sum_Filename for output due to ", $!;

# This gets a little messy for corner simulations:
# Do the loop for generating corner netlists based on the corner constants, but do not read from <VALUES>
goto LBEGIN if defined $op_corners;

while( <VALUES> ) {
  next unless m/^\d+/ or m/^-/; # Discards any line(s) of text (expecting space-delimited numbers)

  # Obtain a line of randomized values from the randomized number set (don't do if $op_corners is selected)
  @vals = split;
  $val_col = scalar @vals;
  if( $val_col < $req_col ) {
    close VALUES unless defined $op_corners;
    close VARY_SUM;
    die 'Error: Not enough columns in Monte Carlo values file (need ', $req_col, ', has ', $val_col, ' [value set is: ', $_, ']) in randomized number set for sim#', $copy_num+1;
  } # Incremented copy_num, or else Error message would be one off


  LBEGIN: $copy_num++;

  if( $copy_num == 1 ) { # This is the first varied netlist, so must print column headings for the Monte Carlo (MC) values summary file
    print VARY_SUM 'file number,';
    for( $i = 0; exists $r_data_column{$i}; $i++ ) {
      print VARY_SUM 'value ', $r_data_column{$i}, ','; # The final (weighted) value of the variable:param or fetname:param pair
      print VARY_SUM 'location ', $r_data_column{$i}, ','; # The location of the final value relative to the mean (it's the Monte Carlo value from file--expecting zero mean, unit std. dev.)
    }
  }
  print VARY_SUM "\n", $copy_num, ','; # Print the file number for the new row in the MC summary file

  # Cycle through every column of the randomized number set, and appropriately set the variable it corresponds to
  for( $i = 0; exists $r_data_column{$i}; $i++ ) {
    my $final_val; # The final (weighted) value of the variable:param or fetname:param pair
    my $mc_val; # The location of the final value relative to the mean (it's the MC value from file--expecting zero mean, unit std. dev.)

    if( $r_data_column{$i} !~ m/([\w.]+):(\w+)/i ) {
      close VALUES unless defined $op_corners;
      close VARY_SUM;
      die 'Internal error: Cannot extract fetname:param from ', $r_data_column{$i};
    }
    my $fetname = $1;
    my $param = $2;
    if( $fetname eq 'variable' ) { # Modifies a .param variable
      if( !exists $vy_var_means{$param} or !exists $vy_var_stddevs{$param} ) {
        close VALUES unless defined $op_corners;
        close VARY_SUM;
        die 'Internal error: mean or std_dev missing for .param variable:', $param;
      }
      if( !exists $var_linenum{$param} ) {
        close VALUES unless defined $op_corners;
        close VARY_SUM;
        die 'Internal error: line number missing for .param variable: ', $param;
      }
      $mc_val = $vals[$i]; # Note: .param variables are not placed into the data_columns hash for corner simulations, so no risk of corner sims here
      if( $vy_var_means{$param} eq 'file' ) { # Directly substitute randomized number from randomized number set
        $final_val = $mc_val;
      } elsif( $vy_var_stddevs{$param} eq 'default' ) { # Scale random number from random number set with mean and default std. dev.
        $final_val = $vy_var_means{$param} + $mc_val * $vy_var_means{$param} / $Default_Std_Dev_Factor;
      } else { # Scale random number from random number set with mean and specified std. dev.
        $final_val = $vy_var_means{$param} + $mc_val * $vy_var_stddevs{$param};
      }
      $spice[$var_linenum{$param}] =~ s/\s+$param\s*=\s*\S+/ $param=$final_val/i;
    } else { # Modifies a fet parameter
      if( !exists $fet_model{$fetname} ) {
        close VALUES unless defined $op_corners;
        close VARY_SUM;
        die 'Internal error: The model for "', $fetname, '" could not be found';
      }
      my $orig_model = $fet_model{$fetname};
      if( !exists $vy_param_means{$orig_model.':'.$param} or !exists $vy_param_stddevs{$orig_model.':'.$param} ) {
        close VALUES unless defined $op_corners;
        close VARY_SUM;
        die 'Internal error: mean or std_dev missing for ', $orig_model, ':', $param;
      }

      if( !exists $model_inline_param{$orig_model.':'.$param} ) { # Normal fet parameter -- defined in .MODEL
        # Note: model name for a fet parameter defined in .MODEL is renamed to be the same as fetname
        if( !exists $model_linenum{$fetname} ) {
          close VALUES unless defined $op_corners;
          close VARY_SUM;
          die 'Internal error: line number missing for model: ', $fetname;
        }
        # Set the corner value "cval" to the number of specified standard deviations with the +/- factor
        my $cval = ($model_fettype{$orig_model} eq 'nmos' ? $op_corners*$Corner_N_Factors[$copy_num-1] : $op_corners*$Corner_P_Factors[$copy_num-1]) if defined $op_corners;
        $mc_val = defined $op_corners ? $cval : $vals[$i];
        if( $vy_param_means{$orig_model.':'.$param} eq 'file' ) { # Directly substitute randomized number from randomized number set
          close VARY_SUM if defined $op_corners;
          die 'Error: Param. value from file makes no sense for corner test' if defined $op_corners;
          $final_val = $mc_val;
        } elsif( $vy_param_stddevs{$orig_model.':'.$param} eq 'default' ) { # Scale random number from random number set with mean and default std. dev.
          $final_val = $vy_param_means{$orig_model.':'.$param} + $mc_val * $vy_param_means{$orig_model.':'.$param} / $Default_Std_Dev_Factor;
        } else { # Scale random number from random number set with mean and specified std. dev.
          $final_val = $vy_param_means{$orig_model.':'.$param} + $mc_val * $vy_param_stddevs{$orig_model.':'.$param};
        }
        $spice[$model_linenum{$fetname}] =~ s/\s+$param\s*=\s*\S+/ $param=$final_val/i;
      } else { # Inline fet parameter
        if( !exists $fet_linenum{$fetname} ) {
          close VALUES unless defined $op_corners;
          close VARY_SUM;
          die 'Internal error: line number missing for fet: ', $fetname;
        }
        # Set the corner value "cval" to the number of specified standard deviations with the +/- factor
        my $cval = ($model_fettype{$orig_model} eq 'nmos' ? $op_corners*$Corner_N_Factors[$copy_num-1] : $op_corners*$Corner_P_Factors[$copy_num-1]) if defined $op_corners;
        $mc_val = defined $op_corners ? $cval : $vals[$i];
        if( $vy_param_means{$orig_model.':'.$param} eq 'file' ) { # Directly substitute randomized number from randomized number set
          close VARY_SUM if defined $op_corners;
          die 'Error: Param. value from file makes no sense for corner test' if defined $op_corners;
          $final_val = $mc_val;
        } elsif( $vy_param_stddevs{$orig_model.':'.$param} eq 'default' ) { # Scale random number from random number set with mean and default std. dev.
          $final_val = $vy_param_means{$orig_model.':'.$param} + $mc_val * $vy_param_means{$orig_model.':'.$param} / $Default_Std_Dev_Factor;
        } else { # Scale random number from random number set with mean and specified std. dev.
          $final_val = $vy_param_means{$orig_model.':'.$param} + $mc_val * $vy_param_stddevs{$orig_model.':'.$param};
        }
        $spice[$fet_linenum{$fetname}] =~ s/\s+$param\s*=\s*\S+/ $param=$final_val/i;
      }
    }
    print VARY_SUM $final_val, ',', $mc_val, ','; # Print the final value and MC value of this variable:param or fetname:param pair to the MC summary file
  }

  # TODO: In future allow to sweep and vary a netlist (and same parameters) at same time
  #       This script allowed a user to just just sweep a netlist, but an error would
  #       occur (it expected the Monte Carlo values for variation) and would exit the
  #       script.  A user can run this script multiple times to first sweep the netlist
  #       and then vary the netlist.
  if( scalar (keys %sweep_vals) > 0 ) { # Values are swept, so generate netlists for each sweep combination
    for( my $sweep_iter = 0; $sweep_iter < $sw_num_combos; $sweep_iter++ ) {
      while((my $sw_name, my $sw_lst) = each %sweep_vals) {
        my @sw_vals = split ' ', $sw_lst;
        if( $sw_name !~ m/([\w.]+):(\w+)/i ) {
          close VALUES unless defined $op_corners;
          close VARY_SUM;
          die 'Internal error: Cannot extract fetname:param from ', $sw_name;
        }
        my $model = $1;
        my $param = $2;
        if( $model eq 'variable' ) { # Sweeps a .param variable
          if( !exists $var_linenum{$param} ) {
            close VALUES unless defined $op_corners;
            close VARY_SUM;
            die 'Internal error: line number missing for .param variable: ', $param;
          }
          $spice[$var_linenum{$param}] =~ s/\s+$param\s*=\s*\S+/ $param=$sw_vals[$sweep_iter]/i;
        } else { # This is a model:param being swept
          if( !exists $model_inline_param{$sw_name} ) { # Normal fet parameter -- defined in .MODEL
            # Note: model name for a fet parameter defined in .MODEL is renamed to be the same as fetname
            if( !exists $model_linenum{$model} ) {
              close VALUES unless defined $op_corners;
              close VARY_SUM;
              die 'Internal error: line number missing for model: ', $model;
            }
            $spice[$model_linenum{$model}] =~ s/\s+$param\s*=\s*\S+/ $param=$sw_vals[$sweep_iter]/i;
          } else { # Inline fet parameter
            if( !exists $model_fets{$model} ) {
              close VALUES unless defined $op_corners;
              close VARY_SUM;
              die 'Internal error: fetnames missing for model ', $model;
            }
            foreach my $fet_name (split ' ', $model_fets{$model}) {
              if( !exists $fet_linenum{$fet_name} ) {
                close VALUES unless defined $op_corners;
                close VARY_SUM;
                die 'Internal error: line number missing for fet: ', $fet_name;
              }
              $spice[$fet_linenum{$fet_name}] =~ s/\s+$param\s*=\s*\S+/ $param=$sw_vals[$sweep_iter]/i;
            }
          }
        }
      }

      # Write varied netlist into the varied simulation directory
      # Name is taken from the corner names if corner simulations are being generated
      #   else, the name is a numeral (the value of "copy_num")
      my $deckfile_n = defined $op_corners ? $batch_dir.$sweep_titles[$sweep_iter].$Corner_Names[$copy_num-1].$Fmt_Input : $batch_dir.$sweep_titles[$sweep_iter].$copy_num.$Fmt_Input;
      open OUT, ">$deckfile_n" or die 'Error: Cannot open ', $deckfile_n, ' due to: ', $!;
      print OUT @spice;
      close OUT;
    }
  } else { # Not sweeping any values, so just print the varied netlist to file (once)
    # Write varied netlist into the varied simulation directory
    # Name is taken from the corner names if corner simulations are being generated
    #   else, the name is a numeral (the value of "copy_num")
    my $deckfile_n = defined $op_corners ? $batch_dir.$Corner_Names[$copy_num-1].$Fmt_Input : $batch_dir.$copy_num.$Fmt_Input;
    open OUT, ">$deckfile_n" or die 'Error: Cannot open ', $deckfile_n, ' due to: ', $!;
    print OUT @spice;
    close OUT;
  }

  goto LEXIT if $copy_num >= $sim_num; # Finished with netlist generation
  goto LBEGIN if defined $op_corners; # Loop back to top if doing corner sims
}

LEXIT: print VARY_SUM "\n";

close VARY_SUM;

if( !defined $op_corners ) { # Corner sims did not use MC values file, so no need to close file
  close VALUES;
}

print 'Warning: File terminated prematurely, only ', $copy_num, " files created\n" if $copy_num < $sim_num;
print "...Netlists generated.\n";


=head1 NAME

init_batch_seus.pl - A Perl script from the SEUS (Scripts for Easier Use of Spice) Package used to initialize a batch of spice netlists in a directory.  Typically this will be used to vary (e.g., for Monte Carlo simulations) or sweep transistor parameters, supply voltages, or options, such as temperature.

=head1 VERSION

Version 1.1.4

=head1 SYNOPSYS

    init_batch_seus.pl  [OPTIONS]  spice_deck_name  batch_netlist_directory  init_batch_def_file  [mc_values_file]  [#sims]

Run init_batch_seus.pl on a command line and provide the spice_deck_name (e.g., to initialize a batch of netlists from a spice netlist named I<inverter.i>, then use I<inverter> as the spice_deck_name), the name of the batch_netlist_directory to place the generated batch of netlists (if a pre-existing directory name is used, then it is first deleted so this new batch directory can take its place), and the definitions file (init_batch_def_file) for how to sweep or vary parameters in the batch of netlists to display a summary table of swept or varied parameters/variables.  If just sweeping parameters/variables, then the batch_netlist_directory will be initialized with the swept spice netlists; subdirectories will be created and each will have a 0.i netlist, which is a swept version of the original netlist.

The L<syntax of the init_batch_def_file|/"SYNTAX OF THE init_batch_def_file"> is described in the next section below.

Run init_batch_seus.pl on a command line and provide the spice_deck_name, the name of the batch_netlist_directory to place the generated batch of netlists (if a pre-existing directory name is used, then it is first deleted so this new batch directory can take its place), the definitions file (init_batch_def_file) for how to sweep or vary parameters in the batch of netlists, the Monte Carlo/variation values file (mc_values_file), and the number of simulations (#sims) to initialize the batch_netlist_directory with the varied spice netlists; the directory will contain numbered netlists from 0.i (which is functionally identical to the original netlist) to #sims.i (netlists 1.i to #sims.i are varied versions of the original netlist).

The L<syntax of the mc_values_file|/"SYNTAX OF THE mc_values_file"> is described two sections below.

After running this script, typically batchexec_seus.pl is used to simulate all netlists and a measurement script (using MeasSeus.pm) can summarize results of interest.

By default, this script assumes the use of ngspice or an ngspice-based simulator.  This script requires the "listing expand" operation provided by ngspice.

init_batch_seus.pl will print its usage to STDERR and exit if it is run without any command-line arguments.

=head1 SYNTAX OF THE init_batch_def_file

A batch of spice netlists can be generated for either swept parameters or varied parameters, but currently not both.  To signal init_batch_seus.pl that parameters will be swept or varied, use a SWEEP block or a VARY block in this file.  A swept or varied spice .param parameter/variable requires an identifier, "param.PARAM_name"; PARAM_name is the name of the .param parameter/variable to sweep or vary.  A swept or varied MOSFET transistor parameter first requires a model identifier, "model.MOSFET_model_name", then every line that follows, until the next model or .param parameter/variable is described, lists a parameter of that MOSFET transistor model to sweep or vary; MOSFET_model_name is the name of the MOSFET transistor model (such as pmos or nmos).

=head2 Defining values for varied parameters

When varying .param parameters/variables or MOSFET transistor parameters within a VARY block, the mean and standard deviation can be indicated or only the mean can be indicated (and a default standard deviation will be used).  Or, if the mean is not indicated, then the parameter value will be directly loaded from file without scaling.  If values are given for the mean and/or standard deviation, then they must be expressed in scientific notation so the init_batch_seus.pl script can correctly use these values in calculations (e.g., C<tox=12e-9> instead of C<tox=12n>); values expressed using Spice prefixes, such as 'n' for nano (1e-9) or 'u' for micro (1e-6), will not be recognized by this script and cause an error.

The following summarizes how values will be determined when varying parameters:

=over

=item *

If the mean is not specified, then parameter values will be directly loaded from file without scaling

=item *

If the mean is specified, then the data value is assumed to have a mean of 0 with a standard deviation (std_dev) of 1.  The parameter value will be scaled by the following formula: param_value = mean + data_value * std_dev.
If the standard deviation is not specified, then 3*std_dev = 10%*mean (or std_dev = (mean*0.1)/3) is used as the standard deviation by default.

=item *

Note: .param parameters/variables are declared as param:name in the init_batch_def_file, however, output of init_batch_seus.pl refers to these as "variables", such as the "Summary Table of Sweep Values"

=back

The following is an example of an init_batch_def_file for performing parameter variations on the amp-csrc-dc.i netlist in the examples/ directory.  This init_batch_def_file is available in the examples/ directory as init_batch_def.vary.txt.

=head2 Example VARY-block syntax for the init_batch_def_file when varying parameters

    VARY

    * Comments begin with an asterisk (*)

    * Variations in model parameters:
    model.nmos
    tox = 12e-9
    w
    l = 3e-6; stddev = 0.3e-6
    * Mean is specified and the default standard deviation (3*std_dev = 10%*mean) is used for "tox"
    * Values for "w" are loaded directly from file (no scaling)
    * Mean and standard deviation are both specified for "l"

    model.pmos
    vth0
    nch = 2.4e17
    * Values for "vth0" are loaded directly from file (no scaling)
    * Mean is specified and the default standard deviation (3*std_dev = 10%*mean) is used for "nch"

    * Variations in .param variables:
    param.tempc = 27; stddev = 10
    * Mean and standard deviation are both specified for "tempc"

    END VARY

Note: This init_batch_def_file is available in the examples/ directory as init_batch_def.vary.txt

=head2 Defining values for swept parameters

When sweeping .param parameters/variables or MOSFET transistor parameters within a SWEEP block, either the beginning, increment, and ending values can be specified; the nominal value and percent difference from the nominal value for the beginning, increment, and ending values can be specified; or a comma-delimited list of values can be specified.  Note that increment values may be positive or negative (for decrementing values).

It is optional to declare sweep names for the simulations.  To do so, use the "SWEEP NAMES" identifier followed by a comma- or space-delimited list of names for the generated directories.  The number of sweep names and steps should match, or else the names will be ignored.  If no sweep names are defined, then each netlist will be located in a numbered directory named 1, 2, etc.

The next example is of an init_batch_def_file for sweeping parameters of the amp-csrc-dc.i netlist in the examples/ directory.  This init_batch_def_file is available in the examples/ directory as init_batch_def.sweep.txt.  The second example is of an init_batch_def_file for a hierarchical sweep of parameters of the amp-csrc-dc.i netlist in the examples/ directory.  This init_batch_def_file is available in the examples/ directory as init_batch_def.sweep-hierarchy.txt.

=head2 Example non-hierarchical SWEEP-block syntax for the init_batch_def_file when sweeping parameters

    SWEEP

    model.nmos
    tox = 13e-9:-0.5e-9:11e-9
    * tox will be swept from 13n to 11n in -0.5n increments (5 steps)

    model.pmos
    w = 3e-6; -20%:10%:20%
    * width will be swept from 2.4u to 3.6u in 0.3u increments (5 steps)

    param.tempc = 0, 27, 50, 75, 100
    * .param variable "tempc" will be swept in 5 steps using the above values

    END SWEEP

Note 1: This init_batch_def_file is available in the examples/ directory as init_batch_def.sweep.txt

Note 2: Each sweep must take the same number of steps.  In this example, three parameters are swept with five steps apiece, generating five different netlists with swept parameters:

=over

=item *

Netlist 1/0.i: model.nmos.tox = 13.0e-9, model.pmos.w = 2.4e-6, param.tempc = 0

=item *

Netlist 2/0.i: model.nmos.tox = 12.5e-9, model.pmos.w = 2.7e-6, param.tempc = 27

=item *

Netlist 3/0.i: model.nmos.tox = 12.0e-9, model.pmos.w = 3.0e-6, param.tempc = 50

=item *

Netlist 4/0.i: model.nmos.tox = 11.5e-9, model.pmos.w = 3.3e-6, param.tempc = 75

=item *

Netlist 5/0.i: model.nmos.tox = 11.0e-9, model.pmos.w = 3.6e-6, param.tempc = 100

=back

=head2 Example hierarchical SWEEP-block syntax for the init_batch_def_file when sweeping parameters

    SWEEP NAMES 27c_a, 27c_b, 27c_c, 100c_a, 100c_b, 100c_c

    SWEEP LEVEL=2

    model.nmos
    tox = 12.5e-9:-0.5e-9:11.5e-9
    * tox will be swept from 12.5n to 11.5n in -0.5n increments (3 steps)

    model.pmos
    w = 3e-6; -10%:10%:10%
    * width will be swept from 2.7u to 3.3u in 0.3u increments (3 steps)

    END SWEEP


    SWEEP LEVEL=1

    param.tempc = 27, 100
    * .param variable "tempc" will be swept in 2 steps using the above values

    END SWEEP

Note 1: This init_batch_def_file is available in the examples/ directory as init_batch_def.sweep-hierarchy.txt

Note 2: The exact sweep level numbers do not matter as long as they can be sorted from smallest (highest hierarchy level) to largest (lowest hierarchy level).  This generates six different netlists with swept parameters:

=over

=item *

Netlist  27c_a/0.i: model.nmos.tox = 12.5e-9, model.pmos.w = 2.7e-6, param.tempc = 27

=item *

Netlist  27c_b/0.i: model.nmos.tox = 12.0e-9, model.pmos.w = 3.0e-6, param.tempc = 27

=item *

Netlist  27c_c/0.i: model.nmos.tox = 11.5e-9, model.pmos.w = 3.3e-6, param.tempc = 27

=item *

Netlist 100c_a/0.i: model.nmos.tox = 12.5e-9, model.pmos.w = 2.7e-6, param.tempc = 100

=item *

Netlist 100c_b/0.i: model.nmos.tox = 12.0e-9, model.pmos.w = 3.0e-6, param.tempc = 100

=item *

Netlist 100c_c/0.i: model.nmos.tox = 11.5e-9, model.pmos.w = 3.3e-6, param.tempc = 100

=back

=head1 SYNTAX OF THE mc_values_file

The mc_values_file must be a space-delimited list of numbers (e.g., Monte Carlo values) to assign to each dimension (e.g., each variable/parameter in the netlist).  The number of lines in this file should equal the desired number of simulations to run (#sims).  An example of this is available in the examples/ directory as init_batch_mcvals.vary.txt.

A random number generator can be used to create an mc_values_file, such as Carnegie Mellon University's quasi-Monte Carlo (CMU QMC) program "gennorm".  Contact the authors for the CMU QMC application if interested (see citation below):

=over

=item *

Amith Singhee and Rob A. Rutenbar, "From finance to flip-flops: A study of fast quasi-Monte Carlo methods from computational finance applied to statistical circuit analysis", International Symposium on Quality Electronic Design, 2007.  DOI: L<https://doi.org/10.1109/ISQED.2007.79>

=back

=head1 LIMITATIONS

Currently, this script only supports sweeping or varying .param netlist parameters/variables and model parameters for MOSFET transistors (in Spice, transistors with names beginning with "m").

A netlist cannot be swept and varied at the same time.  Due to this, and due to limitations with this script, a parameter cannot both be swept and varied.  Call init_batch_seus.pl multiple times to first sweep a netlist, then vary each resultant netlist.

MOSFET transistors cannot be given 5 terminals without this script misinterpreting the model name of the transistors.

If declared in the Spice netlist, the C<.option>, C<.print>, and C<.save> statements must each be entirely written on one line (e.g., for the C<.option> line, an C<+ ABSTOL=1E-6> cannot be listed on the subsequent line).

In addition, the hashes used to store model and parameter information in this script make a few assumptions:

=over

=item 1.

The colon (:) is not a valid character for a model or parameter name; the colon is used as a delimiter character within a few hashes.  Using this character will make this script misinterpret model and/or parameter names and lead to errors or incorrect netlists being generated.

=item 2.

No model or transistor is named "variable"; "variable" is used in a few hashes to identify .param parameters/variables not associated with a model, such as simulation temperature.  If a model or transistor has the name "variable", then this will make this script misinterpret the model and/or parameter/variable name and lead to errors or incorrect netlists being generated.

=item 3.

A model does not have the same name as a transistor; if a transistor has any non-inline, model parameters varied (e.g., tox is a non-inline model parameter declared in the model, while the transistor's width and length are parameters declared inline with the transistor), then the model is copied and named after that transistor in the generated netlists and the parameters of the new model are varied as defined by the user.  If a model and a transistor already share a name, then this script may malfunction and/or two models with the same name will be present in the generated Spice netlists and cause an error during Spice execution.

=back

=head1 OPTIONS

To see all available options, run init_batch_seus.pl without any options to print its usage, including all options, to STDERR.

If sweeping parameters of a netlist, then by default, if no options are provided, this script will output a summary table of swept parameters and generate a directory of swept netlists as long as the netlist name, name of the directory to place swept netlists, and the "init_batch_def_file" is specified.

If varying parameters of a netlist, then by default, if no options are provided, this script will output a summary table of varied parameters, generate a directory with a 0.i netlist (which is functionally identical to the original netlist), and exit as long as the netlist name, name of the directory to place varied netlists, and the "init_batch_def_file" is specified.

If varying parameters of a netlist, then by default, if no options are provided, this script will output a summary table of varied parameters, generate a directory of varied netlists (0.i to #sims.i) as long as the netlist name, name of the directory to place varied netlists, the "init_batch_def_file" is specified, the "mc_values_file" is specified, and the number of simulations (#sims) is specified.

=head2 -f  Use netgen_seus.pl to expand mosfet transistor fingers into separate mosfets

By default, init_batch_seus.pl uses netgen_seus.pl to help generate the batch of netlists with varied or swept parameters/variables.  Using the -f option of init_batch_seus.pl will  use the -f option of netgen_seus.pl and mosfet transistor fingers will be expanded into separate mosfet transistors; mosfet transistors with fingers (e.g., if mosfet "m1" has three fingers or m=3) will be deleted and replaced with new numbered transistors (e.g., "m1.1", "m1.2", and "m1.3").

Note, currently, if the number of mosfet transistor fingers are defined by a parameter and if any parameters are varied or swept by init_batch_seus.pl, then these mosfet transistor fingers will not be expanded; this is a known limitation and will be fixed in a future revision.

Also note, netgen_seus.pl will not check the other transistor names in the netlist when expanding mosfet transistors; this can cause Spice syntax errors to occur.  Avoid naming transistors similar to the numbering scheme used by netgen_seus.pl; for example, if you have a mosfet transisor named "m1" with fingers that you wish to expand, do not name another transistor "m1.#" in your circuit (where # is a number which is greater than or equal to 1).

=head2 -l  Suppress display of SEUS's AGPL-3.0-or-later license information

If the -l option is passed, then the short description of the license is not printed. The seus.pl script, the main entry point for running SEUS scripts, passes the -l option since it prints the license information itself.

=head2 -o path  Run other Spice executable at "path" (this must support "listing expand")

By default, init_batch_seus.pl will call "ngspice" to expand subcircuits in the netlist using the "listing expand" operation of ngspice.  This option can be used if you wish to use a different simulator (e.g., a different ngspice-based simulator).  For this to work properly, the simulator should run at "path" at the directory location where init_batch_seus.pl is run, the simulator must be a file that can be executed with IPC::Run, and lastly (and most importantly) the simulator must support the "listing expand" operation to expand subcircuits; this is so each transistor in each subcircuit instantiation can be swept or independently varied.

=head2 -u path  Use "path" for the location of the SEUS Package (if SEUS Package not installed)

If the SEUS Package is successfully installed, then just providing the script name (e.g., init_batch_seus.pl) is sufficient to run the script.  If the SEUS Package is not installed, then the path to the SEUS Package must be provided since, by default, this script will call netgen_seus.pl.

=head1 TODO

The following is a list of known bugs to fix, potential revisions, or potential additions which are ordered by urgency (e.g., HIGH, MODERATE, or LOW):

=over

=item *

(HIGH Revision) Do not rely on ngspice "listing expand" for subcircuit expansion; this script should do this instead

=over

=item *

Or, show better error if ngspice netlist expansion fails (Note: unclear if ngspice gives an error; e.g., subcircuit definition provided twice)

=back

=item *

(HIGH Revision) Support varying and sweeping a netlist, and support parameters that are both swept and varied.  E.g., Sweep a parameter across some particular values, then vary the parameter's values to get Monte Carlo results for each swept value

=over

=item *

Place varied and swept parameters in same model:param hashes, and use a separate hash to indicate if the parameter is varied, swept, or both

=back

=item *

(MODERATE Bug Fix) Ensure the nominal 0.i generated during one level of sweep is functionally equivalent to the original netlist (variable substitutions are made and two .option lines may appear in 0.i)

=item *

(MODERATE Revision) Use a different file format/layout for the "init_batch_def_file"

=over

=item *

Perhaps add /* */ comment support to "init_batch_def_file"

=back

=item *

(MODERATE Revision) Find a better way to define corners.  Perhaps define a "profile" defining the parameter values/changes to use for a netlist.

=over

=item *

Maybe define corners more clearly by parameter (e.g., fast (f) = decrease R, increase w, decrease l, ...) or, instead allow swept parameters (such as corners) to be named in the "init_batch_def_file"

=back

=item *

(MODERATE Revision) Give back better error if sweeping a .param variable not present in the netlist (an internal error is printed)

=item *

(MODERATE Revision) Create additional functions/subroutines and move these to a separate Perl module to clean up the code for this script

=item *

(MODERATE Revision) Use a different character (such as "*") as a delimiter for model and parameter names, instead of ":".  Define a scalar for this constant character.

=item *

(MODERATE Revision) Re-examine how to handle transistor parameters declared inline with a transistor versus transistor parameters declared in a model

=item *

(MODERATE Revision) If using parameter substitution when calling netgen_seus.pl, then MOSFET fingers which use a parameter will not be expanded; this is because a placeholder is being used for the finger count.  init_batch_seus.pl could call netgen_seus.pl a second time to expand these MOSFET fingers, but this will be resolved if netgen_seus.pl can expand subcircuits.

=item *

(LOW Addition) Allow sweep names to be hierarchical.  Lowest hierarchy is the filename (or more likely, the directory containing "0.i"), and upper hierarchies are used for parent directory names.  Parameter value and names could be used at each sweep level (e.g., 1ntx/2nrd...) or sweep names could instead be defined within each sweep block; or could just have long directory names (append parameter value/name and/or sweep level titles with "_" instead (e.g., 1ntx_2nrd...).  This is not completely trivial, but it is similar to creating the sweep values.

=back

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

