#!/usr/bin/perl -w
use strict;
use warnings;

our $VERSION = '1.1.4';

# SEUS Package's seus.pl:
#   Main entry-point for running all SEUS Package scripts
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'seus.pl';

# License
my $License = "\n".
"SEUS $Script_Name Copyright (C) 2009-2020 Michael A. Turi\n".
"This program comes with ABSOLUTELY NO WARRANTY.  This is free software,\n".
"  and you are welcome to redistribute it under certain conditions.\n".
"Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')\n".
"  in the SEUS package for more details.\n\n";

# Requires scripts:
my $BatchExec = 'batchexec_seus.pl';
my $InitBatch = 'init_batch_seus.pl';
my $MakeOut = 'mkout_seus.pl';
my $NetGen = 'netgen_seus.pl';
my $Run = 'run_seus.pl';

# Usage:
my $Usage = ''.
'Usage: '.$Script_Name."  --SEUS_command  [options and/or parameters for SEUS_command]\n".
"The following SEUS commands are supported:\n".
"  'BatchExec'/'b'..Batch execute spice simulations (for Monte Carlo sims)\n".
"  'InitBatch'/'i'..Initialize a batch of spice netlists (for Monte Carlo sims)\n".
"  'MakeOut'/'m'....Convert ngspice .o file output to other file formats\n".
"  'NetGen'/'n'.....Correctly generates a netlist for an ngspice simulation\n".
"  'Run'/'r'........More-easily run an ngspice simulation with a timer\n\n".
"Note: Each SEUS_command can be issued using its name or first letter (this is\n".
"      case insensitive) and zero, one, or two dashes may precede it.\n".
"      For example: Run, -run, or --r are all fine for the 'Run' SEUS command\n";
my $Min_Arg_Num = 1;

# seus.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. and Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is the main entry-point Perl script for running all SEUS Package scripts.
# This script has the ability to run the batchexec_seus.pl (use "BatchExec" or
# "B"), init_batch_seus.pl (use "InitBatch" or "I"), mkout_seus.pl (use
# "MakeOut" or "M"), netgen_seus.pl (use "NetGen" or "N"), and run_seus.pl
# (use "Run" or "R") scripts.  The MeasSeus.pm Perl module is typically used
# by a Perl measurement script; for an example, please see the
# meas_sram4t2r-tran.pl script in the examples directory.
#
# Note, either the name or first letter of the SEUS command can be used to run
# the specified script; you may use zero, one, or two dashes to precede the
# SEUS command name or letter.  Also the names/letters of the SEUS command to
# use for each script are case insensitive.  A "-l" option is passed to each
# SEUS script called so the license information will not be printed multiple
# times (this script, seus.pl, will print the license information). 
# 
# Usage: Provide the SEUS command as well as any other options or arguments
#        required by the specified script.  Using a SEUS command (calling a
#        SEUS script) without any additional options or arguments will
#        cause it to print its usage and exit.

print "** $Script_Name ** SEUS Package Version: $VERSION **\n$License";
die $Usage if scalar @ARGV < 1;
my $argnum = scalar @ARGV;

my $seus_cmd = shift;

my @args = ('-l'); # Add argument to suppress displaying license info for issued command
push @args, @ARGV;

if( $seus_cmd =~ m/^-?-?b$/i or $seus_cmd =~ m/^-?-?batchexec$/i ) {
  system( $BatchExec, @args ) == 0
    or die 'Error: "', $BatchExec, ' ', (join ' ', @args), '" failed (see error above)';

} elsif( $seus_cmd =~ m/^-?-?i$/i or $seus_cmd =~ m/^-?-?initbatch$/i ) {
  system( $InitBatch, @args ) == 0
    or die 'Error: "', $InitBatch, ' ', (join ' ', @args), '" failed (see error above)';

} elsif( $seus_cmd =~ m/^-?-?m$/i or $seus_cmd =~ m/^-?-?makeout$/i ) {
  system( $MakeOut, @args ) == 0
    or die 'Error: "', $MakeOut, ' ', (join ' ', @args), '" failed (see error above)';

} elsif( $seus_cmd =~ m/^-?-?n$/i or $seus_cmd =~ m/^-?-?netgen$/i ) {
  system( $NetGen, @args ) == 0
    or die 'Error: "', $NetGen, ' ', (join ' ', @args), '" failed (see error above)';

} elsif( $seus_cmd =~ m/^-?-?r$/i or $seus_cmd =~ m/^-?-?run$/i ) {
  system( $Run, @args ) == 0
    or die 'Error: "', $Run, ' ', (join ' ', @args), '" failed (see error above)';

} else {
  die 'Error: Unrecognized SEUS Command: "', $seus_cmd, ' ', (join ' ', @args), "'";
}


=head1 NAME

seus.pl - The main entry-point Perl script for running all SEUS (Scripts for Easier Use of Spice) Package scripts

=head1 VERSION

Version 1.1.4

=head1 SYNOPSYS

    seus.pl  --SEUS_command  [options and/or parameters for SEUS_command]

Run seus.pl on a command line and provide the SEUS command name to execute that particular SEUS script.  Other options and parameters for the SEUS command should be listed after the SEUS command.  seus.pl will also pass the -l option to each SEUS script called so the license information will not be printed multiple times (seus.pl will print the license information). 

Each SEUS command is case insensitive and can be issued by using its name or first letter and zero, one, or two dashes may precede it.  For example, the following are all acceptable ways to execute the 'Run' SEUS command to display its usage to STDERR:

=over

=item *

seus.pl Run

=item *

seus.pl -run

=item *

seus.pl --r

=back

seus.pl will print its usage to STDERR and exit if it is run without a SEUS command.

=head1 SEUS COMMANDS

To see all available SEUS commands, run seus.pl without any command-line arguments to print its usage to STDERR.  The following SEUS commands are supported:

=head2 BatchExec - Batch execute spice simulations (e.g., for Monte Carlo simulations)

This script will batch execute a directory of spice simulations.  To see all available options for this script, then run this SEUS command without any additional command-line arguments to print its usage, including all options, to STDERR.

For more information about this SEUS command, please refer to the documentation for batchexec_seus.pl; this is available in batchexec_seus.html or by using "perldoc batchexec_seus.pl".

=head2 InitBatch - Initialize a batch of spice netlists (e.g., for Monte Carlo simulations)

This script will initialize a batch of spice netlists in a directory.  To see all available options for this script, then run this SEUS command without any additional command-line arguments to print its usage, including all options, to STDERR.

For more information about this SEUS command, please refer to the documentation for init_batch_seus.pl; this is available in init_batch_seus.html or by using "perldoc init_batch_seus.pl".

=head2 MakeOut - Convert ngspice .o file output to other file formats

This script will convert ngspice .o file output to .csv, .dat, .sti, and/or .txt file formats for various waveform viewers.  Note: If using the SEUS Package's MeasSeus.pm Perl module to make measurements from ngspice simulation output, then the piecewise linear (PWL), .sti output format must be used.  MeasSeus.pm is typically used by a Perl measurement script; for an example, please see the meas_sram4t2r-tran.pl script in the examples directory.

To see all available options for this script, then run this SEUS command without any additional command-line arguments to print its usage, including all options, to STDERR.

For more information about this SEUS command, please refer to the documentation for mkout_seus.pl; this is available in mkout_seus.html or by using "perldoc mkout_seus.pl".

=head2 NetGen - Correctly generates a netlist for an ngspice simulation

This script will initialize a batch of spice netlists in a directory.  To see all available options for this script, then run this SEUS command without any additional command-line arguments to print its usage, including all options, to STDERR.  By default, this command expects the input netlist to come from STDIN and the generated, output netlist to go to STDOUT.

The reason for I<NetGen> is that an ngspice-based simulator (University of Florida's Spice3-UFDG (Linux version 3.71)) incorrectly handles parameter substitutions; the results vary depending on whitespace and comments.

For more information about this SEUS command, please refer to the documentation for netgen_seus.pl; this is available in netgen_seus.html or by using "perldoc netgen_seus.pl".

=head2 Run - More easily run an ngspice simulation with a timer

This script will run ngspice, or a ngspice-based simulator, and generate a timer to display the simulation's run time.  This timer can be configured to terminate the simulation, if it runs too long.  To see all available options for this script, then run this SEUS command without any additional command-line arguments to print its usage, including all options, to STDERR.

For more information about this SEUS command, please refer to the documentation for run_seus.pl; this is available in run_seus.html or by using "perldoc run_seus.pl".

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

