* DC sweep of common-source amplifier with pmos load

* An example netlist for the SEUS package

* This netlist comes with ABSOLUTELY NO WARRANTY.
* This is free and you are welcome to redistribute it under certain conditions.
* Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')
*   in the SEUS package for more details.

* Using BSIM3 MOSFET models
.model nmos nmos level=49
+ VERSION=3.3
+ VTH0=0.6
+ U0=670
+ TOX=12n
+ NCH=2E17
+ NSUB=1E17
+ XT=1.5E-7

.model pmos pmos level=49
+ VERSION=3.3
+ VTH0=-0.6
+ U0=250
+ TOX=12n
+ NCH=2E17
+ NSUB=1E17
+ XT=1.5E-7

.param l = 0.35u
.param w = 3u
.param vddval = 3.3
.param tempc = 27
.param pfingers = 3

* D G S B

* Amplifier subcircuit
.SUBCKT amp inp out vcc vgnd
* Note: ngspice does not allow comments inline with subcircuit header

mp out out vcc  vcc  pmos l={l} w={w} m={pfingers}
mn out inp vgnd vgnd nmos l={l} w={w} m=2
* Note: ngspice does not allow comments inline with MOSFET

.ic v(inp)=0 v(out)=3.3

.ENDS amp

xinv in out vdd vss amp
* Note: ngspice does not allow comments inline with subcircuit instance

vin in 0 dc 0
vdd vdd 0 dc {vddval}
vss vss 0 dc 0

.dc vin 0 {vddval} 0.1

.OP
.PRINT DC v(in) v(out) i(vdd) i(vss)

.OPTION  ABSTOL=1E-6 PIVTOL=1E-10 GMIN=1E-14 VNTOL=2.5E-5 RELTOL=1E-2 CHGTOL=1E-18 METHOD = GEAR ITL1=1000 ITL4=500 TEMP={tempc}

.END

