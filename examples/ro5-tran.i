* Transient analysis of five inverter ring oscillator

* An example netlist for the SEUS package

* This netlist comes with ABSOLUTELY NO WARRANTY.
* This is free and you are welcome to redistribute it under certain conditions.
* Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')
*   in the SEUS package for more details.

* Using BSIM3 MOSFET models
.model nmos nmos level=49
+ VERSION=3.3
+ VTH0=0.6
+ U0=670
+ TOX=12n
+ NCH=2E17
+ NSUB=1E17
+ XT=1.5E-7

.model pmos pmos level=49
+ VERSION=3.3
+ VTH0=-0.6
+ U0=250
+ TOX=12n
+ NCH=2E17
+ NSUB=1E17
+ XT=1.5E-7

.param l = 0.35u
.param w = 3u
.param vddval = 3.3
.param tempc = 27

* D G S B

* Inverter subcircuit
.SUBCKT inv in out vdd vss

mp out in vdd vdd pmos l={l} w={w} m=1
mn out in vss vss nmos l={l} w={w} m=1

.ENDS inv

* 2-Input NAND gate subcircuit
.SUBCKT nand2 in1 in2 out vdd vss

mp1 out in1 vdd vdd pmos l={l} w={w} m=1
mp2 out in2 vdd vdd pmos l={l} w={w} m=1
mn1 int in1 vss vss nmos l={l} w={w} m=1
mn2 out in2 int vss nmos l={l} w={w} m=1

.ENDS nand2

vdd vdd 0 dc {vddval}
vss vss 0 dc 0

* Five inverter ring oscillator (using NAND gate to control oscillator)
xnand en inv4out nandout vdd vss nand2
xinv1 nandout inv1out vdd vss inv
xinv2 inv1out inv2out vdd vss inv
xinv3 inv2out inv3out vdd vss inv
xinv4 inv3out inv4out vdd vss inv

ven en 0 dc 0 pwl( 0n 0  1n 0  5n {vddval}  15n {vddval}  19n 0 )

.tran 0.5n 20n

.OP
.PRINT TRAN v(en) v(nandout) v(inv1out) v(inv2out) i(vdd) i(vss)

.OPTION  ABSTOL=1E-6 PIVTOL=1E-10 GMIN=1E-14 VNTOL=2.5E-5 RELTOL=1E-2 CHGTOL=1E-18 METHOD = GEAR ITL1=1000 ITL4=500 TEMP={tempc}

.END

