#!/usr/bin/perl -w
use strict;
use warnings;

use MeasSeus;

# SEUS Package's meas_sram4t2r-tran.pl:
#   An example measurement script for the SEUS package.  This can be used as a
#   model to write custom measurement scripts.
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'meas_sram4t2r-tran.pl';

my $Data_Format = '.sti';
# .sti format for Mentor Graphics EZWave or Synopsys WaveView

# Usage:
my $Usage = ''.
"Usage: $Script_Name  measurement_results_file  output_data_file(s)\n".
"  measurement_results_file will written in .csv format in append mode.\n".
"  This script expects at least one output_data_file.  For more than one\n".
"    output_data_file, give a space-delimited list of files/pathnames.\n";
my $Min_Arg_Num = 2;

# meas_sram4t2r-tran.pl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. & Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
# 
# This is an example Perl script that measures and calculates performance of
# an ngspice netlist; specifically the example sram4t2r-tran.i netlist of a
# 4T+2R (four transistors and two resistors) SRAM cell.
#
# This script uses the MeasSeus.pm Perl package to calculate measurements from
# output data in piecewise linear (PWL) format.  This script expects the output
# data to be in an ".sti" file.  You can generate this file by running:
# "run_seus.pl -s sram4t2r-tran".
# 
# Usage: Provide the Spice output_data_file name(s), and measurements will be
#        calculated from the "output_data_file.sti" file(s) and written to
#        measurement_results_file in .csv format.  For more than one
#        output_data_file, provide a space-delimited list of files/pathnames;
#        at least one output_data_file is expected.

print STDERR '** ', $Script_Name, " **\n\n";
die $Usage if scalar @ARGV == 0;
my $argnum = scalar @ARGV;
die "Error: Not enough parameters\n\n$Usage" if $argnum < $Min_Arg_Num;

my $meas_results_filename = shift;

# Measurement Settings:
# ---------------------
# Triggers when Write causes Bit/NBit to increase to X% of Vdd and Nbit/Bit to decrease to X% of Vdd
my $WR_HIGH_TRIG = 0.9;
my $WR_LOW_TRIG = 0.1;
# Trigger when Read causes Bit/Nbit discharge to X% of Vdd
my $RD_LOW_TRIG = 0.5;

my @meas_setting_names = ( 'wr_high_trig', 'wr_low_trig', 'rd_low_trig' );
my @meas_setting_values = ( $WR_HIGH_TRIG, $WR_LOW_TRIG, $RD_LOW_TRIG );

# Constants:
# ----------
# Netlist Nets
my $NET_B = 'bit';
my $NET_NB = 'nbit';
my $NET_BITCELL = 'bitcell';
my $NET_NBITCELL = 'nbitcell';

# Voltage Sources
my $V_CELL = 'vdd';

# Netlist Variables (send these values to this script by using a net set by a voltage source)
my $VAR_VDD_VAL = 'vddval';
my $VAR_SIMLEN = 'simlen';

my @net_var_names = ( 'vdd value', 'sim length' );

my $filelist_name_count = 0;
my $filelist_names = join ',', @meas_setting_names;
$filelist_names .= ',filename,works';
my @filelist_data;

foreach (@ARGV) {

  # Start output with the measurement settings
  my @data_names = @meas_setting_names;
  my @data_values = @meas_setting_values;

  # Add filename to output and assume file will not open
  push @data_names, 'filename', 'works';
  push @data_values, $_, 'error - could not open';

  my $in_fh;
  open $in_fh, "<$_$Data_Format" or die 'Error (', $Script_Name,
    '): Cannot open ', $_, $Data_Format, ' due to: ', $!;

  ###########################
  # Begin Measurement Section

  # Load variable values (if present in your netlist)
  my %vdd_voltage;
  get_signal_sti( $in_fh, $VAR_VDD_VAL, 'V', \%vdd_voltage );
  my $vdd_val = yval( \%vdd_voltage, 0 );
  my %simlen_variable;
  get_signal_sti( $in_fh, $VAR_SIMLEN, 'V', \%simlen_variable );
  my $simlen = yval( \%simlen_variable, 0 );

  my @net_var_values = ( $vdd_val, $simlen );

  # Define Constants
  # Voltage threshold values (10% and 90%)
  my $THRES_10 = 0.1*$vdd_val;
  my $THRES_90 = 0.9*$vdd_val;

  # Triggers/threshold values for voltages at end of read/write operations
  my $WR_HIGH_TRIG_VOLT = $WR_HIGH_TRIG*$vdd_val;
  my $WR_LOW_TRIG_VOLT  = $WR_LOW_TRIG*$vdd_val;
  my $RD_LOW_TRIG_VOLT  = $RD_LOW_TRIG*$vdd_val;

  # Time in the simulation to check the values stored in the cell
  my $LOC_1_CELL = 300e-9;
  my $LOC_0_CELL = 1000e-9;

  # Time in the simulation to measure the leakage current
  my $LOC_LK = 1000e-9;

  # Time in the simulation where the precharge and read 0 operation begins/ends
  my $LOC_PG0_BEG = 1030e-9;
  my $LOC_RD0_BEG = 1230e-9;
  my $LOC_RD0_END = 1360e-9;

  # Time in the simulation where the write 1 operation begins/ends
  my $LOC_WR1_BEG = 30e-9;
  my $LOC_WR1_END = 160e-9;

  # Values to test to see if the circuit works
  my %bitcell_voltage;
  get_signal_sti( $in_fh, $NET_BITCELL, 'V', \%bitcell_voltage );
  my %nbitcell_voltage;
  get_signal_sti( $in_fh, $NET_NBITCELL, 'V', \%nbitcell_voltage );

  my $yb1  = yval(  \%bitcell_voltage, $LOC_1_CELL );
  my $ynb1 = yval( \%nbitcell_voltage, $LOC_1_CELL );
  my $yb0  = yval(  \%bitcell_voltage, $LOC_0_CELL );
  my $ynb0 = yval( \%nbitcell_voltage, $LOC_0_CELL );

  my %bit_voltage;
  get_signal_sti( $in_fh, $NET_B, 'V', \%bit_voltage );
  my %nbit_voltage;
  get_signal_sti( $in_fh, $NET_NB, 'V', \%nbit_voltage );

  ### Test if the circuit works and capture error message(s) in $works
  my $works = "";
  $works .= 'Init. Store 1 Wrong; ' if (($yb1 < $THRES_90) || ($ynb1 > $THRES_10));
  $works .= 'Write 0 Failed; ' if (($yb0 > $THRES_10) || ($ynb0 < $THRES_90));

  if ( $works ne "" ) {
    $data_values[-1] = "error = $works"; # Data value for "works"
    # Add to list of data for all data files
    push @filelist_data, (join ',', @data_values);
    close $in_fh;
    next; # Continue to the next data file
  } else {
    $data_values[-1] = 0; # Data value for "works"
  }

  # Add variable names/values to the output
  push @data_names, @net_var_names;
  push @data_values, @net_var_values;

  ### Measure the Leakage Current
  push @data_names, 'ave_leak_cell';
  my %vddcell_current;
  get_signal_sti( $in_fh, $V_CELL, 'I', \%vddcell_current );
  my %vddcell_abs_current = %vddcell_current;
  abs_waveform( \%vddcell_abs_current );
  my $ave_lkcell = yval( \%vddcell_abs_current, $LOC_LK );
  push @data_values, $ave_lkcell;

  ### Measure the Read Time (when reading a 0)
  # Uses xval to find the exact time when a signal crossing occurs
  push @data_names, 'rd0time';
  my @rd0ps = xval( \%bit_voltage, $RD_LOW_TRIG*$vdd_val,
                    $LOC_RD0_BEG, $LOC_RD0_END );
  my $rd0cross = scalar @rd0ps;
  if( $rd0cross != 1 ) { # Only expecting there to be one signal crossing
    my $rdcrossvals = ($rd0cross > 0) ? '('.(join ',', @rd0ps).')' : '';
    push @data_values, "error-$rd0cross read0 crossings $rdcrossvals at $RD_LOW_TRIG_VOLT-V";
    # Add to list of data for all data files
    push @filelist_data, (join ',', @data_values);
    close $in_fh;
    next; # Continue to the next data file
  }
  my $rd0time = $rd0ps[0] - $LOC_RD0_BEG;
  push @data_values, $rd0time;

  ### Measure the Read Current (when reading a 0)
  push @data_names, 'rd0_cell_current';
  my $rd0cur = avg(\%vddcell_abs_current, $LOC_RD0_BEG, $LOC_RD0_BEG+$rd0time);
  push @data_values, $rd0cur;

  # Calculate power, energy, and energy-delay product (EDP)
  push @data_names, 'rd0_cell_power', 'rd0_cell_energy',
    'rd0_cell_edp';
  push @data_values, $rd0cur*$vdd_val, $rd0cur*$vdd_val*$rd0time,
    $rd0cur*$vdd_val*$rd0time*$rd0time;

  ### Measure the Write Time (when writing a 1)
  # First, measure time it takes for Bit to go from 0 to 1
  push @data_names, 'wr1_time_bit_hi';
  my @wr1ps_bhi = xval( \%bitcell_voltage, $WR_HIGH_TRIG*$vdd_val,
                        $LOC_WR1_BEG, $LOC_WR1_END );
  my $wr1cross_b = scalar @wr1ps_bhi;
  if( $wr1cross_b != 1 ) { # Only expecting there to be one signal crossing
    my $wrcrossvals = ($wr1cross_b > 0) ? '('.(join ',', @wr1ps_bhi).')' : '';
    push @data_values, "error-$wr1cross_b write1 bitcell crossings $wrcrossvals at $WR_HIGH_TRIG_VOLT-V";
    # Add to list of data for all data files
    push @filelist_data, (join ',', @data_values);
    close $in_fh;
    next; # Continue to the next data file
  }
  my $wr1time_bhi = $wr1ps_bhi[0] - $LOC_WR1_BEG;
  push @data_values, $wr1time_bhi;

  # Second, measure time it takes for NBit to go from 1 to 0
  push @data_names, 'wr1_time_nbit_low';
  my @wr1ps_nlow = xval( \%nbitcell_voltage, $WR_LOW_TRIG*$vdd_val,
                         $LOC_WR1_BEG, $LOC_WR1_END );
  my $wr1cross_nb = scalar @wr1ps_nlow;
  if( $wr1cross_nb != 1 ) { # Only expecting there to be one signal crossing
    my $wrcrossvals =($wr1cross_nb > 0) ? '('.(join ',', @wr1ps_nlow).')' : '';
    push @data_values, "error-$wr1cross_nb write1 nbitcell crossings $wrcrossvals at $WR_LOW_TRIG_VOLT-V";
    # Add to list of data for all data files
    push @filelist_data, (join ',', @data_values);
    close $in_fh;
    next; # Continue to the next data file
  }
  my $wr1time_nlow = $wr1ps_nlow[0] - $LOC_WR1_BEG;
  push @data_values, $wr1time_nlow;

  # The write time is the maximum of time for Bit 0->1 and NBit 1->0
  push @data_names, 'wr1time';
  my $wr1time = ($wr1time_bhi > $wr1time_nlow) ? $wr1time_bhi : $wr1time_nlow;
  push @data_values, $wr1time;

  ### Measure the Write Current (when writing a 1)
  push @data_names, 'write1_cell_current';
  my $wr1cur = avg(\%vddcell_abs_current, $LOC_WR1_BEG, $LOC_WR1_BEG+$wr1time);
  push @data_values, $wr1cur;

  # Calculate power, energy, and energy-delay product (EDP)
  push @data_names, 'write1_cell_power', 'write1_cell_energy',
    'write1_cell_edp';
  push @data_values, $wr1cur*$vdd_val, $wr1cur*$vdd_val*$wr1time,
    $wr1cur*$vdd_val*$wr1time*$wr1time;

  ### Checking the Precharging (of the bit line)
  my @pgb_trigs = xval( \%bit_voltage, $RD_LOW_TRIG*$vdd_val, $LOC_PG0_BEG, $LOC_RD0_BEG );
  my $pgb_trig;
  #if( scalar @pgb_trigs != 1 ) {
    $pgb_trig = ( scalar @pgb_trigs < 1 ) ? $LOC_PG0_BEG : $pgb_trigs[-1];
  #}
  my $pgbval = yval( \%bit_voltage, $pgb_trig+$rd0time );

  ### Set the flag if appropriate
  my $flag = "";
  $flag .= "Precharge Bit too low ($pgbval); " if ( $pgbval < $THRES_90 );
  push @data_names, 'flag';
  $flag = 0 if ($flag eq "");
  push @data_values, $flag;

  # End Measurement Section
  #########################
  
  if( scalar @data_names > $filelist_name_count ) {
    $filelist_name_count = scalar @data_names;
    $filelist_names = join ',', @data_names;
  }
  # Add to list of data for all data files
  push @filelist_data, (join ',', @data_values);
  close $in_fh;
  # Continue to the next data file
}


# Write measurment results to file in .csv format
my $app_fh;
if( -e $meas_results_filename ) {
  open $app_fh, ">>$meas_results_filename" or die "Error: Cannot open $meas_results_filename due to: ", $!;
} else {
  open $app_fh, ">>$meas_results_filename" or die "Error: Cannot open $meas_results_filename due to: ", $!;
  print $app_fh $filelist_names, "\n";
}

foreach (@filelist_data) {
  print $app_fh $_, "\n";
}

close $app_fh;


=head1 NAME

meas_sram4t2r-tran.pl - An example Perl measurement script using the MeasSeus.pm Perl module to measure the sram4t2r-tran.i netlist; this script can serve as a potential template to write custom measurement scripts

=head1 SYNOPSYS

    ./meas_sram4t2r-tran.pl  measurement_results_file  output_data_file(s)

Run meas_sram4t2r-tran.pl on a command line to measure the simulation results (in sram4t2r-tran.sti) from the sram4t2r-tran.i netlist.  This measurement script serves as a template for writing custom measurement scripts for other Spice netlists.

To use this script, provide the Spice "output_data_file" name(s), and measurements will be taken/calculated from the "output_data_file.sti" file(s) and written to "measurement_results_file" in .csv format.  For more than one "output_data_file", provide a space-delimited list of files/pathnames; at least one "output_data_file" is expected.

meas_sram4t2r-tran.pl will print its usage to STDERR and exit if it is run without any command-line arguments.

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

