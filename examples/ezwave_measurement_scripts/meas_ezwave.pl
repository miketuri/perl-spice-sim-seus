#!/usr/bin/perl -w
use strict;
use warnings;

# SEUS Package's meas_ezwave.pl:
#   A script using Mentor Graphics EZWave in a Linux/Unix environment for
#   spice simulation measurements.
#   This script is used with a TCL script to make measurements/calculations
#   using EZWave.
#   This script is deprecated.  It is recommended to instead use the newer
#   MeasSeus.pm Perl package with a Perl measurement script unless you wish to
#   use Mentor Graphics EZWave (specifically "run_wdb_server" via Linux/Unix).
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

my $Script_Name = 'meas_ezwave.pl';

# Constants:
my $Ezwave_Exec = 'run_wdb_server -do';

my $Default_Fmt_Ezwave = '.csv';
my $Meas_Sum_Prefix = 'meas_sum_';
my $Meas_Sum_Filetype = '.csv';
my $Stat_Sum_Prefix = 'meas_sum_';
my $Stat_Sum_Filetype = '.txt';

my $Works_Keyword = 'works';
my $Flag_Keyword = 'flag';
my $Filename_Keyword = 'filename';

# Usage:
my $Usage = ''.
"Usage: $Script_Name  [OPTIONS]  deckname_OR_directory_name  tcl_script_name\n".
"Options:\n".
"  -0      Ignore the nominal simulation (e.g., 0.csv or 0.sti) in any directory\n".
"            when computing overall statistics\n".
"  -b N    Begin measurements with the N'th netlist (ordered by name) in the\n".
"            directory\n".
"  -e N    End measurements with the N'th netlist (ordered by name) in the\n".
"            directory\n".
"  -f fmt  Use another file format \"fmt\" (e.g., \"sti\") for input to EZWave\n".
"  -n N    Use a limit of the first N results for statistics\n".
"            (also omit failed sims from the results used)\n".
"  -r      Randomize the order of simulation results\n".
"            (useful if using a limit of the results for statistics)\n".
"  -s      Measure all data sets in subdirectories of \"directory_name\"\n";
my $Min_Arg_Num = 2;

# Procedure: sum
#
#   Computes the sum for a passed array
#
# Parameter: An array of numbers to sum
sub sum {
  my $s = 0;
  foreach (@_) {
    $s += $_;
  }
  return $s
}

# Procedure: mean
#
#  Computes the mean (average) for a passed array
#
# Parameter: An array of numbers to be averaged
sub mean {
  return (sum @_)/(scalar @_);
}

# Procedure: std
#
#   Computes the standard deviation for a passed array
#
# Parameter: An array of numbers to be used in finding the standard deviation
sub std {
  my $sq_err = 0;
  my $ave = mean @_;
  foreach (@_) {
    $sq_err += ($_ - $ave)**2;
  }
  return sqrt ($sq_err/((scalar @_)-1));
}

# meas_ezwave.pl
#
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. and Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
#
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
#
# Note: This script requires Mentor Graphics EZWave (specifically the
#       "run_wdb_server" that does not require the GUI).  It is recommended that
#       the MeasSeus.pm Perl Module is used with a Perl measurement script
#       instead of using this script with a TCL measurement script.
# 
# This is a Perl script used to work with a TCL script to gather measurement
# data from Mentor Graphics EZWave.  The script expects the TCL script to use
# STDIN for a carriage returned list of pathnames (to .csv/.sti files) to
# measure (use one pathname if only measuring one file).  The script expects
# the TCL script to use STDOUT in the following way: first line to STDOUT
# contains a pipe "|" delimited string of the measurement names/titles, the
# second line to STDOUT contains a pipe "|" delimited string of measurement
# data values (1st value on the line corresponds to the 1st measurment name,
# 2nd value corresponds to the 2nd measurment name, etc.), and additional
# lines (3rd and on) to STDOUT contain pipe "|" delimited strings of
# measurement data values for additional pathnames (in order the pathnames
# were given to the TCL script).  If taking measurments for one file, two
# lines of STDOUT are generated, the measurement names and the measurement
# data values.  If taking measurements for N files, N+1 lines of STDOUT are
# generated, the measurement names and N lines of measurement data values.
# 
# Usage: Provide the filename (without the .xxx suffix--e.g., .csv or .sti) or
#        a directory name, and the name of the TCL script to run.  Note that
#        .csv files are assumed by default (use -f fmt to use a different file
#        format (e.g., -f sti to use .sti files)).
#
#        meas_ezwave.pl deckname_or_directory_name* tcl_script_name**
#
#        *If a deckname is specified, then deckname.csv will be measured by
#         the tcl_script_name measurement script and run by EZWave.  If a
#         directory is specified, then all *.csv files in the directory
#         (and optionally in subdirectories if -s option is specified) will
#         be measured by the tcl_script_name measurement script and run by
#         EZWave (unless -b and/or -e options are used to specify which
#         .csv data files to use (e.g., -b 10 -e 20 specifies that the 10th
#         through the 20th (by ASCII sort) .csv data file will be measured)).
#
#        **The tcl_script_name needs to specify the entire filename for a TCL
#          script (e.g., meas_sram4t2r-tran.tcl) that acts as expected by this
#          Perl script and correctly uses the EZWave API.

print "** $Script_Name **\n";
die $Usage if scalar @ARGV < 1;
my $argnum = scalar @ARGV;

my $i = 0;
my $arg = shift;
my $op_rm0; # -0
my $op_begfile; # -b N
my $op_endfile; # -e N
my $fmt_ezwave = $Default_Fmt_Ezwave; # -f fmt
my $op_limit; # -n N
my $op_rand; # -r
my $op_subdirs; # -s
while( $arg =~ m/^-/ ) {
  if( $arg =~ m/^-(\S+)/ ) { # Found options
    my $argmatch = $1;

    # Check single letter options first
    if( $argmatch =~ m/0/i ) {
      $op_rm0 = 1; # Ignore 0.csv or 0.sti (if it exists) when computing overall statistics
    }
    if( $argmatch =~ m/r/i ) { # Randomize the order of the sim results
      $op_rand = 1;
    }
    if( $argmatch =~ m/s/i ) { # Measure netlists in subdirectories
      $op_subdirs = 1;
    }

    # Check options which require another argument
    if( $argmatch =~ m/b/i ) { # Begin measurements with simulation #N
      $op_begfile = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
      $op_begfile--; # Change notation from 1..#files to 0..#files-1
    }
    if( $argmatch =~ m/e/i ) { # End measurements with simulation #N
      $op_endfile = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
      $op_endfile--; # Change notation from 1..#files to 0..#files-1
    }
    if( $argmatch =~ m/f/i ) { # Use another file format "fmt" instead
      $fmt_ezwave = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
      $fmt_ezwave = '.'.$fmt_ezwave unless $fmt_ezwave =~ m/^\./;
    }
    if( $argmatch =~ m/n/i ) { # Use a limit of N for including simulation results in statistics
      $op_limit = shift;
      die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
    }
  }
  die "Error: Not enough parameters\n\n$Usage" if $argnum < (++$i + $Min_Arg_Num);
  $arg = shift;
}
die "Error: Not enough parameters\n\n$Usage" if $argnum < $Min_Arg_Num;

my $deckname = $arg;
my $tcl_name = shift;
die 'Error: Cannot find tcl script "', $tcl_name, '"' unless -e $tcl_name;

my @files;
if( -d $deckname ) { # $deckname is actually the name of a directory with simulations
  if( $deckname !~ m/\/$/ ) { $deckname .= '/'; } # Add the trailing slash if it doesn't exist
  opendir D, $deckname;
  # Get a list of all converted spice output files (e.g., .csv files) from the deck directory (grep /(.csv)$/i, readdir D)
  #   Note: If traversing subdirectories, then @files will have entire paths, else, @files
  #         only contain the basenames.  This is done so numerical sorting is possible if
  #         simulating within the top directory.
  my @files_with_suffix = defined $op_subdirs ? `find $deckname -name \\*$fmt_ezwave` : grep /($fmt_ezwave)$/i, readdir D;
  closedir D;
  # Remove the ".csv" or ".sti" from the end of each file (with map and substr)
  @files = map { substr $_, 0, (rindex $_, '.') } @files_with_suffix;
  # If files have character names, then sort alphabetically, else, sort numerical names numerically
  @files = (join '', @files) =~ m/[a-z]/i ? sort @files : sort { $a <=> $b} @files;

  if( defined $op_rm0 ) { # Ignore the nominal simulation (0.csv or 0.sti) when simulating a directory
    if( defined $op_subdirs ) { # Subdirs are traversed
      my $deckname0 = $deckname.'0';
      @files = grep !/^$deckname0$/, @files; # Remove main directory's 0.i from list of files
      @files = grep !/\/0$/, @files; # Also remove 0.i in subdirs from list of files
    } else {
      @files = grep !/^0$/, @files; # No subdirs traversed, so can just remove "0" from list of files
    }
  }

  my $op_begfile_print;
  my $op_endfile_print;
  if( defined $op_begfile and defined $op_endfile ) { # Use defined beginning/ending boundaries from user if defined
    $op_begfile = 0 unless $op_begfile >= 0 and $op_begfile < scalar @files;
    $op_endfile = (scalar @files)-1 unless $op_endfile >= 0 and $op_endfile < scalar @files and $op_endfile >= $op_begfile;
    $op_begfile_print = ($op_begfile+1).' ('.$files[$op_begfile].$fmt_ezwave.')';
    $op_endfile_print = ($op_endfile+1).' ('.$files[$op_endfile].$fmt_ezwave.')';
    @files = @files[$op_begfile..$op_endfile];
  } elsif( defined $op_begfile ) {
    $op_begfile = 0 unless $op_begfile >= 0 and $op_begfile < scalar @files;
    $op_begfile_print = ($op_begfile+1).' ('.$files[$op_begfile].$fmt_ezwave.')';
    $op_endfile_print = 'END ('.$files[-1].$fmt_ezwave.')';
    @files = @files[$op_begfile..(scalar @files)-1];
  } elsif( defined $op_endfile ) {
    $op_endfile = (scalar @files)-1 unless $op_endfile >= 0 and $op_endfile < scalar @files;
    $op_begfile_print = 'BEGIN ('.$files[0].$fmt_ezwave.')';
    $op_endfile_print = ($op_endfile+1).' ('.$files[$op_endfile].$fmt_ezwave.')';
    @files = @files[0..$op_endfile];
  }

  die 'Error: No ', $fmt_ezwave, ' files found in directory "', $deckname, '"' unless scalar @files > 0;

  if( defined $op_begfile or defined $op_endfile ) {
    print 'Files ', $op_begfile_print, ' through ', $op_endfile_print, " will be measured\n";
    sleep 2;
  }

  if( !defined $op_subdirs ) { # Using the "find" command for subdirectories already includes directory names in the pathname
    my $tmp = join '|', @files;  # Ugly way to add directory names prior to the filenames (assumes pipe "|" isn't in the file name)
    $tmp =~ s/\|/\|$deckname/g;
    $tmp = $deckname.$tmp;
    @files = split /\|/, $tmp;
  }
} elsif( -r $deckname.$fmt_ezwave ) { # $deckname is the name of one netlist's .csv or .sti file
  push @files, $deckname;
} else {
  die 'Error: Cannot find directory "', $deckname, '" or read from "', $deckname, $fmt_ezwave, '"';
}

my $echo_names = join "\n", @files;
open EZWAVE, "echo \"$echo_names\" | $Ezwave_Exec $tcl_name |" or die 'Error: Cannot run EZWave with ', $tcl_name;

my $data_names_str = <EZWAVE>; # Measurement names are on the first line of TCL output, data values are on subsequent lines
my @data_names = split /[,\n]/, $data_names_str;
my @tcl_data = <EZWAVE>;
close EZWAVE;

# Determine the indicies of each "special" measurement
my $works_index = -1;
my $flag_index = -1;
my $filename_index = -1;
for( $i = 0; $i < scalar @data_names; $i++ ) {
  if( $data_names[$i] eq $Works_Keyword ) {
    $works_index = $i;
  } elsif( $data_names[$i] eq $Flag_Keyword ) {
    $flag_index = $i;
  } elsif( $data_names[$i] eq $Filename_Keyword ) {
    $filename_index = $i;
  }
}

my @size_errors; # Error in size; either (data_lines != #_of_files --or-- #_of_measurements_in_a_line != #_of_measurement_names)
my @works_errors; # Errors from "works" variable in TCL measurement
my @flag_warnings; # Warnings from "flag" variable in TCL measurement

if( scalar @tcl_data != scalar @files ) { # Too few lines of data for number of lines simulated
  die 'Error: Failure in EZWave, not all data gathered (', scalar @tcl_data, ' lines of data for ', scalar @files, " files)" unless @files > 1;
  push @size_errors, 'Error: Failure in EZWave, not all data gathered ('.scalar @tcl_data.' lines of data for '.scalar @files." files)";
}

if( defined $op_rand ) { # Randomize the order of the sim results
  for( $i = 0; $i < scalar @tcl_data; $i++ ) {
    my $j = rand scalar @tcl_data;
    @tcl_data[$i, $j] = @tcl_data[$j, $i];
  }
}

for( $i = 0; $i < scalar @tcl_data; $i++ ) {
  my @file_data = split /[,\n]/, $tcl_data[$i]; # All measurements for one file, now as an array
  my $this_name = $filename_index >= 0 ? $file_data[$filename_index] : '#'.$i; # This file's name
  my $limit = scalar @data_names; # How many pieces of measurement data this file has (assume all pieces of data for now)

  if( scalar @file_data != scalar @data_names ) { # Too few measurements in this line for number of measurements expected (# of measurement names)
    push @size_errors, 'Error for file "'.$this_name.'": Has '.scalar @file_data.' data measurements (Expected '.scalar @data_names.')';
    if( scalar @file_data < scalar @data_names ) {
      chomp $tcl_data[$i];
      $tcl_data[$i] .= ",size mismatch error\n"; # Place error in first missing measurement (causes this file to be ignored in overall stats and shows up in .csv file)
      $limit = scalar @file_data; # This file has fewer pieces of data, so change for-loop limit accordingly
    }
  }

  for( my $j = 0; $j < $limit; $j++ ) { # Print the measurment name and value to stdout
    print $data_names[$j], ': ', $file_data[$j], "\n";
  }

  # Capture (for later printing) the 'works' TCL error and 'flag' TCL warning if they exist, are contained in this file's data, and are non-zero
  if( $works_index >= 0 and $works_index < $limit and ($file_data[$works_index] =~ m/error/i or $file_data[$works_index] != 0) ) {
    push @works_errors, 'Error for file "'.$this_name.'": Works is non-zero ('.$file_data[$works_index].')';
  }
  if( $flag_index  >= 0 and $flag_index  < $limit and $file_data[$flag_index]  != 0 ) {
    push @flag_warnings, 'Warning for file "'.$this_name.'": Flag is non-zero ('.$file_data[$flag_index].')';
  }
}

if( scalar @files > 1 ) {
  my @deck_basename = split /\//, $deckname; # The directory name (basename of the pathname) is at the end of the array
  open MEAS_SUM, ">$Meas_Sum_Prefix$deck_basename[-1]$Meas_Sum_Filetype"
       or die "Error: Cannot create $Meas_Sum_Prefix$deck_basename[-1]$Meas_Sum_Filetype for output due to ", $!;

  # Print the first row to the .csv file (sim_name and the names of all measurements)
  print MEAS_SUM $data_names_str;

  # Print the row of measurements for each simulation
  foreach (@tcl_data) {
    print MEAS_SUM;
  }
  print MEAS_SUM "\n";
  close MEAS_SUM;

  open STAT_SUM, ">$Stat_Sum_Prefix$deck_basename[-1]$Stat_Sum_Filetype"
       or die "Error: Cannot open $Stat_Sum_Prefix$deck_basename[-1]$Stat_Sum_Filetype due to: ", $!;

  # Removing failed simulations from the overall statistics
  for( $i = 0; $i < scalar @tcl_data; $i++ ) {
    if( $tcl_data[$i] =~ m/error/i ) {

      my @file_data = split /[,\n]/, $tcl_data[$i]; # Let's pinpoint where the error occurred
      my $this_name = $filename_index >= 0 ? $file_data[$filename_index] : '#'.$i; # This file's name
      for( my $j = 0; $j < scalar @file_data; $j++ ) {
        if( $file_data[$j] =~ m/error/i ) {
          print 'Error for file "', $this_name, '": Measurement "', $data_names[$j], "\" failed\n";
          print STAT_SUM 'Error for file "', $this_name, '": Measurement "', $data_names[$j], "\" failed\n";
        }
      }

      splice @tcl_data, $i, 1; # Remove the failed simulation ($i now indexes the next simulation
      $i--; # Compensate for the for-loop incrementing $i on the next pass
    }
  }
  my $complete_sims = $i; # Also == scalar @tcl_data
  print "\n";

  my $count;
  if( $complete_sims < 2 ) {
    print STAT_SUM "Skipping statistical summary (too few simulations successfully completed without errors)\n";
    print "Skipping statistical summary (too few simulations successfully completed without errors)\n";
  } else {
    # Print the means and standard deviations for each measurement to a summary file
    my $m;
    my $s;
    for( $i = 0; $i < scalar @data_names; $i++ ) { # Not using a foreach since $i is used to find correct data in @tcl_data

      next if $data_names[$i] eq $Works_Keyword; # Don't do mean & std. dev. for works entry
      next if $data_names[$i] eq $Flag_Keyword; # Don't do mean & std. dev. for flag entry
      next if $data_names[$i] eq $Filename_Keyword; # Don't do mean & std. dev. for filename entry
      my @meas_data;
      $count = 0;
      foreach (@tcl_data) {
        if( defined $op_limit and ++$count > $op_limit ) { last; }
        my @file_data = split /[,\n]/;
        push @meas_data, $file_data[$i]; # @meas_data contains all data for a given measurement
      }
      $m = mean @meas_data;
      $s = std @meas_data;
      print STAT_SUM $data_names[$i], ":\nmean and stddev:\n", $m, "\n", $s, "\n";
      print $data_names[$i], ":\nmean and stddev:\n", $m, "\n", $s, "\n";
    }
  }

  print STAT_SUM $complete_sims, " simulations completed without errors\n";
  print $complete_sims, " simulations completed without errors\n";
  print STAT_SUM 'Statistics computed for ', $count-1, " simulations\n";
  print 'Statistics computed for ', $count-1, " simulations\n";

  foreach (@size_errors, @works_errors, @flag_warnings) { # Now print all of the errors and warnings
    print STAT_SUM $_, "\n";
    print $_, "\n";
  }

  close STAT_SUM;

} else {
  # Only one file was simulated, print the data values again for easy copy-paste, then print errors or warnings
  print "\n";
  my @file_data = split /[,\n]/, $tcl_data[0]; # All measurements for one file, now as an array
  foreach (@file_data) {
    print $_, "\n";
  }
  print "\n";

  foreach (@size_errors, @works_errors, @flag_warnings) { # Now print all of the errors and warnings
    print $_, "\n";
  }
}


=head1 NAME

meas_ezwave.pl - A Perl script from the SEUS (Scripts for Easier Use of Spice) Package which uses Mentor Graphics EZWave in a Linux/Unix environment to make spice simulation measurements.  This script is used with a TCL script (such as the example script meas_sram4t2r-tran.tcl) to make measurements/calculations using EZWave.

This is a deprecated script.  It is recommended to instead use the MeasSeus.pm Perl package with a Perl measurement script unless preferring Mentor Graphics EZWave (specifically "run_wdb_server" via Linux/Unix).

=head1 SYNOPSYS

    meas_ezwave.pl  [OPTIONS]  deckname_or_directory_name  tcl_script_name

Run meas_ezwave.pl on a command line and provide a deckname (e.g., to measure a data set (I<inverter.csv>) from the spice netlist named I<inverter.i>, then use I<inverter> as the deckname) or a directory_name and a TCL measurement script to measure the data set(s) and calculate statistics.  Data sets are assumed to be in .csv format, but the -f option can specify a different file format, such as .sti; note, mkout_seus.pl can generate both of these file formats.  If a deckname is specified, then deckname.csv will be measured by the tcl_script_name measurement script and run by EZWave.  If a directory is specified, then all .csv files in the directory (and optionally in subdirectories if the -s option is specified) will be measured by the tcl_script_name measurement script and run by EZWave.  Measurements are written to "meas_sum_I<deckname_or_directory_name>.csv" and statistics are written to "meas_sum_I<deckname_or_directory_name>.txt" in the current working directory.  This script is often used in tandem with batchexec_seus.pl.

The TCL measurement script (specified by tcl_script_name, the entire filename of the TCL script) must correctly use the Mentor Graphics EZWave API.  In addition, meas_ezwave.pl expects the TCL measurement script to use STDIN for a carriage-returned list of pathnames  (to .csv/.sti files) to measure; note, use one pathname if only measuring one file.  meas_ezwave.pl also expects the TCL measurement script to use STDOUT in the following way:

=over

=item *

The first line to STDOUT contains a pipe "|" delimited string of the measurement names/titles

=item *

The second line to STDOUT contains a pipe "|" delimited string of measurement data values (first value on the line corresponds to the first measurement name, second value corresponds to the second measurement name, etc.)

=item *

Additional lines (third line and on) to STDOUT contain pipe "|" delimited strings of measurement data values for additional pathnames (in the order the pathnames were given to the TCL script).

=back

If taking measurements for one file, then two lines of STDOUT are generated: the measurement names and the measurement data values.  If taking measurements for N files, then N+1 lines of STDOUT are generated: the measurement names and N lines of measurement data values.

meas_ezwave.pl will print its usage to STDERR and exit if it is run without any command-line arguments.

=head1 LIMITATIONS

Note: If a deckname has the same name as a directory, then this script will prefer to measure data sets in the directory.  For example, if running "C<meas_ezwave.pl sram measure.tcl>" and both "sram.csv" and "sram/" are present in the current working directory, then this script will prefer to measure data sets in "sram/".  Furthermore, if "sram/" does not contain any data sets, then this will cause an error in this script.

=head1 OPTIONS

To see all available options, run meas_ezwave.pl without any command-line arguments to print its usage, including all options, to STDERR.

By default, if no options are provided, then the data set from the specified deck name, or all data sets in the specified directory (but not its subdirectories), will be measured and statistics will be computed.

=head2 -0  Ignore the nominal simulation (e.g., 0.csv or 0.sti) in any directory when computing overall statistics

A 0.i netlist is created by init_batch_seus.pl when initializing a directory with a batch of swept or varied netlists; this netlist is functionally identical to the original netlist without any swept or varied parameters.  This option will skip measuring data from this netlist (e.g., a 0.csv or a 0.sti file).  This can be useful to calculate the performance statistics of the batch of netlists, omitting the 0.i simulation.

=head2 -b N  Begin measurements with the N'th data set (ordered by name) in the directory

Only data (.csv files by default, unless using the -f option) in the specified directory from data sets I<N> to the final data set will be measured.  For example:

If a directory contains 0.csv, 1.csv, 2.csv, 3.csv, 4.csv, and 5.csv, then

=over

=item *

-b 3 will only measure data sets 2.csv, 3.csv, 4.csv, and 5.csv (data set 0.csv is the first data set, 1.csv is the second data set, ...)

=item *

-0b 3 will only measure data sets 3.csv, 4.csv, and 5.csv (now data set 1.csv is the first data set, 2.csv is the second data set, ...)

=back

If a directory contains 0.csv, a.csv, b.csv, c.csv, d.csv, and e.csv, then, similarly,

=over

=item *

-b 3 will only measure data sets b.csv, c.csv, d.csv, and e.csv (data set 0.csv is the first data set, a.csv is the second data set, ...)

=item *

-0b 3 will only measure data sets c.csv, d.csv, and e.csv (now data set a.csv is the first data set, b.csv is the second data set, ...)

=back

If the -s option is also used to measure data sets in subdirectories, and if the directory contains 0.csv, bar/0.csv, bar/a.csv, bar/b.csv, bar/c.csv, foo/0.csv, foo/1.csv, foo/2.csv, and foo/3.csv, then

=over

=item *

-sb 5 will only measure data sets bar/c.csv, foo/0.csv, foo/1.csv, foo/2.csv, and foo/3.csv (data set 0.csv is the first data set, bar/0.csv is the second data set, ...)

=item *

-0sb 5 will only measure data sets foo/2.csv and foo/3.csv (now data set bar/a.csv is the first data set, bar/b.csv is the second data set, ...)

=back

If the -e option is also used to specify where to end measurements, then a range of data sets can be specified.  If a directory contains 0.csv, 1.csv, 2.csv, 3.csv, 4.csv, and 5.csv, then

=over

=item *

-b 2 -e 4 will only measure data sets 1.csv, 2.csv, and 3.csv (data set 0.csv is the first data set, 1.csv is the second data set, ...)

=item *

-0b 2 -e 4 will only measure data sets 2.csv, 3.csv, and 4.csv (now data set 1.csv is the first data set, 2.csv is the second data set, ...)

=back

=head2 -e N  End measurements with the N'th data set (ordered by name) in the directory

Only data (.csv files by default, unless using the -f option) in the specified directory from the first data set to data set I<N> will be measured.  For example:

If a directory contains 0.csv, 1.csv, 2.csv, 3.csv, 4.csv, and 5.csv, then

=over

=item *

-e 3 will only measure data sets 0.csv, 1.csv, and 2.csv (data set 0.csv is the first data set, 1.csv is the second data set, ...)

=item *

-0e 3 will only measure data sets 1.csv, 2.csv, and 3.csv (now data set 1.csv is the first data set, 2.csv is the second data set, ...)

=back

If a directory contains 0.csv, a.csv, b.csv, c.csv, d.csv, and e.csv, then, similarly,

=over

=item *

-e 3 will only measure data sets 0.csv, a.csv, and b.csv (data set 0.csv is the first data set, a.csv is the second data set, ...)

=item *

-0e 3 will only measure data sets a.csv, b.csv, and c.csv (now data set a.csv is the first data set, b.csv is the second data set, ...)

=back

If the -s option is also used to measure data sets in subdirectories, and if the directory contains 0.csv, bar/0.csv, bar/a.csv, bar/b.csv, bar/c.csv, foo/0.csv, foo/1.csv, foo/2.csv, and foo/3.csv, then

=over

=item *

-se 5 will only measure data sets 0.csv, bar/0.csv, bar/a.csv, bar/b.csv, and bar/c.csv (data set 0.csv is the first data set, bar/0.csv is the second data set, ...)

=item *

-0se 5 will only measure data sets bar/a.csv, bar/b.csv, bar/c.csv, foo/1.csv and foo/2.csv (now data set bar/a.csv is the first data set, bar/b.csv is the second data set, ...)

=back

If the -b option is also used to specify where to begin measurements, then a range of data sets can be specified.  If a directory contains 0.csv, 1.csv, 2.csv, 3.csv, 4.csv, and 5.csv, then

=over

=item *

-b 2 -e 4 will only measure data sets 1.csv, 2.csv, and 3.csv (data set 0.csv is the first data set, 1.csv is the second data set, ...)

=item *

-0b 2 -e 4 will only measure data sets 2.csv, 3.csv, and 4.csv (now data set 1.csv is the first data set, 2.csv is the second data set, ...)

=back

=head2 -f fmt  Use another file format "fmt" (e.g., "sti") for input to EZWave

By default, meas_ezwave.pl expects to measure data sets in .csv files.  This is one format that Mentor Graphics EZWave can interpret, but EZWave can also interpret .sti files.  Use this option (e.g., -f sti) to instead measure data sets in .sti files.

=head2 -n N    Use a limit of the first N results for statistics (also omit failed measurements from the statistics)

This option will cause this script to compute statistics for only the first N measurements which finish without failure.  This is helpful to only measure a particular number of data sets of a Monte Carlo simulation.  It may be useful to also use the -r option to randomize the order of simulation results.

=head2 -r  Randomize the order of simulation results (useful if using a limit of the results for statistics)

This option will randomize the order of simulation results, whereas typically simulation results are obtained in the order that they were measured.  If using the -n option to limit the number of results when calculating statistics, then randomizing the results first may be preferable so that the statistics do not just cover the first N measurements completed.

=head2 -s  Measure all data sets in subdirectories of directory_name

By default, meas_ezwave.pl will measure data sets that are only found at the first level of the specified directory_name.  This option can be used to measure data sets that are found within subdirectories of directory_name.

Note 1: If also using the -0 option, then this script will not measure 0.csv (or a data set in a different format if the -f option is used) in directory_name or any subdirectory within

Note 2: If also using the -b and/or -e options, then this script will measure a number of data sets ordered by name

=head1 SUPPORT

To contribute to this script or seek support, please contact the author (see next section).  To report a bug, please submit this via the I<Issues> tracker in the SEUS Package repository: L<https://bitbucket.org/miketuri/perl-spice-sim-seus/issues>

=head1 AUTHOR

Mike Turi <mturi@ieee.org>

=head1 COPYRIGHT

Copyright (C) 2009-2020 Michael A. Turi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see L<https://www.gnu.org/licenses/>.

=cut

