# SEUS Package's meas_sram4t2r-tran.tcl:
#   An example measurement script for the SEUS package to use with meas_ezwave.pl.
#   While this can be used as a model to write custom measurement scripts, it is
#   recommended to instead use the newer MeasSeus.pm Perl package with a Perl
#   measurement script (such as meas_sram4t2r-tran.pl) unless you wish to use
#   Mentor Graphics EZWave (specifically "run_wdb_server" via Linux/Unix).
# Copyright (C) 2009-2020 Michael A. Turi

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set Scriptname "meas_sram4t2r-tran.tcl"

# meas_sram4t2r-tran.tcl
# 
# Creator: Mike Turi
# Currently at: California State University, Fullerton; Cpt. Engineering Prgm.
# Formerly at: Pacific Lutheran University; Dept. of Cpt. Sci. & Cpt. Eng.
# Formerly at: Washington State University; School of Elect. Eng. & Cpt. Sci.
# 
# Note: This program is distributed without any warranty.  It may fail under
#       certain conditions; please refer to the README for more details.
#
# Note: This script requires Mentor Graphics EZWave to run it (specifically the
#       "run_wdb_server" that does not require the GUI).  It is recommended to
#       use the MeasSeus.pm Perl package with a Perl measurement script
#       instead of using the meas_ezwave.pl Perl script with this TCL
#       measurement script.  Note that the sram4t2r-tran.pl example script does
#       not make the same measurements/calculations as this script.
# 
# This is an example Perl script that measures and calculates performance of
# an ngspice netlist; specifically the example sram4t2r-tran.i netlist of a
# 4T+2R (four transistors and two resistors) SRAM cell.
#
# This TCL script will be called by Mentor Graphics EZWave (specifically, the
# "run_wdb_server" that does not require the GUI).  The filename for this script
# must be provided to EZWave by the meas_ezwave.pl Perl script.
#
# Usage: This script expects pathname(s) of deck(s) from stdin
#          (usually by "echo deckpath |")
#        Use a carriage-returned list of pathnames for more than one:
#          path1\npath2\npath3...
#        In order to measure properly without error, a .csv data file must be
#          present for each deck to measure (e.g. path1.csv must exist for path1).
#          If you wish to use a different type of data file (e.g. .sti), then
#          you must edit this script to work with .sti files instead of .csv files;
#          This would require editing lines 107, 108, and 115.

puts stderr "** $Scriptname **\n\n"

set deckpath [list]
set deckname [list]

while { [eof stdin] != 1 } {
  set x [gets stdin]

  if { [regexp "^\W*$" $x] } { # If there is a blank line, assume end of stdin
    break
  }

  lappend deckpath $x
  lappend deckname [lindex [split $x "/"] end]
}

# Measurement Settings:
# ---------------------
# Trigger when Read causes Bit/Nbit discharge to X volts
set RD_LOW_TRIG 0.5
set ANOTHER_MEAS_TRIG 0.9
set meas_setting_names [list "rd_low_trig" "another_trig"]
set meas_setting_values [list $RD_LOW_TRIG $ANOTHER_MEAS_TRIG]

# Constants:
# ----------
# Netlist Nets or Voltage Sources
set NET_B "bit"
set V_CELL "vdd"

# Netlist Variables
set VAR_VDD_VAL "vddval"
set VAR_SIMLEN "simlen"
set net_var_names [list "vdd value" "sim length"]

# Loop Variables
set name_count 0
append name_print [join $meas_setting_names ","] ",filename,works"
set data_lst [list]

foreach path $deckpath name $deckname {

  set dataout $meas_setting_values
  set names $meas_setting_names

  lappend names "filename"
  lappend dataout $path

  if { [file isfile "$path.csv"] == 0 } {
    puts stderr "Error ($Scriptname): File \"$path.csv\" not found"
    lappend dataout "error - could not open"
    lappend data_lst [join $dataout ","]
    dataset close
    continue
  }

  dataset open "$path.csv"
  # -----------------------------------------------------------------
  # begin measurement section

  # Example: Using a voltage source to pass a variable value to the TCL script
  set vdd_val [wfc "yval(wf(\"<$name/TRAN>V($VAR_VDD_VAL)\"), 0)"]
  set simlen [wfc "yval(wf(\"<$name/TRAN>V($VAR_SIMLEN)\"), 0)"]
  set net_var_values [list $vdd_val $simlen]

  # Locations:
  # ----------
  # Time in the simulation to measure the leakage current
  set LOC_LK 1000e-9
  # Time in the simulation where the read 0 operation begins/ends
  set LOC_RD0_BEG 1230e-12
  set LOC_RD0_END 1360e-12

  # For Errors - do some tests and set errors to a meaningful non-zero value for an error
  set RD_LOW_TRIG_VOLT [expr {$RD_LOW_TRIG * $vdd_val}]
  set rd0_ok [wfc "yval(wf(\"<$name/TRAN>V($NET_B)\"), $LOC_RD0_END)"]

  set works ""
  if { $rd0_ok > $RD_LOW_TRIG_VOLT } {
    append works "Read0 Failed ($rd0_ok); "
  }
  lappend names "works"
  if { $works ne "" } {
    lappend dataout "error = $works"
    lappend data_lst [join $dataout ","]
    dataset close
    continue
  } else {
    lappend dataout 0
  }

  # Suggested to add netlist variable names and values now after it has been verified that the simulation worked
  set names [concat $names $net_var_names]
  set dataout [concat $dataout $net_var_values]

  # Example: Using yval to get a leakage current value
  set leakage [wfc "abs(yval(wf(\"<$name/TRAN>I($V_CELL)\"), $LOC_LK))"]
  lappend names "leakage value"
  lappend dataout $leakage
  
  # Example: Using xval to get a delay for the read 0 operation
  set rd_time [wfc "xval(wf(\"<$name/TRAN>V($NET_B)\"), $RD_LOW_TRIG_VOLT, $LOC_RD0_BEG, $LOC_RD0_END) - $LOC_RD0_BEG"]
  # Caution: There could be 0, 1, or multiple x-values for a given y-interval --> must check result before using
  set testlen [llength $rd_time]
  if { $testlen == 1 } {
    # No error: one crossing found
    lappend dataout $rd_time
  } elseif { $testlen < 1 } {
    # An error code: no crossings found
    lappend dataout "error $testlen"
  } else {
    # An error code: multiple crossings found
    lappend dataout "error $testlen"
  }
  lappend names "rd0 time"
  # If $rd_time is used elsewhere in the script (e.g. see below) may want to abort the rest of the script for this file
  if { $testlen != 1 } {
    lappend data_lst [join $dataout ","]
    # Important!  Must close dataset anytime "continue" is used, or else the same file's dataset will be used for all subsequent measurements in the foreach loop
    dataset close
    continue
  }

  # Example: Using avg to get an average current value when reading a 0
  set ave_current [wfc "avg(abs(wf(\"<$name/TRAN>I($V_CELL)\")), $LOC_RD0_BEG, $LOC_RD0_BEG + $rd_time)"]
  lappend names "rd0 cell current"
  lappend dataout $ave_current
  # Calculate power, energy, and energy-delay product (EDP)
  lappend names "rd0 cell power" "rd0 cell energy" "rd0 cell EDP"
  lappend dataout [expr {$ave_current * $vdd_val}]
  lappend dataout [expr {$ave_current * $vdd_val * $rd_time}]
  lappend dataout [expr {$ave_current * $vdd_val * $rd_time * $rd_time}]

  # For Warnings -- do some tests and set flag to a meaningful non-zero value for a warning
  set flag ""
  if { $leakage > 10e-9 } {
    append flag "Leakage Current ($leakage)) > 10nA; "
  }
  lappend names "flag"
  if { $flag eq "" } {
    set flag 0
  }
  lappend dataout $flag

  # end measurement section
  # -----------------------------------------------------------------
  if { [llength $names] > $name_count } {
    set name_count [llength $names]
    set name_print [join $names ","]
  }
  lappend data_lst [join $dataout ","]
  dataset close
}

puts $name_print
foreach i $data_lst {
  puts $i
}

exit

