* Transient analysis of 4T+2R (four transistors and two resistors) SRAM cell

* An example netlist for the SEUS package

* This netlist comes with ABSOLUTELY NO WARRANTY.
* This is free and you are welcome to redistribute it under certain conditions.
* Please refer to the full AGPL-3.0-or-later license file (named 'LICENSE')
*   in the SEUS package for more details.

* Using BSIM3 MOSFET models
.model nmos nmos level=49
+ VERSION=3.3
+ VTH0=0.6
+ U0=670
+ TOX=12n
+ NCH=2E17
+ NSUB=1E17
+ XT=1.5E-7

.model pmos pmos level=49
+ VERSION=3.3
+ VTH0=-0.6
+ U0=250
+ TOX=12n
+ NCH=2E17
+ NSUB=1E17
+ XT=1.5E-7

.param l = 0.35u
.param w = 3u
.param vddval = 3.3
.param tempc = 27

.param rval = 60k
.param cval = 5p
.param simlen = 1450n

* Dummy voltage sources for measurement script variables
vddval vddval 0 dc {vddval}
vsimlen simlen 0 dc {simlen}


* D G S B

* Resistor-Inverter subcircuit
.SUBCKT inv in out vdd vss

rp vdd out {rval}
mn out in vss vss nmos l={l} w={w} m=1

.ENDS inv


* 4T+2R SRAM Cell
mbit  bit wl bitcell gnd nmos l={l} w={w} m=1

xinv1 bitcell  nbitcell vdd vss inv
xinv2 nbitcell bitcell  vdd vss inv

mnbit nbit wl nbitcell gnd nmos l={l} w={w} m=1


* Precharge transistors for bit lines
mpbit  bit  pre vddp vddp pmos l={l} w={4*w} m=1
mpnbit nbit pre vddp vddp pmos l={l} w={4*w} m=1


* Tri-State Inverter subcircuit
.SUBCKT triinv in out en en_b vdd vss

mp   ppp in   vdd vdd pmos l={l} w={4*w} m=1
mpen out en_b ppp vdd pmos l={l} w={4*w} m=1
mnen out en   nnn vss nmos l={l} w={4*w} m=1
mn   nnn in   vss vss nmos l={l} w={4*w} m=1

.ENDS triinv

* Tri-state inverters for write circuitry
xwrnbit in   nbit wr  wr_b vddp vssp triinv
xwrbit  in_b bit  wr  wr_b vddp vssp triinv


* Capacitive load on bit line
cbit  bit  0 {cval}
cnbit nbit 0 {cval}


vdd vdd 0 dc {vddval}
vss vss 0 dc 0
vddp vddp 0 dc {vddval}
vssp vssp 0 dc 0

* Precharge signal (active low)
vpre  pre  0 dc {vddval} pwl( 0n {vddval}  310n {vddval}  350n 0  460n 0  500n {vddval}  1010n {vddval}  1050n 0  1160n 0  1200n {vddval} )

* Write signal (and inverse)
vwr   wr   0 dc 0 pwl( 0n 0  10n 0  50n {vddval}  160n {vddval}  200n 0  710n 0  750n {vddval}  860n {vddval}  900n 0 )
bwr_b wr_b 0 v={vddval}-v(wr)

* Input value to write (and inverse)
vin   in   0 dc {vddval} pwl( 0n {vddval}  210n {vddval}  250n 0 )
bin_b in_b 0 v={vddval}-v(in)

* Word Line: Write 1 from 50-160ns, Read from 550-660ns, Write 0 from 750-860ns, Read from 1250-1360ns
vwl   wl 0 dc 0 pwl( 0n 0  10n 0  50n {vddval}  160n {vddval}  200n 0  510n 0  550n {vddval}  660n {vddval}  700n 0  710n 0  750n {vddval}  860n {vddval}  900n 0  1210n 0  1250n {vddval}  1360n {vddval}  1400n 0 )

.tran 2n {simlen}
.ic v(bit)=0 v(bitcell)=0 v(nbit)={vddval} v(nbitcell)={vddval}

.OP
.PRINT TRAN v(vddval) v(simlen) v(pre) v(wr) v(in) v(wl) v(bit) v(nbit) v(bitcell) v(nbitcell) i(vdd) i(vss)

.OPTION  ABSTOL=1E-6 PIVTOL=1E-10 GMIN=1E-14 VNTOL=2.5E-5 RELTOL=1E-2 CHGTOL=1E-18 METHOD = GEAR ITL1=1000 ITL4=500 TEMP={tempc}

.END

